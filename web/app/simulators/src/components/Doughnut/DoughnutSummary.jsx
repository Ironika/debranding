import React from 'react'
import PropTypes from 'prop-types'
import ReactDom from 'react-dom'
import * as d3 from 'd3'

import styles from './doughnut.scss'

export default class DoughnutSummaryChart extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      className: this.props.className,
      width: this.props.width,
      height: this.props.height,
      percent: this.props.percent,
      duration: this.props.duration,
      customText: this.props.customText
    }
  }

  // The canvas is drawn when the component is mounted.
  componentDidMount () {
    this.draw(this.props.className, this.props.width, this.props.height, this.props.percent, this.props.duration, this.props.customText)
  }

  // The canvas is drawn when the component updates.
  componentDidUpdate () {
    d3.selectAll('.' + this.props.className + '> svg').remove()
    this.draw(this.props.className, this.props.width, this.props.height, this.props.percent, this.props.duration, this.props.customText)
  }

  draw (className, width, height, percent, duration, customText) {
    const outerRadius = height / 2 - 5
    const innerRadius = outerRadius - 5
    const dataset = {
      lower: this.calcPercent(0),
      upper: this.calcPercent(percent),
      max:  this.calcPercent(100),
    }
    const cornerRadius = 50
    const pie = d3.pie().sort(null)
    const format = d3.format('.0%')

    let arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .cornerRadius(cornerRadius)

    let svg = d3.select('.' + className).append('svg')
            .attr('width', width)
            .attr('height', height)
            .attr('class', 'donut')
            .append('g')
            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    let svg2 = d3.select('.' + className).append('svg')
            .attr('width', width)
            .attr('height', height)
            .attr('class', 'background')
            .append('g')
            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    svg2.selectAll('path')
      .data(pie(dataset.lower))
      .enter().append('path')
        .style('fill', 'white')
        .style('fill-opacity', 1)
        .attr('d', arc)

    let path = svg.selectAll('path')
      .data(pie(dataset.lower))
      .enter().append('path')
      .attr('class', function (d, i) {
        return 'color' + i
      })

      .attr('d', arc)
      .each(function (d) {
        this._current = d
      })

    // let custom = svg.append('text')
    //   .attr('text-anchor', 'start')
    //   .attr('dy', '0px')
    //   .attr('class', 'custom')
    //   .text(customText)

    // let text = svg.append('text')
    //   .attr('text-anchor', 'start')
    //   .attr('class', 'percentage')
    //   .attr('dy', '0px')

    let progress = 0

    let timeout = setTimeout(function () {
      clearTimeout(timeout)
      path = path.data(pie(dataset.upper))
      path.transition().duration(duration).attrTween('d', function (a) {
        let i = d3.interpolate(this._current, a)
        let i2 = d3.interpolate(progress, percent)
        this._current = i(0)
        return function (t) {
          //text.text((format(i2(t) / 100)))
          return arc(i(t))
        }
      })
    }, 200)
  }

  calcPercent (percent) {
    return [percent, 100 - percent]
  }

  render () {
    return (
      <div className={this.props.className + ' doughnut'}>
        {/* {doughnut} */}
      </div>
    )
  }
}

DoughnutSummaryChart.propTypes = {
  className: PropTypes.any,
  width: PropTypes.any,
  height: PropTypes.any,
  percent: PropTypes.any,
  duration: PropTypes.any,
  customText: PropTypes.any
}

DoughnutSummaryChart.defaultProps = {
  className: '',
  width: 0,
  height: 0,
  percent: 0,
  duration: 0,
  customText: ''
}