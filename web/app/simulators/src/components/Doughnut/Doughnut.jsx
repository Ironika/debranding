import React from 'react'
import PropTypes from 'prop-types'
import ReactDom from 'react-dom'
import * as d3 from 'd3'

import styles from './doughnut.scss'

export default class DoughnutChart extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      className: this.props.className,
      width: this.props.width,
      height: this.props.height,
      percent: this.props.percent,
      duration: this.props.duration,
      customTextTop: this.props.customTextTop,
      customTextBottom: this.props.customTextBottom
    }
  }

  // The canvas is drawn when the component is mounted.
  componentDidMount () {
    this.draw(this.props.className, this.props.width, this.props.height, this.props.percent, this.props.duration, this.props.customTextTop, this.props.customTextBottom)
  }

  // The canvas is drawn when the component updates.
  componentDidUpdate () {
    d3.selectAll('.' + this.props.className + '> svg').remove()
    this.draw(this.props.className, this.props.width, this.props.height, this.props.percent, this.props.duration, this.props.customTextTop, this.props.customTextBottom)
  }

  draw (className, width, height, percent, duration, customTextTop, customTextBottom) {
    const outerRadius = height / 2 - 10
    const innerRadius = outerRadius - 10
    const dataset = {
      lower: this.calcPercent(0),
      upper: this.calcPercent(percent),
      max:  this.calcPercent(100),
    }
    const cornerRadius = 50
    const pie = d3.pie().sort(null)
    const format = d3.format('.0%')

    let arc = d3.arc()
      .innerRadius(innerRadius)
      .outerRadius(outerRadius)
      .cornerRadius(cornerRadius)

    let svg = d3.select('.' + className).append('svg')
            .attr('width', width)
            .attr('height', height)
            .attr('class', 'donut')
            .attr('shape-rendering', 'geometricPrecision')
            .append('g')
            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

  

    let svg2 = d3.select('.' + className).append('svg')
            .attr('width', width)
            .attr('height', height)
            .attr('class', 'background')
            .attr('shape-rendering', 'geometricPrecision')
            .append('g')
            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');

    svg2.selectAll('path')
      .data(pie(dataset.lower))
      .enter().append('path')
        .style('fill', 'white')
        .style('fill-opacity', 1)
        .attr('d', arc)

    let path = svg.selectAll('path')
      .data(pie(dataset.lower))
      .enter().append('path')
      .attr('class', function (d, i) {
        return 'color' + i
      })

      .attr('d', arc)
      .each(function (d) {
        this._current = d
      })


    // let fo = svg.append('foreignObject')
    //   .attr('width', width - 100)
    //   .attr('height', height - 100)
    //   .attr('transform', 'translate(' + -(width - 100) / 2 + ',' + -(height - 100) / 2 + ')')
    
    // let div = fo.append('xhtml:div')
    //   .attr('class', 'doughnut_content')

    // div.append('p')
    //   .attr('class', 'customTextTop')
    //   .html(customTextTop)

    let text = svg.append('text')
      .attr('text-anchor', 'middle')
      .attr('class', 'percentage')
      .attr('dy', '5px')

    // div.append('p')
    //   .attr('class', 'customTextBottom')
    //   .html(customTextBottom)

    let progress = 0

    let timeout = setTimeout(function () {
      clearTimeout(timeout)
      path = path.data(pie(dataset.upper))
      path.transition().duration(duration).attrTween('d', function (a) {
        let i = d3.interpolate(this._current, a)
        let i2 = d3.interpolate(progress, percent)
        this._current = i(0)
        return function (t) {
          text.text((format(i2(t) / 100)))
          return arc(i(t))
        }
      })
    }, 200)
  }

  calcPercent (percent) {
    return [percent, 100 - percent]
  }

  render () {
    return (
      <div className={this.props.className + ' doughnut'}>
        <div className='doughnut_content'>
          <p className='customTextTop'>{ this.props.customTextTop }</p>
          <p className='customTextBottom'>{ this.props.customTextBottom }</p>
        </div>
        {/* {doughnut} */}
      </div>
    )
  }
}

DoughnutChart.propTypes = {
  className: PropTypes.any,
  width: PropTypes.any,
  height: PropTypes.any,
  percent: PropTypes.any,
  duration: PropTypes.any,
  customTextTop: PropTypes.any,
  customTextBottom: PropTypes.any
}

DoughnutChart.defaultProps = {
  className: '',
  width: 0,
  height: 0,
  percent: 0,
  duration: 0,
  customTextTop: '',
  customTextBottom: ''
}