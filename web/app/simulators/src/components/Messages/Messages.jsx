import React from 'react'
import PropTypes from 'prop-types'
import ReactDom from 'react-dom'

import styles from './messages.scss'

import classNames from 'classnames'

export default class Messages extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      type: this.props.type,
      title: this.props.title,
      message: this.props.height,
    }
  }

  // The canvas is drawn when the component is mounted.
  componentDidMount () {
    
  }

  // The canvas is drawn when the component updates.
  componentDidUpdate () {
  }

  messagesFormated () {
    if (this.props.type === 'advice') {
      document.getElementById('results').classList.add('overlay')
      return (
        <div>
          <h3 className='messages-title'>{ this.props.title }</h3>
          <p className='messages-content' dangerouslySetInnerHTML={{ __html:  this.props.message }} />
        </div>
      )
    }

    if (this.props.type === 'error') {
      document.getElementById('results').classList.add('overlay')
      let element = document.getElementsByClassName('product')
      for (let i = 0; i < element.length; i++) {
        element[i].classList.add('product-error')
      }
      return (
        <div>
          <h3 className='messages-title'>{ this.props.title }</h3>
          <p className='messages-content' dangerouslySetInnerHTML={{ __html:  this.props.message }} />
        </div>
      )
    }
  }

  render () {
    return (
      <div className={'messages messages_' + this.props.type }>
        { this.messagesFormated() }
      </div>
    )
  }
}

Messages.propTypes = {
  type: PropTypes.any,
  title: PropTypes.any,
  message: PropTypes.any,
}

Messages.defaultProps = {
  type: '',
  title: '',
  message: ''
}