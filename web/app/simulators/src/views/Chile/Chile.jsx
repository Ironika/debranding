import React from 'react'
// import PropTypes from 'prop-types'
import update from 'immutability-helper'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import './chile.scss'

import Toggle from './../../components/Toggle/Toggle'
import CounterWithIcons from './../../components/CounterWithIcons/CounterWithIcons'
// import SliderTooltip from './../../components/SliderTooltip/SliderTooltip'
import Bar from './../../components/Bar/Bar'
import BarSummaryChart from './../../components/Bar/BarSummary'

import money1 from './../../images/money-1.svg'
import money2 from './../../images/money-2.svg'
import money3 from './../../images/money-3.svg'
import people1 from './../../images/people-1.svg'
import people2 from './../../images/people-2.svg'
import people3 from './../../images/people-3.svg'

import ChileSimulator from './../../utils/chileSimulator.js'

import axios from 'axios'
// import classNames from 'classnames'

class ChileApp extends React.Component {
  constructor (props) {
    // Pass props to parent class
    super(props)
    // Set initial state
    this.state = {
      // Data inputs
      dataInputs: {
        numberOfEmployees: 1,
        DailyAmount: 1000,
      },

      // Other data inputs
      toggle_results: false,
      loading: true,
      showTypeform: false,

      // Results
      dataOutputs : {
        costWithoutChequeResto: 0,
        discountVat: 0,
        netVatCost: 0,
        reduceNetResultTaxable: 0,
        realCost: 0,
        dailySavings: 0,
        percentOfSaving: 0,
      }
    }

    this.toggleResultsChange = this.toggleResultsChange.bind(this)
    this.nbEmployeeChange = this.nbEmployeeChange.bind(this)
    this.voucherChange = this.voucherChange.bind(this)
    this.handleClickShowMore = this.handleClickShowMore.bind(this)

    this.onClickTypeform = this.onClickTypeform.bind(this)
    this.onClickSendEmail = this.onClickSendEmail.bind(this)
    this.onClickDownloadPdf = this.onClickDownloadPdf.bind(this)
    this.closeTypeform = this.closeTypeform.bind(this)
    this.createIframe = this.createIframe.bind(this)

    window.dataLayer = window.dataLayer || []

    this._getDataParams()
  }

  /* UTILS */
  _getDataParams () {
    let url = document.location.origin + '/wp-json/acf/v2/options/'
    // let url = document.location.origin + '/front-wp-dev/wp-json/acf/v2/options/'
    if (__DEV__) {
      url = 'http://10.224.86.238:3000/wp-json/acf/v2/options/'
      // url = 'http://10.0.75.1:3000/front-wp-dev/wp-json/acf/v2/options/'
    }

    return axios({
      method:'get',
      url: url,
    })
    .then((response) => {
      if (response.status === 200) {
        return this.setState({ dataParams: response.data.acf }, () => {
          this.simulator = new ChileSimulator(
            this.state
          )

          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            costWithoutChequeResto: this.simulator.dataOutputs['costWithoutChequeResto'],
            discountVat: -this.simulator.dataOutputs['discountVat'],
            netVatCost: this.simulator.dataOutputs['netVatCost'],
            realCost: this.simulator.dataOutputs['realCost'],
            dailySavings: this.simulator.dataOutputs['dailySavings'],
            percentOfSaving: this.simulator.dataOutputs['percentOfSaving'],
          } })

          this.setState({
            typeform: this.state.dataParams['form_simulator_typeform_url'],
            ctaSendPerEmailLabel:  this.state.dataParams['pdf_modal_title'],
            ctaBeContactedLabel:  this.state.dataParams['simulator_be_contacted_label'],
            ctaDownloadLabel:  this.state.dataParams['simulator_download_label'],
            ctaPLaceanOrderLabel:  this.state.dataParams['simulator_place_an_order_label'],
            ctaPLaceanOrderLink:  this.state.dataParams['simulator_place_an_order_link'],
            ctaDisclaimer:  this.state.dataParams['simulator_legend'],

            dataOutputs: newDataOutputs,
            loading: false
          })
        })
      }
    })
  }

  _updateSimultorState (value) {
    window.dataLayer.push({ 'event':'click-simulator' })
    const newDataInput = update(this.state.dataInputs, { $merge: value })
    this.setState({ dataInputs: newDataInput }, () => {
      this.simulator = new ChileSimulator(
        this.state
      )

      const newDataOutputs = update(this.state.dataOutputs, { $merge: {
        costWithoutChequeResto: this.simulator.dataOutputs['costWithoutChequeResto'],
        discountVat: -this.simulator.dataOutputs['discountVat'],
        netVatCost: this.simulator.dataOutputs['netVatCost'],
        realCost: this.simulator.dataOutputs['realCost'],
        dailySavings: this.simulator.dataOutputs['dailySavings'],
        percentOfSaving: this.simulator.dataOutputs['percentOfSaving'],
      } })

      this.setState({ dataOutputs: newDataOutputs }, () => {
        if (this.state.toggle_results) {
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            costWithoutChequeResto: this.simulator.dataOutputs['costWithoutChequeResto'] * 12,
            discountVat: -this.simulator.dataOutputs['discountVat'] * 12,
            netVatCost: this.simulator.dataOutputs['netVatCost'] * 12,
            realCost: this.simulator.dataOutputs['realCost'] * 12,
            dailySavings: this.simulator.dataOutputs['dailySavings'] * 12,
            percentOfSaving: this.simulator.dataOutputs['percentOfSaving'],
          } })
          this.setState({ dataOutputs: newDataOutputs })
        }
      })
    })
  }

  /* ON CHANGE */
  toggleResultsChange (value) {
    this.setState({ toggle_results: !this.state.toggle_results }, () => {
      this.simulator = new ChileSimulator(
        this.state
      )

      const newDataOutputs = update(this.state.dataOutputs, { $merge: {
        costWithoutChequeResto: this.simulator.dataOutputs['costWithoutChequeResto'],
        discountVat: -this.simulator.dataOutputs['discountVat'],
        netVatCost: this.simulator.dataOutputs['netVatCost'],
        realCost: this.simulator.dataOutputs['realCost'],
        dailySavings: this.simulator.dataOutputs['dailySavings'],
        percentOfSaving: this.simulator.dataOutputs['percentOfSaving'],
      } })

      this.setState({ dataOutputs: newDataOutputs }, () => {
        if (this.state.toggle_results) {
          const newDataOutputs = update(this.state.dataOutputs, { $merge: {
            costWithoutChequeResto: this.simulator.dataOutputs['costWithoutChequeResto'] * 12,
            discountVat: -this.simulator.dataOutputs['discountVat'] * 12,
            netVatCost: this.simulator.dataOutputs['netVatCost'] * 12,
            realCost: this.simulator.dataOutputs['realCost'] * 12,
            dailySavings: this.simulator.dataOutputs['dailySavings'] * 12,
            percentOfSaving: this.simulator.dataOutputs['percentOfSaving'],
          } })
          this.setState({ dataOutputs: newDataOutputs })
        }
      })
    })
  }

  nbEmployeeChange (value) {
    return this._updateSimultorState({ numberOfEmployees: value })
  }

  voucherChange (value) {
    return this._updateSimultorState({ DailyAmount: value })
  }

  handleClickShowMore () {
    this.setState({ showMore: !this.state.showMore }, () => {
      // if (this.state.showMore) {
      //   document.getElementById('root').classList.add('overlay')
      // } else {
      //   document.getElementById('root').classList.remove('overlay')
      // }
    })
  }

  getFullResults (className, width, height, data) {
    return (
      <div id='box' className={className} key='key'>
        <div className={'results__header'}>
          <h3 className='results-title simulator-title'>Resultado:</h3>
          <div className='results-toggle'>
            <Toggle
              trueLabel={'Mensual'}
              falseLabel={'Anual'}
              defaultChecked={this.state.toggle_results}
              onChange={(value) => { this.toggleResultsChange(value) }}
            />
          </div>
        </div>
        <ul className='results-list'>
          <li>
            <p className='results-label'>{'Ahorro IVA 19%'}</p>
            <p className='results-value'>$ { this.state.dataOutputs.discountVat.toLocaleString('es-CL', { style: 'decimal', minimumFractionDigits: 0, maximumFractionDigits: 0 }) } </p>
          </li>
          <li>
            <p className='results-label'>{'Ahorro impuesto a la renta 25%'}</p>
            <p className='results-value'>$ { this.state.dataOutputs.netVatCost.toLocaleString('es-CL', { style: 'decimal', minimumFractionDigits: 0, maximumFractionDigits: 0 }) } </p>
          </li>
          <li>
            <p className='results-label'>{'Con Cheque Restaurant ahorra $'}</p>
            <p className='results-value'>$ { this.state.dataOutputs.dailySavings.toLocaleString('es-CL', { style: 'decimal', minimumFractionDigits: 0, maximumFractionDigits: 0 }) } </p>
          </li>
          <li>
            <p className='results-label'>Con Cheque Restaurant ahorra %</p>
            <p className='results-value'>{ this.state.dataOutputs.percentOfSaving.toLocaleString('es-CL') } %</p>
          </li>
        </ul>

        <Bar
          className={className + '_bar'}
          width={width}
          height={height}
          data={data}
          duration={1500}
          customTextBefore={''}
        />

      </div>
    )
  }

  // CTA PART

  _toQueryString (obj) {
    let parts = []
    for (var i in obj) {
      if (obj.hasOwnProperty(i)) {
        parts.push(encodeURIComponent(i) + '=' + encodeURIComponent(obj[i]))
      }
    }
    return parts.join('&')
  }

  onClickSendEmail (event) {
    event.preventDefault()
    let data = {
      simulator: 'chile',
      annual: this.state.toggle_results,
      numberofemployees: this.state.dataInputs.numberOfEmployees,
      dailyamount: this.state.dataInputs.DailyAmount,
      costwithoutchequeresto: this.simulator.dataOutputs['costWithoutChequeResto'],
      discountvat: this.simulator.dataOutputs['discountVat'],
      netvatcost: this.simulator.dataOutputs['netVatCost'],
      realcost: this.simulator.dataOutputs['realCost'],
      dailysavings: this.simulator.dataOutputs['dailySavings'],
      percentofsaving: this.simulator.dataOutputs['percentOfSaving']
    }

    data = this._toQueryString(data)
    document.getElementById('mail-pdf-form').action = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickDownloadPdf (event) {
    event.preventDefault()
    window.dataLayer.push({ 'event': 'download', 'name':'PDF simulador Chile' })
    let data = {
      simulator: 'chile',
      // annual: this.state.toggle_results,
      numberofemployees: this.state.dataInputs.numberOfEmployees,
      dailyamount: this.state.dataInputs.DailyAmount,
      costwithoutchequeresto: this.simulator.dataOutputs['costWithoutChequeResto'],
      discountvat: this.simulator.dataOutputs['discountVat'],
      netvatcost: this.simulator.dataOutputs['netVatCost'],
      realcost: this.simulator.dataOutputs['realCost'],
      dailysavings: this.simulator.dataOutputs['dailySavings'],
      percentofsaving: this.simulator.dataOutputs['percentOfSaving']
    }

    data = this._toQueryString(data)
    window.location.href = document.location.origin + this.state.dataParams['pdf_action_url_adress'] + '?' + data
  }

  onClickTypeform (event) {
    event.preventDefault()
    this.setState({ showTypeform: true })
    window.dataLayer.push({ 'event':'click-contactus', 'location':'simulator' })
  }

  closeTypeform (event) {
    event.preventDefault()
    this.setState({ showTypeform: false })
  }

  createIframe () {
    let data = {
      // simulator: 'chile',
      // annual: this.state.toggle_results,
      numberofemployees: this.state.dataInputs.numberOfEmployees,
      dailyamount: this.state.dataInputs.DailyAmount,
      costwithoutchequeresto: this.simulator.dataOutputs['costWithoutChequeResto'],
      discountvat: this.simulator.dataOutputs['discountVat'],
      netvatcost: this.simulator.dataOutputs['netVatCost'],
      realcost: this.simulator.dataOutputs['realCost'],
      dailysavings: this.simulator.dataOutputs['dailySavings'],
      percentofsaving: this.simulator.dataOutputs['percentOfSaving']
    }

    data = this._toQueryString(data)
    return {
      __html: '<iframe src="' + this.state.typeform + '?' + data + '" width="540" height="450"></iframe>'
    }
  }

  render () {
    let showMoreClass = this.state.showMore ? 'open' : ''
    // let showMoreText = this.state.showMore ? '-' : '+'
    let showMoreCheuvronClass = this.state.showMore ? '' : 'bottom'

    let dataBar = [
      {
        class: 'bar-left',
        up: '$ ' + this.state.dataOutputs.realCost.toLocaleString('es-CL', { style: 'decimal', minimumFractionDigits: 0, maximumFractionDigits: 0 }),
        down: 'Costo con Cheque Restaurant',
        value: this.state.dataOutputs.realCost,
      },
      {
        class: 'bar-right',
        up: '$ ' + this.state.dataOutputs.costWithoutChequeResto.toLocaleString('es-CL', { style: 'decimal', minimumFractionDigits: 0, maximumFractionDigits: 0 }),
        down: 'Costo sin ahorro',
        value: this.state.dataOutputs.costWithoutChequeResto,
      }
    ]

    let component = this.state.showMore ? this.getFullResults('results__mobile_full', 350, 200, dataBar) : ''
    let showTypeform = this.state.showTypeform ? 'show-typeform' : ''
    let showTypeformOverlay = this.state.showTypeform ? 'overlay' : ''

    if (this.state.loading) {
      return (
        <div className='loading'>
          <p>Loading...</p>
        </div>
      )
    } else {
      return (
        <div>
          <section className={'simulator chile ' + showTypeformOverlay}>

            <div className={'settings'}>
              <div className='container_simulator' id='scrollbar'>

                <div className='settings__block'>
                  <CounterWithIcons
                    className='settings__block-employees'
                    text='Cantidad de empleados'
                    icons={[people1, people2, people3]}
                    more='Plafond'
                    value={this.state.dataInputs.numberOfEmployees}
                    min={1}
                    max={1000}
                    step={1}
                    onChange={(value) => { this.nbEmployeeChange(value) }}
                  />

                  <CounterWithIcons
                    className='settings__block-voucher'
                    text='Valor diario de colación ($)'
                    icons={[money1, money2, money3]}
                    more="Plafond d'éxonération 8,97€"
                    value={this.state.dataInputs.DailyAmount}
                    min={0}
                    max={20000}
                    step={50}
                    onChange={(value) => { this.voucherChange(value) }}
                    />
                </div>
              </div>
            </div>

            <div className={'results'}>
              <div className={'results__mobile ' + showMoreClass}>
                <div className='results__mobile_summary'>
                  <BarSummaryChart
                    className={'results__mobile_summary_bar'}
                    width={90}
                    height={70}
                    data={dataBar}
                    duration={1200}
                  />

                  <div className='results__mobile_summary_content'>
                    <span className='results__mobile_summary_content-text'>Your taxes save is</span>
                    <span className='results__mobile_summary_content-percentage'>{ this.state.dataOutputs.percentOfSaving.toLocaleString('es-CL', { style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 }) } %</span>
                  </div>
                </div>

                <ReactCSSTransitionGroup
                  transitionName='slide'
                  transitionAppear
                  transitionAppearTimeout={500}
                  transitionEnterTimeout={300}
                  transitionLeaveTimeout={300}
                >
                  {component}
                </ReactCSSTransitionGroup>
                <div className={'results__show-more ' + showMoreClass} onClick={this.handleClickShowMore}>
                  <span className={'chevron ' + showMoreCheuvronClass} />
                </div>
              </div>

              { this.getFullResults('results__full', 350, 200, dataBar) }

            </div>

            <div className={'iframe-typeform ' + showTypeform}>
              <div dangerouslySetInnerHTML={this.createIframe()} />
              <a className='product-close' onClick={this.closeTypeform} href='#'><i className='icon-close'></i></a>
            </div>
          </section>

          <section className='callToAction'>
            <div className='callToAction-links'>
              <div className='callToAction-groupLinks'>
                <a href='#' onClick={this.onClickDownloadPdf} className='callToAction-link'>{this.state.ctaDownloadLabel}</a>
                <a href='#' className='callToAction-link' onClick={this.onClickSendEmail} data-toggle='modal' data-target='#sendmailmodal'>{this.state.ctaSendPerEmailLabel}</a>
              </div>
              <div className='callToAction-groupLinks'>
                <a href='#' className='callToAction-link btn-sodexo btn-sodexo-white' onClick={this.onClickTypeform}>{this.state.ctaBeContactedLabel}</a>
                <a href={this.state.ctaPLaceanOrderLink} className='callToAction-link callToAction-link-chile callToAction-link-order btn-sodexo btn-sodexo-red' target='_blank'>{this.state.ctaPLaceanOrderLabel}</a>
              </div>
            </div>
            <p className='callToAction-disclaimer'>{this.state.ctaDisclaimer}</p>
          </section>
        </div>
      )
    }
  }
}
export default ChileApp
