export default class ChileSimulator {
  constructor (state) {
    // var this.dataInputs = {
    //   numberOfEmployees:  1,
    //   DailyAmount:  4500,
    // }

    this.dataInputs = state.dataInputs

    /* End mock for Inputs Data */

    /* Mock for Params Data */

    this.dataParams = {
      dayPerMonth: state.dataParams.days_per_month
    }

    /* END mock for Params Data */

    this.dataOutputs = []

    this.dataOutputs['costWithoutChequeResto'] = this.dataInputs.numberOfEmployees * this.dataInputs.DailyAmount * this.dataParams['dayPerMonth']
    this.dataOutputs['discountVat'] = -this.dataOutputs['costWithoutChequeResto'] + (this.dataOutputs['costWithoutChequeResto']/1.19)
    this.dataOutputs['netVatCost'] = this.dataOutputs['discountVat'] + this.dataOutputs['costWithoutChequeResto']
    this.dataOutputs['reduceNetResultTaxable'] = -this.dataOutputs['netVatCost'] * 0.255
    this.dataOutputs['realCost'] = this.dataOutputs['netVatCost'] + this.dataOutputs['reduceNetResultTaxable']
    this.dataOutputs['dailySavings'] = this.dataOutputs['costWithoutChequeResto'] - this.dataOutputs['realCost']
    this.dataOutputs['percentOfSaving'] = Number((this.dataOutputs['dailySavings'] / this.dataOutputs['costWithoutChequeResto'] * 100 ).toFixed(2))
  }
}
