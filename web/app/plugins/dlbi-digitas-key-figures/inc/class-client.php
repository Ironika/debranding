<?php

class DigitasKeyFigures_Client {

    public function __construct() {
        add_action('init', array($this, 'digitas_key_figure_register_cpt'));
    }

    /**
     * Register the key_figure post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_key_figure_register_cpt() {
        register_post_type(DIG_KEY_PTYPE, array(
            'labels' => array(
                'name' => __('Key figures', 'digitas_key_figure'),
                'singular_name' => __('Key figure', 'digitas_key_figure'),
                'add_new' => __('Add new', 'digitas_key_figure'),
                'add_new_item' => __('Add new Key figure', 'digitas_key_figure'),
                'edit_item' => __('Edit Key figure', 'digitas_key_figure'),
                'new_item' => __('New Key figure', 'digitas_key_figure'),
                'view_item' => __('Show Key figure', 'digitas_key_figure'),
                'search_items' => __('Search Key figures', 'digitas_key_figure'),
                'not_found' => __('No Key figures found', 'digitas_key_figure'),
                'not_found_in_trash' => __('No FAQ Key figures in trash', 'digitas_key_figure'),
                'parent_item_colon' => __('Parent Key figure', 'digitas_key_figure'),
            ),
            'description' => '',
            'publicly_queryable' => true,
            'exclude_from_search' => true,
            'map_meta_cap' => true,
            'capability_type' => 'post',
            'public' => true,
            'hierarchical' => false,
            'rewrite' => array(
                'slug' => DIG_KEY_PTYPE,
                'with_front' => true,
                'pages' => true,
                'feeds' => true,
            ),
            'has_archive' => true,
            'query_var' => DIG_KEY_PTYPE,
            'supports' => array(
                0 => 'title',
                1 => 'editor',
                2 => 'thumbnail',
                3 => 'excerpt',
            ),
            'show_ui' => true,
            'menu_position' => 25,
            'menu_icon' => 'dashicons-admin-network',
            'can_export' => true,
            'show_in_nav_menus' => false,
            'show_in_menu' => true,
        ));

        return true;
    }
}
