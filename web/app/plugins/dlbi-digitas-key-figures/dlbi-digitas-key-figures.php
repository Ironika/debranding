<?php

/*
  Plugin Name: Digitas Key figures
  Description: Plugin for manage Key figures custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-key-figures
 */

require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-key-figures', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-key-figures' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_KEY_VERSION', '1.0');
define('DIG_KEY_URL', plugins_url('', __FILE__));
define('DIG_KEY_DIR', dirname(__FILE__));
define('DIG_KEY_PTYPE', 'key-figure');

//Load class
require( DIG_KEY_DIR . '/inc/class-client.php' );

//Init
function digitas_key_figures_init() {
    new DigitasKeyFigures_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-key-figures', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'digitas_key_figures_init', 11);
