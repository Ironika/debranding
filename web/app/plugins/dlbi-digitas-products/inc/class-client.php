<?php

class DigitasProduct_Client {

    public function __construct() {
	add_action('init', array($this, 'digitas_product_register_cpt'));
	add_action('init', array($this, 'digitas_product_register_taxonomies'));
	/* add_filter('post_type_link', array($this, 'digitas_remove_slug'), 10, 3);
	  add_action('pre_get_posts', array($this, 'digitas_parse_request')); */
    }

    /**
     * Register the product post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_product_register_cpt() {

	$slug_products = get_field('url_product', 'option') ? get_field('url_product', 'option') : DIG_PRO_PTYPE;
	register_post_type(DIG_PRO_PTYPE, array(
	    'labels' => array(
		'name' => __('Products', 'dlbi-digitas-products'),
		'singular_name' => __('Products', 'dlbi-digitas-products'),
		'add_new' => __('Add new', 'dlbi-digitas-products'),
		'add_new_item' => __('Add new Product', 'dlbi-digitas-products'),
		'edit_item' => __('Edit Product', 'dlbi-digitas-products'),
		'new_item' => __('New Product', 'dlbi-digitas-products'),
		'view_item' => __('Show Product', 'dlbi-digitas-products'),
		'search_items' => __('Search Products', 'dlbi-digitas-products'),
		'not_found' => __('No Products found', 'dlbi-digitas-products'),
		'not_found_in_trash' => __('No Products found in trash', 'dlbi-digitas-products'),
		'parent_item_colon' => __('Parent Product', 'dlbi-digitas-products'),
	    ),
	    'description' => '',
	    'publicly_queryable' => true,
	    'exclude_from_search' => false,
	    'map_meta_cap' => true,
	    'capability_type' => 'post',
	    'public' => true,
	    'hierarchical' => false,
	    'rewrite' => array(
		'slug' => $slug_products,
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
		'hierarchical' => true,
	    ),
	    'has_archive' => false,
	    //'query_var' => $slug_products,
	    'supports' => array(
		0 => 'title',
		1 => 'editor',
		2 => 'thumbnail',
		3 => 'excerpt',
	    ),
	    'show_ui' => true,
	    'menu_position' => 25,
	    'menu_icon' => 'dashicons-cart',
	    'can_export' => true,
	    'show_in_nav_menus' => true,
	    'show_in_menu' => true,
	));

	flush_rewrite_rules();

	return true;
    }

    /**
     * Register the product taxonomies
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_product_register_taxonomies() {
	// create a new taxonomy
	register_taxonomy(
		__('category-product', 'dlbi-digitas-products'), DIG_PRO_PTYPE, array(
	    'label' => __('Category', 'dlbi-digitas-products'),
	    'rewrite' => array(
		'slug' => __('category-product', 'dlbi-digitas-products'),
		'hierarchical' => 'true'),
	    'capabilities' => array(
		'assign_terms' => 'edit_guides',
		'edit_terms' => 'publish_guides'
	    ),
	    'hierarchical' => true,
	    'publicly_queryable' => false,
		)
	);

	return true;
    }

    public static function digitas_remove_slug($post_link, $post, $leavename) {

	$slug_products = get_field('url_product', 'option') ? get_field('url_product', 'option') : DIG_PRO_PTYPE;

	if (DIG_PRO_PTYPE != $post->post_type || 'publish' != $post->post_status) {
	    return $post_link;
	}
	$categories = wp_get_post_terms($post->ID, 'category-product');
	if ($categories) {
	    if (count($categories) == 1) {
		$cat_slug = $categories[0]->slug . '/';
	    } elseif (count($categories) == 2) {
		$cat_slug = $categories[0]->slug . '/' . $categories[1]->slug . '/';
	    }
	} else {
	    $cat_slug = '';
	}
	$post_link = str_replace('/' . $slug_products . '/', '/' . $cat_slug, $post_link);

	return $post_link;
    }

    public static function digitas_parse_request($query) {

	if (!$query->is_main_query() || 2 != count($query->query) || !isset($query->query['page'])) {
	    return;
	}

	if (!empty($query->query['name'])) {
	    $query->set('post_type', array('post', DIG_PRO_PTYPE, DIG_FAQ_PTYPE, DIG_NEED_PTYPE, DIG_TES_PTYPE, 'page'));
	}
	flush_rewrite_rules();
    }

}
