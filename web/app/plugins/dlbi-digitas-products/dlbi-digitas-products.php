<?php

/*
  Plugin Name: Digitas Products
  Description: Plugin for manage products custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-products
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-products', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-products' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_PRO_VERSION', '1.0');
define('DIG_PRO_URL', plugins_url('', __FILE__));
define('DIG_PRO_DIR', dirname(__FILE__));
define('DIG_PRO_PTYPE', 'product');

//Load class
require( DIG_PRO_DIR . '/inc/class-client.php' );

//Init
function digitas_product_init() {
    new DigitasProduct_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-products', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'digitas_product_init', 11);