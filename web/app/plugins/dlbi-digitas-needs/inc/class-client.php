<?php

class DigitasNeeds_Client {

    public function __construct() {
	add_action('init', array($this, 'digitas_need_register_cpt'));
	add_filter('acf/settings/save_json', array($this, 'digitas_acf_json_save_point'));
	add_filter('acf/settings/load_json', array($this, 'digitas_acf_json_load_point'));
    }

    public function digitas_acf_json_save_point($path) {
	// update path
	$path = DIG_NEED_DIR . '/acf-json';
	// return
	return $path;
    }

    public function digitas_acf_json_load_point($paths) {
	// remove original path (optional)
	unset($paths[0]);

	// append path
	$paths[] = DIG_NEED_DIR . '/acf-json';

	// return
	return $paths;
    }

    /**
     * Register the need post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_need_register_cpt() {

	$slug_needs = get_field('url_needs', 'option') ? get_field('url_needs', 'option') : DIG_NEED_PTYPE;
	register_post_type(
		DIG_NEED_PTYPE, array(
	    'labels' => array(
		'name' => __('Needs', 'dlbi-digitas-needs'),
		'singular_name' => __('Need', 'dlbi-digitas-needs'),
		'add_new' => __('Add new', 'dlbi-digitas-needs'),
		'add_new_item' => __('Add new Need', 'dlbi-digitas-needs'),
		'edit_item' => __('Edit Need', 'dlbi-digitas-needs'),
		'new_item' => __('New Need', 'dlbi-digitas-needs'),
		'view_item' => __('Show Need', 'dlbi-digitas-needs'),
		'search_items' => __('Search Needs', 'dlbi-digitas-needs'),
		'not_found' => __('No Needs found', 'dlbi-digitas-needs'),
		'not_found_in_trash' => __('No Needs found in trash', 'dlbi-digitas-needs'),
		'parent_item_colon' => __('Parent Need', 'dlbi-digitas-needs'),
	    ),
	    'description' => '',
	    'publicly_queryable' => true,
	    'exclude_from_search' => false,
	    'map_meta_cap' => true,
	    'capability_type' => 'post',
	    'public' => true,
	    'hierarchical' => false,
	    'rewrite' => array(
		'slug' => $slug_needs,
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	    ),
	    'has_archive' => false,
	    'query_var' => DIG_NEED_PTYPE,
	    'supports' => array(
		0 => 'title',
		1 => 'editor',
		2 => 'thumbnail',
		3 => 'excerpt',
	    ),
	    'show_ui' => true,
	    'menu_position' => 25,
	    'menu_icon' => 'dashicons-admin-post',
	    'can_export' => true,
	    'show_in_nav_menus' => true,
	    'show_in_menu' => true,
		)
	);

	return true;
    }

}
