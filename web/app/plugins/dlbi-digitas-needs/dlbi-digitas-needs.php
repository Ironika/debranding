<?php

/*
  Plugin Name: Digitas Needs
  Description: Plugin for manage Needs custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-needs
 */

require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-needs', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-needs' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_NEED_VERSION', '1.0');
define('DIG_NEED_URL', plugins_url('', __FILE__));
define('DIG_NEED_DIR', dirname(__FILE__));
define('DIG_NEED_PTYPE', 'need');

//Load class
require( DIG_NEED_DIR . '/inc/class-client.php' );

//Init
function digitas_need_init() {
    new DigitasNeeds_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-needs', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'digitas_need_init', 11);
