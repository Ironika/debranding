<?php

/*
  Plugin Name: Widget Contact US Popin
  Description: Widget Contact US Popin
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-contact-us-popin-widget
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-contact-us-popin-widget', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-contact-us-popin-widge' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_CON_VERSION', '1.0');
define('DIG_CON_URL', plugins_url('', __FILE__));
define('DIG_CON_DIR', dirname(__FILE__));

//Load class
require( DIG_CON_DIR . '/inc/class-contact-us-widget.php' );

//Init widget newletter
function dlbi_digitas_contact_us_init() {
    register_widget('DigitasWidget_Contact_Us');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-contact-us-popin-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_digitas_contact_us_init');
