<?php

class DigitasTestimonials_Client {

    public function __construct() {
	add_action('init', array($this, 'digitas_testimonial_register_cpt'));
	add_action('pre_get_posts', array($this, 'digitas_alter_main_query'));
	add_filter('acf/settings/save_json', array($this, 'digitas_testimonials_acf_json_save_point'));
	add_filter('acf/settings/load_json', array($this, 'digitas_testimonials_acf_json_load_point'));
    }

    /**
     * Register the testimonial post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_testimonial_register_cpt() {

	$slug_testimonials = get_field('url_testimonials', 'option') ? get_field('url_testimonials', 'option') : DIG_TES_PTYPE;
	register_post_type(DIG_TES_PTYPE, array(
	    'labels' => array(
		'name' => __('Testimonial', 'dlbi-digitas-testimonials'),
		'singular_name' => __('Testimonial', 'dlbi-digitas-testimonials'),
		'add_new' => __('Add new', 'dlbi-digitas-testimonials'),
		'add_new_item' => __('Add new Testimonial', 'dlbi-digitas-testimonials'),
		'edit_item' => __('Edit Testimonial', 'dlbi-digitas-testimonials'),
		'new_item' => __('New Testimonial', 'dlbi-digitas-testimonials'),
		'view_item' => __('Show Testimonial', 'dlbi-digitas-testimonials'),
		'search_items' => __('Search Testimonials', 'dlbi-digitas-testimonials'),
		'not_found' => __('No Testimonials found', 'dlbi-digitas-testimonials'),
		'not_found_in_trash' => __('No Testimonials found in trash', 'dlbi-digitas-testimonials'),
		'parent_item_colon' => __('Parent Testimonial', 'dlbi-digitas-testimonials'),
	    ),
	    'description' => '',
	    'publicly_queryable' => true,
	    'exclude_from_search' => false,
	    'map_meta_cap' => true,
	    'capability_type' => 'post',
	    'public' => true,
	    'hierarchical' => false,
	    'rewrite' => array(
		'slug' => $slug_testimonials,
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	    ),
	    'has_archive' => $slug_testimonials,
	    'query_var' => DIG_TES_PTYPE,
	    'supports' => array(
		0 => 'title',
		1 => 'editor',
		2 => 'thumbnail',
		3 => 'excerpt',
	    ),
	    'taxonomies' => array('category', 'post_tag'),
	    'show_ui' => true,
	    'menu_position' => 25,
	    'menu_icon' => 'dashicons-format-quote',
	    'can_export' => true,
	    'show_in_nav_menus' => true,
	    'show_in_menu' => true,
	));


	return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @param WP_Query $query
     * @return boolean
     * @author Digitas LBI
     */
    public function digitas_alter_main_query($query) {
	if ($query->is_post_type_archive(DIG_TES_PTYPE) && $query->is_main_query() && !is_admin()) {
	    $query->set('posts_per_page', 16);
	}
    }

    public function digitas_testimonials_acf_json_save_point($path) {

	// update path
	$path = DIG_TES_DIR . '/acf-json';

	// return
	return $path;
    }

    public function digitas_testimonials_acf_json_load_point($paths) {
	// remove original path (optional)
	unset($paths[0]);

	// append path
	$paths[] = DIG_TES_DIR . '/acf-json';

	// return
	return $paths;
    }

}
