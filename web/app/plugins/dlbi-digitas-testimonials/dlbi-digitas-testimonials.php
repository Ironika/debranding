<?php

/*
  Plugin Name: Digitas Testimonials
  Description: Plugin for manage Testimonials custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-testimonials
 */

require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-testimonials', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-testimonials' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_TES_VERSION', '1.0');
define('DIG_TES_URL', plugins_url('', __FILE__));
define('DIG_TES_DIR', dirname(__FILE__));
define('DIG_TES_PTYPE', 'testimonial');

//Load class
require( DIG_TES_DIR . '/inc/class-client.php' );

//Init
function digitas_testimonial_init() {
    new DigitasTestimonials_Client();

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-testimonials', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'digitas_testimonial_init', 11);
