<?php

/*
  Plugin Name: Custom widgets for Visual Composer Plugin
  Description: Extend Visual Composer with your own set of widgets.
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
 */

require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-vc-widget', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-vc-widget' //Plugin slug. Usually it's the same as the name of the directory.
);


// Plugin Consts
define('DIG_VCWID_VERSION', '1.0');
define('DIG_VCWID_URL', plugins_url('', __FILE__));
define('DIG_VCWID_DIR', dirname(__FILE__));

//Load class
require( DIG_VCWID_DIR . '/inc/class-client.php' );
require( DIG_VCWID_DIR . '/inc/class-admin.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-blog-component.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-boxes.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-carousel.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-carousel-affiliate.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-carousel-they-trust-us.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-choose-digitas.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-cross-sell.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-discover-more.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-discover-more-three-blocks.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-employee-component.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-prod-ctas.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-product-page-info.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-product-page-2nd-ctas.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-single-carousel.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-test.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-testimonials-popin.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-why-become-partner.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-bloc-link.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-store-locator.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-affiliate-info.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-faq.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-discover-more-one-block.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-iframe-store-locator.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-contact-our-adviser.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-documents-needed.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-title-subtitle.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-online-accreditation-form.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-life-quality-mobility.php' );
require( DIG_VCWID_DIR . '/inc/widgets/class-poll.php' );

// don't load directly
if (!defined('ABSPATH'))
    die('-1');

// Check if Visual Composer is installed
if (!defined('WPB_VC_VERSION')) {
    // Display notice that Visual Compser is required
    add_action('admin_notices', 'showVcVersionNotice');
    add_action('plugins_loaded', 'digitas_load_textdomain',12);
    return;
}

// Show notice if your plugin is activated but Visual Composer is not
function showVcVersionNotice() {
    $plugin_data = get_plugin_data(__FILE__);
    echo '
        <div class="updated">
          <p>' . sprintf(__('<strong>%s</strong> requires <strong><a href="http://bit.ly/vcomposer" target="_blank">Visual Composer</a></strong> plugin to be installed and activated on your site.', 'vc_widget'), $plugin_data['Name']) . '</p>
        </div>';
}


//Init
function digitas_vcwid_init() {
    new DigitasVcwid_Client();

    if (is_admin()) {
	new DigitasVcwid_Admin();
    }

}

add_action('plugins_loaded', 'digitas_vcwid_init', 11);

/**
 * Load plugin textdomain.
 * @since 1.5.2
 */
function digitas_load_textdomain() {
    load_plugin_textdomain( 'dlbi-digitas-vc-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

