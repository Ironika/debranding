<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);

// Caution duplicate of choose-digitas.php
?>

<section class="choose-digitas">

  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <?php if (array_key_exists('widget_title', $atts)) : ?>
            <h2 class="digitas-title"><?php echo $atts['widget_title']; ?></h2>
        <?php endif; ?>
      </diV>
    </div>

    <div class="choose-digitas--desktop">
      <?php if (array_key_exists('titles', $atts)) : ?>
        <div class="row no-gutters align-items-stretch">
                    <?php
                    switch (count($titles)) {
                        case 3:
                        case 5:
                        case 6:
                            $col = "col-4";
                            break;
                        case 4:
                            $col = "col-6";
                            break;
                        default:
                            $col = "col-sm";
                            break;
                    }

                    $uniqid = uniqid(rand());

                    foreach ($titles as $title): ?>

                    <div class="<?php echo $col ?> mt-2"  data-mh="mh-group-<?php echo $uniqid; ?>">
                        <div class="choose-digitas--container">
                            <?php if (array_key_exists('image', $title)): ?>
                                <div class="choose-digitas--container_image">
                                <?php
                                // Get image, title, label and link : 'READ MORE'
                                $image = $title['image'];

                                if (!empty($image)):
                                    echo wp_get_attachment_image($image);
                                endif;
                                ?>
                                </div>
                            <?php endif ?>
                            <div class="choose-digitas--container_subtitle">
                                <p><?php echo $title['subtitle']; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
              </div>
          <?php endif; ?>
        </div>

        <div class="choose-digitas--mobile">

            <?php if (array_key_exists('titles', $atts)) : ?>

                    <div class="choose-digitas-slick">

                        <?php foreach ($titles as $key => $title) :  ?>

                          <div class="choose-digitas--container">
                              <div class="choose-digitas--container_image">
                                  <?php
                                  // Get image, title, label and link : 'READ MORE'
                                  $image = $title['image'];

                                  if (!empty($image)):
                                      echo wp_get_attachment_image($image);
                                  endif;
                                  ?>
                              </div>

                              <div class="choose-digitas--container_subtitle">
                                  <p><?php echo $title['subtitle']; ?></p>
                              </div>
                          </div>

                        <?php endforeach; ?>

                    </div>

            <?php endif; ?>
        </div>


        <?php if (array_key_exists('login_title_link', $atts)): ?>
            <div class="choose-digitas--cta">
                <?php $linkField = vc_build_link($atts['login_link']); ?>
                <a class="btn-digitas btn-digitas-white" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
                <?php $linkField = vc_build_link($atts['become_an_affiliate_link']); ?>
                <a class="btn-digitas btn-digitas-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
            </div>
        <?php endif ?>
    </div>
</section>
