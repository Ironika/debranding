<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);
$uniqid = uniqid(rand());
?>

<section class="choose-digitas">

  <div class="container">

    <div class="row">

      <div class="col-md-12">
        <?php if (array_key_exists('widget_title', $atts)) : ?>
          <h2 class="digitas-title"><?php echo $atts['widget_title']; ?></h2>
        <?php endif; ?>
      </div>

    </div>

    <?php if (array_key_exists('titles', $atts)) : ?>

    <div class="choose-digitas--desktop">

        <div class="row no-gutters align-items-stretch">

          <?php
          $count = count($titles);

          $col = "col-sm";

          if ($count == 3 || $count == 5 || $count == 6) {
            $col = "col-4";
          }

          if ($count == 4) {
            $col = "col-6";
          }

          foreach ($titles as $title) { ?>
            <div class="<?php echo $col ?> mt-2" data-mh="mh-group-<?php echo $uniqid; ?>">
              <div class="choose-digitas--container">
                <?php if (array_key_exists('image', $title)): ?>
                  <div class="choose-digitas--container_image">
                    <?php
                    // Get image, title, label and link : 'READ MORE'
                    $image = $title['image'];

                    if (!empty($image)):
                      echo wp_get_attachment_image($image);
                    endif;
                    ?>
                  </div>
                <?php endif ?>
                <div class="choose-digitas--container_subtitle">
                    <?php if (array_key_exists('subtitle', $title)):?>
                        <p><?php echo $title['subtitle']; ?></p>
                    <?php endif; ?>
                </div>
              </div><!-- .choose-digitas--container -->
            </div><!-- .col- -->
          <?php } ?>

        </div><!-- .row -->

      </div><!-- .choose-digitas--desktop -->

      <div class="choose-digitas--mobile">

        <div class="choose-digitas-slick">

          <?php foreach ($titles as $key => $title) { ?>

            <div class="choose-digitas--container">
              <div class="choose-digitas--container_image">
                <?php
                // Get image, title, label and link : 'READ MORE'
                $image = $title['image'];

                if (!empty($image)):
                  echo wp_get_attachment_image($image);
                endif;
                ?>
              </div>
              <div class="choose-digitas--container_subtitle">
                <?php if (array_key_exists('subtitle', $title)):?>
                    <p><?php echo $title['subtitle']; ?></p>
                <?php endif; ?>
              </div>
            </div>

          <?php } ?>

        </div>

      </div><!-- .choose-digitas--mobile -->

    <?php endif; ?>


    <?php if (array_key_exists('pre-text-link', $atts)): ?>
      <div class="choose-digitas--cta">
        <?php
        $simplelink = 'simplelink';
        $typeform = 'typeform';
        $youtube='youtube';

        $typelink = '';
        if (array_key_exists('type_link', $atts)):
            $typelink = $atts["type_link"];
        endif;

        $linkField = vc_build_link($atts['link']); ?>

        <span><?php echo $atts['pre-text-link']; ?></span>
        <?php if ($typelink == $simplelink):?>
            <a class="btn-digitas btn-digitas-red" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
        <?php elseif ($typelink == $typeform) : ?>
            <a class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
        <?php elseif ($typelink == $youtube) : ?>
            <a class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $linkField['url']; ?>"><?php echo $linkField['title']; ?></a>
        <?php endif; ?>
      </div>
    <?php endif; ?>

  </div>

</section>
