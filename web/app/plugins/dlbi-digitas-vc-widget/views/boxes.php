<?php
$titles = vc_param_group_parse_atts($atts['titles']);
?>
<?php if (array_key_exists('top-title', $atts)) : ?>
    <h2><?php echo $atts['top-title']; ?></h2>
<?php endif ?>
<div class="digitas-widget-boxes">
    <?php if ($titles): ?>
        <div class="digitas-widget-boxes-container">

	    <?php foreach ($titles as $title) { ?>

		<div class="box">
		    <div class="box-image">
			<?php
			if (array_key_exists('img_size', $title)) :
			    echo wp_get_attachment_image($title['img_size']);
			endif;
			?>
		    </div>
		    <h3 class="box-title"><?php echo $title['title']; ?></h3>
		    <p class="box-text"><?php echo $title['text']; ?></p>
		    <div class="box-line"></div>
		</div>

	    <?php } ?>

        </div>
    <?php endif ?>

</div>
