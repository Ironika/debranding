<section class="poll-digitas-vc-widget">

  <div class="container">

    <div class="row">

      <div class="col-12">

        <?php if (array_key_exists('digitas_poll_title', $atts)) : ?>
          <h2 class="digitas-title"><?php echo $atts['digitas_poll_title']; ?></h2>
        <?php endif ?>
        <?php if (array_key_exists('digitas_poll_id', $atts)) : ?>
          <?php echo do_shortcode('[poll id="' . $atts['digitas_poll_id']. '"]'); ?>
        <?php endif ?>

      </div>

    </div>

  </div>

</section>
