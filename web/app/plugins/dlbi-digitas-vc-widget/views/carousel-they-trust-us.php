<section class="carousel-theytrustus">

  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <?php if (array_key_exists('widget_title', $atts)) : ?>
          <h2 class="digitas-title"><?php echo $atts['widget_title']; ?></h2>
        <?php endif; ?>
      </div>
    </div>

    <?php if (array_key_exists('logos', $atts)) : $logos = explode(',', $atts['logos']); ?>

      <div class="row">

        <div class="col-md-12">

          <div class="carousel-theytrustus-slick">
          <?php foreach ($logos as $logo) : ?>
              <?php if (!empty($logo)): ?>

                <div>
                    <div class="carousel-theytrustus--container">
                        <div class="carousel-theytrustus--container_image">
                            <?php echo wp_get_attachment_image($logo, 'thumbnail'); ?>
                        </div>
                    </div>
                </div>

              <?php endif; ?>
          <?php endforeach; ?>
          </div>

        </div>

      </div>

    <?php endif; ?>

  </div>

</section>
