<?php
//List of attributs
$titles = vc_param_group_parse_atts($atts['titles']);
?>

<section class="single-carousel">

    <div class="container">

	<?php if (array_key_exists('titles', $atts)) : ?>

    	<div class="row">

    	    <div class="col-md-12">

    		<div class="single-carousel-slick">

			<?php foreach ($titles as $key => $title) : ?>

			    <div>

				<div class="single-carousel--container">

				    <div class="single-carousel--container_number">
					<p class="number_value"><?php echo $title['value']; ?> </p>
					<span class="number_unit"><?php echo $title['unit']; ?> </span>
				    </div>

				    <div class="single-carousel--container_text">
					<p><?php echo $title['description']; ?></p>
					<?php if (array_key_exists('link', $title)): ?>
					    <?php
					    $linkField = vc_build_link($title['link']);
					    if (!empty($linkField['url']) && !empty($linkField['title'])):
						?>
						<a class="btn-digitas btn-digitas-white" href="<?php echo $linkField['url']; ?>" title="<?php echo $linkField['title']; ?>" <?php if ($linkField['target']) : ?>target="_blank"<?php endif; ?>><?php echo $linkField['title']; ?></a>
					    <?php endif ?>
					<?php endif ?>
				    </div>

				</div>

			    </div>

			<?php endforeach; ?>

    		</div><!-- .single-carousel-slick -->

    	    </div><!-- #single-carousel -->

    	</div>

	<?php endif; ?>

    </div>

</section>
