<?php
// Titre
$title = $atts['title'];
// Descrition
$desc = $atts['description'];
// URL ['url'] and ['title']
$link = vc_build_link($atts['link']);
// Image
$image_id = $atts['image'];
// Position
$position = $atts['position'];

$blocposition = 'lqm-left';
if ($position=="right") {
  $blocposition = "lqm-right";
}

?>

<section class="life-quality-mobility <?php echo $blocposition; ?>">

  <div class="row no-gutters align-items-stretch">

    <div class="col-12 col-md-6">

      <div class="life-quality-mobility_bgimage" style="background-image:url('<?php echo wp_get_attachment_url($image_id, 'large'); ?>');">
      </div>

    </div>

    <div class="col-12 col-md-6 align-self-center">

      <div class="life-quality-mobility_content">

        <?php if (array_key_exists('title', $atts)) : ?>
          <h3 class="digitas-title digitas-title--white"><?php echo $title; ?></h3>
        <?php endif; ?>
        <p><?php echo $desc; ?></p>
        <a class="btn-digitas btn-digitas-white" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>

      </div>

    </div>

  </div>

</section>
