<?php

class DigitasWidget_OnlineAccreditationForm {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Digitas Online Accreditation Form';
    // Widget slug
    var $base = 'digitas_online_accreditation_form';
    // Widget category
    var $category = 'Digitas';
    // Widget class
    var $class = 'digitas-online-accreditation-form';
    // Widget description
    var $description = 'Display online accreditation form';
    // Widget view
    var $view = DIG_VCWID_DIR . '/views/online-accreditation-form.php';

    function __construct() {
	$this->digitas_set_widget_params();
	$this->lbidigitas_set_widget_params();
	// Hooks for grid builder
	add_shortcode($this->base, array($this, 'digitas_grid_render'));
	add_filter('vc_grid_item_shortcodes', array($this, 'digitas_widget_add_grid_shortcodes'));

	//Hooks for element
	add_action('vc_before_init', array($this, 'digitas_widget_add_element'));
	add_shortcode($this->base, array($this, 'digitas_element_render'));
    }

    /**
     * Set widget params
     */
    function lbidigitas_set_widget_params() {
	$this->widget_params = array(
	    'name' => __($this->name, 'js_composer'),
	    'base' => $this->base,
	    'class' => $this->class,
	    'category' => __($this->category, "js_composer"),
	    'description' => __($this->description, 'js_composer'),
	    'params' => $this->params,
	    'icon' => 'http://fr.digitas.com/modules/digitascom-templates/img/digitas-favicon.ico'
	);
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function digitas_set_widget_params() {
	$this->params = array(
	    // params group
	    array(
		"type" => "textfield",
		"heading" => __("Title", "js_composer"),
		"param_name" => "title",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("Description", "js_composer"),
		"param_name" => "description",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("TypeForm URL", "js_composer"),
		"param_name" => "typeform-url",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("TypeForm width", "js_composer"),
		"param_name" => "typeform-width",
	    ),
	    array(
		"type" => "textfield",
		"heading" => __("TypeForm height", "js_composer"),
		"param_name" => "typeform-height",
	    )
		)
	;
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function digitas_widget_add_grid_shortcodes($shortcodes) {
	$params = $this->widget_params;
	$params['post_type'] = Vc_Grid_Item_Editor::postType();

	$shortcodes[$this->base] = $params;

	return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function digitas_widget_add_element() {
	// Map the block with vc_map()
	vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function digitas_grid_render($atts) {
	DigitasVcwid_Client::digitas_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function digitas_element_render($atts) {
	DigitasVcwid_Client::digitas_widget_render($atts, $this->view);
    }

}

new DigitasWidget_OnlineAccreditationForm();
