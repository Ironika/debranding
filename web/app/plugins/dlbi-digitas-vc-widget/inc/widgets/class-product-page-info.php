<?php

class DigitasWidget_ProductInfo {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Product Page Info Widget';
    // Widget slug
    var $base = 'product_page_info_widget';
    // Widget category
    var $category = 'Digitas';
    // Widget class
    var $class = 'product-page-info-widget';
    // Widget description
    var $description = 'Product Page Info widget.';
    // Widget view
    var $view = DIG_VCWID_DIR . '/views/product-info.php';

    function __construct() {
    	// Here put on argument your custom post slug
    	$this->digitas_set_widget_params();
    	$this->lbidigitas_set_widget_params();
    	// Hooks for grid builder
    	add_shortcode($this->base, array($this, 'digitas_grid_render'));
    	add_filter('vc_grid_item_shortcodes', array($this, 'digitas_widget_add_grid_shortcodes'));

        //Hooks for element
    	add_action('vc_before_init', array($this, 'digitas_widget_add_element'));
    	add_shortcode($this->base, array($this, 'digitas_element_render'));

    }

    /**
     * Get posts list
     *
     * @param type $post_type
     */
    function digitas_get_posts_list($post_type) {
	   $this->posts_lists = DigitasVcwid_Client::digitas_vcwid_get_posts_values($post_type);
    }

    /**
     * Set widget params
     */
    function lbidigitas_set_widget_params() {
    	$this->widget_params = array(
    	    'name' => __($this->name, 'js_composer'),
    	    'base' => $this->base,
    	    'class' => $this->class,
    	    'category' => __($this->category, "js_composer"),
    	    'description' => __($this->description, 'js_composer'),
    	    'params' => $this->params,
	    'icon'  => 'http://fr.digitas.com/modules/digitascom-templates/img/digitas-favicon.ico'
    	);
    }

    /**
     * Sets params for element and grid builder widget settings
     */
    function digitas_set_widget_params() {
    	$this->params = array(
            array(
                "type" => "textfield",
                "holder" => "",
                "class" => "",
                "heading" => __('Widget Title', 'js_composer'),
                "param_name" => 'widget-title',
                "value" => ''
             ),
    	    // params group
    	    array(
        		'heading' => __('Lines', 'js_composer'),
        		'type' => 'param_group',
        		'value' => '',
        		'param_name' => 'lines',
        		// Note params is mapped inside param-group:
        		'params' => array(
                    array(
                        'type' => "textfield",
                        'holder' => "",
                        'class' => "",
                        'heading' => 'Title',
                        'param_name' => "line",
                        'value' => ''
                    )
        		)
    	    )
    		)
    	;
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function digitas_widget_add_grid_shortcodes($shortcodes) {
    	$params = $this->widget_params;
    	$params['post_type'] = Vc_Grid_Item_Editor::postType();

    	$shortcodes[$this->base] = $params;

    	return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function digitas_widget_add_element() {
        // Map the block with vc_map()
	    vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function digitas_grid_render($atts) {
	   DigitasVcwid_Client::digitas_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function digitas_element_render($atts) {
	   DigitasVcwid_Client::digitas_widget_render($atts, $this->view);
    }

}
new DigitasWidget_ProductInfo();
