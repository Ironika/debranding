<?php

class DigitasWidget_ChooseDigitas {

    // Params for display inputs in widget back-office
    var $params = array();
    // Params of widget
    var $widget_params = array();
    // Display an array of custom posts
    var $posts_lists = array();
    // Widget name
    var $name = 'Choose Digitas Widget (4 colonnes)';
    // Widget slug
    var $base = 'choose_digitas_widget_4_colonnes';
    // Widget category
    var $category = 'Digitas';
    // Widget class
    var $class = 'choose-digitas-widget-4-colonnes';
    // Widget description
    var $description = 'Choose Digitas Widget (4 colonnes).';
    // Widget view
    var $view = DIG_VCWID_DIR . '/views/choose-digitas.php';

    function __construct() {
        $this->digitas_set_widget_params();
        $this->lbidigitas_set_widget_params();
        // Hooks for grid builder
        add_shortcode($this->base, array($this, 'digitas_grid_render'));
        add_filter('vc_grid_item_shortcodes', array($this, 'digitas_widget_add_grid_shortcodes'));

        //Hooks for element
        add_action('vc_before_init', array($this, 'digitas_widget_add_element'));
        add_shortcode($this->base, array($this, 'digitas_element_render'));

    }

    /**
     * Set widget params
     */
    function lbidigitas_set_widget_params() {
        $this->widget_params = array(
            'name' => __($this->name, 'js_composer'),
            'base' => $this->base,
            'class' => $this->class,
            'category' => __($this->category, "js_composer"),
            'description' => __($this->description, 'js_composer'),
            'params' => $this->params,
            'icon'  => 'http://fr.digitas.com/modules/digitascom-templates/img/digitas-favicon.ico'
        );
    }

    /**
     * Load our textdomain for localization.
     *
     * @return void
     */
    public function digitas_textdomain() {
        load_plugin_textdomain( 'dlbi-digitas-vc-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }
    /**
     * Sets params for element and grid builder widget settings
     */
    function digitas_set_widget_params() {
        $this->params = array(
            array(
                'type' => 'textfield',
                'holder' => '',
                'class' => '',
                'heading' => __('Widget Title', 'js_composer'),
                'param_name' => 'widget_title',
                'value' => ''
            ),
             // params group
            array(
                'heading' => __('Titles', 'js_composer'),
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'titles',
                // Note params is mapped inside param-group:
                'params' => array(
                    array(
                        'type' => 'attach_image',
                        'heading' => __('Image', 'js_composer'),
                        'param_name' => 'image',
                        'description' => __('Select image from media library.', 'js_composer')
                    ),
                    array(
                        "type" => "textfield",
                        "holder" => "",
                        "class" => "",
                        "heading" => __('Subtitle', 'js_composer'),
                        "param_name" => 'subtitle',
                        "value" => ''
                    )
                )
            ),
            array(
                "type" => "textfield",
                "heading" => __('Pre-text link', 'js_composer'),
                "param_name" => 'pre-text-link',
                "value" => ''
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Select a link type", "js_composer"),
                "param_name" => "type_link",
                "value" => array(
                    'Simplelink'  => 'simplelink',
                    'Typeform'    => 'typeform',
                    'Youtube'     => 'youtube'
                ),
                'std' => 'Simplelink' // Your default value
            ),
            array(
                "type" => "vc_link",
                "heading" => __('Link', 'js_composer'),
                "param_name" => 'link',
                "value" => ''
            ),
        );
    }

    /**
     * Add widget to grid builder
     *
     * @param array $shortcodes
     * @return type
     */
    public function digitas_widget_add_grid_shortcodes($shortcodes) {
        $params = $this->widget_params;
        $params['post_type'] = Vc_Grid_Item_Editor::postType();

        $shortcodes[$this->base] = $params;

        return $shortcodes;
    }

    /**
     * Add to posts, pages ...
     */
    public function digitas_widget_add_element() {
        // Map the block with vc_map()
        vc_map($this->widget_params);
    }

    /**
     * Grid output function
     *
     * @param type $atts
     */
    public function digitas_grid_render($atts) {
        DigitasVcwid_Client::digitas_widget_render($atts, $this->view);
    }

    /**
     * Element output function
     *
     * @param type $atts
     */
    public function digitas_element_render($atts) {
        DigitasVcwid_Client::digitas_widget_render($atts, $this->view);
    }

}
new DigitasWidget_ChooseDigitas();
