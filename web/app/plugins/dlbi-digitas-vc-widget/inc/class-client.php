<?php

class DigitasVcwid_Client {

    function __construct() {
        add_action('plugins_loaded', array($this, 'digitas_vcwid_get_posts_values'));

        // add_action('wp_enqueue_scripts', array($this, 'digitas_enqueue_scripts'));
    }

    /**
     * Return an array of custom post type contains title and ID
     *
     * @param type $post_type
     * @return type
     */
    public static function digitas_vcwid_get_posts_values($post_type) {

	$args = array(
	    'posts_per_page' => -1,
	    'orderby' => 'title',
	    'order' => 'ASC',
	    'post_type' => $post_type,
	);
	$posts = get_posts($args);
	$posts_list = wp_list_pluck($posts, 'ID','post_title');

	if ($posts_list) {
	    return $posts_list;
	} else {
	    return __('There is a problem !', 'vc_widget');
	}

    }

    /**
     * Load our textdomain for localization.
     *
     * @return void
     */
    public function textdomain() {
        load_plugin_textdomain( 'dlbi-digitas-vc-widget', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
    }

    /**
     *  output function
     *
     * @param type $atts
     * @param type $path
     */
    public static function digitas_widget_render($atts, $path) {
	include($path);
    }

    public static function digitas_enqueue_scripts() {
    	// if(!is_admin()) {
    	//     wp_enqueue_script('widgets-script', SOD_VCWID_DIR . 'dist/scripts/widgets.min.js');
        //     wp_enqueue_style('widgets-style', SOD_VCWID_DIR . 'dist/styles/widgets.min.css');
    	// }
    }

}
