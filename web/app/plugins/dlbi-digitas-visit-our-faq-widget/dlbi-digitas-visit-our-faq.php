<?php

/*
  Plugin Name: Digitas Widget Visit Our FAQ
  Description: Digitas Widget Visit Our FAQ
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-visit-our-faq-widget
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-visit-our-faq-widget', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-visit-our-faq-widget' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_VIS_VERSION', '1.0');
define('DIG_VIS_URL', plugins_url('', __FILE__));
define('DIG_VIS_DIR', dirname(__FILE__));

//Load class
require( DIG_VIS_DIR . '/inc/class-visit-our-faq-widget.php' );

//Init widget our apps
function dlbi_digitas_visit_our_faq_init() {
    register_widget('DigitasWidget_Visit_Our_Faq');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-visit-our-faq-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_digitas_visit_our_faq_init');
