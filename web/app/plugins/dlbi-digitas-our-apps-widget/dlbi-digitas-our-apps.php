<?php

/*
  Plugin Name: Digitas Widget Our Apps
  Description: Digitas Widget Our Apps
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-our-apps-widget
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-our-apps-widget', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-our-apps-widget' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_OUR_VERSION', '1.0');
define('DIG_OUR_URL', plugins_url('', __FILE__));
define('DIG_OUR_DIR', dirname(__FILE__));

//Load class
require( DIG_OUR_DIR . '/inc/class-our-apps-widget.php' );

//Init widget our apps
function dlbi_digitas_our_apps_init() {
    register_widget('DigitasWidget_Our_Apps');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-our-apps-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_digitas_our_apps_init');
