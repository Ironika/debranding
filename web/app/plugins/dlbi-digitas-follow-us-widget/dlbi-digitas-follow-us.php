<?php

/*
  Plugin Name: Digitas Widget Follow US
  Description: Digitas Widget Follow US
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-follow-us-widget
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-follow-us-widget', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-follow-us-widget' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_FOL_VERSION', '1.0');
define('DIG_FOL_URL', plugins_url('', __FILE__));
define('DIG_FOL_DIR', dirname(__FILE__));

//Load class
require( DIG_FOL_DIR . '/inc/class-follow-us-widget.php' );

//Init widget our apps
function dlbi_digitas_follow_us_init() {
    register_widget('DigitasWidget_Follow_Us');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-follow-us-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'dlbi_digitas_follow_us_init');
