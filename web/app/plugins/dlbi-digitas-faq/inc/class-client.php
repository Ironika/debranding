<?php

class DigitasFaq_Client {

    public function __construct() {
	add_action('init', array($this, 'digitas_faq_register_cpt'));
	add_action('init', array($this, 'digitas_faq_register_taxonomies'));
	add_action('pre_get_posts', array($this, 'digitas_alter_main_query'));
    }

    /**
     * Register the faq post type
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_faq_register_cpt() {

	$slug = get_field('url_faq', 'option') ? get_field('url_faq', 'option') : DIG_FAQ_PTYPE;
	register_post_type(DIG_FAQ_PTYPE, array(
	    'labels' => array(
		'name' => __('FAQ', 'dlbi-digitas-faq'),
		'singular_name' => __('FAQ', 'dlbi-digitas-faq'),
		'add_new' => __('Add new', 'dlbi-digitas-faq'),
		'add_new_item' => __('Add new FAQ', 'dlbi-digitas-faq'),
		'edit_item' => __('Edit FAQ', 'dlbi-digitas-faq'),
		'new_item' => __('New FAQ', 'dlbi-digitas-faq'),
		'view_item' => __('Show FAQ', 'dlbi-digitas-faq'),
		'search_items' => __('Search FAQ', 'dlbi-digitas-faq'),
		'not_found' => __('No FAQ found', 'dlbi-digitas-faq'),
		'not_found_in_trash' => __('No FAQ found in trash', 'dlbi-digitas-faq'),
		'parent_item_colon' => __('Parent FAQ', 'dlbi-digitas-faq'),
	    ),
	    'description' => '',
	    'publicly_queryable' => true,
	    'exclude_from_search' => false,
	    'map_meta_cap' => true,
	    'capability_type' => 'post',
	    'public' => true,
	    'hierarchical' => false,
	    'rewrite' => array(
		'slug' => $slug,
		'with_front' => true,
		'pages' => true,
		'feeds' => true,
	    ),
	    'has_archive' => $slug,
	    'query_var' => DIG_FAQ_PTYPE,
	    'supports' => array(
		0 => 'title',
		1 => 'editor',
		2 => 'thumbnail',
		3 => 'excerpt',
	    ),
	    'taxonomies' => array('category', 'post_tag'),
	    'show_ui' => true,
	    'menu_position' => 25,
	    'menu_icon' => 'dashicons-list-view',
	    'can_export' => true,
	    'show_in_nav_menus' => true,
	    'show_in_menu' => true,
	));

	return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @return boolean
     * @author Digitas LBI
     */
    public static function digitas_faq_register_taxonomies() {
	// create a new taxonomy
	register_taxonomy(
		__('profile'), DIG_FAQ_PTYPE, array(
	    'label' => __('Profile'),
	    'rewrite' => array('slug' => __('profile', 'dlbi-digitas-faq')),
	    'capabilities' => array(
		'assign_terms' => 'manage_categories',
		'edit_terms' => 'manage_categories'
	    )
		)
	);
	register_taxonomy(
		__('topic'), DIG_FAQ_PTYPE, array(
	    'label' => __('Topic'),
	    'rewrite' => array('slug' => __('topic', 'dlbi-digitas-faq')),
	    'capabilities' => array(
		'assign_terms' => 'manage_categories',
		'edit_terms' => 'manage_categories'
	    )
		)
	);
	register_taxonomy_for_object_type('profile', 'faq');
	register_taxonomy_for_object_type('topic', 'faq');

	return true;
    }

    /**
     * Register the faq taxonomies
     *
     * @param WP_Query $query
     * @return boolean
     * @author Digitas LBI
     */
    public function digitas_alter_main_query($query) {
	if ($query->is_post_type_archive(DIG_FAQ_PTYPE) && $query->is_main_query() && !is_admin()) {
	    $query->set('posts_per_page', 10);
	    $profile = get_query_var(__('profile'));
	    $topic = get_query_var(__('topic'));
	    $taxquery = array(
		array(
		    'taxonomy' => 'profile',
		    'field' => 'slug',
		    'terms' => array($profile),
		    'operator' => 'IN'
		),
		array(
		    'taxonomy' => 'topic',
		    'field' => 'slug',
		    'terms' => array($topic),
		    'operator' => 'IN'
		)
	    );

	    $query->set('tax_query', $taxquery);

	    return true;
	}
    }

}
