<?php

/*
  Plugin Name: Digitas FAQ
  Description: Plugin for manage FAQ custom post type
  Version: 0.1.1
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-faq
 */

require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-faq', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-faq' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_FAQ_VERSION', '1.0');
define('DIG_FAQ_URL', plugins_url('', __FILE__));
define('DIG_FAQ_DIR', dirname(__FILE__));
define('DIG_FAQ_PTYPE', 'faq');

//Load class
require( DIG_FAQ_DIR . '/inc/class-client.php' );

//Init
function dlbi_digitas_faq_init() {
    new DigitasFaq_Client();
    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-faq', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'dlbi_digitas_faq_init', 11);
