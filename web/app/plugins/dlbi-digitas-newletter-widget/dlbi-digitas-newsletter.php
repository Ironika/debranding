<?php

/*
  Plugin Name: Widget Customized For The Newsletter
  Description: Widget customized for the newsletter
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-newletter-widget
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-newletter-widget', //Metadata URL.
    __FILE__, //Full path to the main plugin file.
    'dlbi-digitas-newletter-widget' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_NEW_VERSION', '1.0');
define('DIG_NEW_URL', plugins_url('', __FILE__));
define('DIG_NEW_DIR', dirname(__FILE__));

//Load class
require( DIG_NEW_DIR . '/inc/class-newsletter-widget.php' );

//Init widget newletter
function digitas_widget_init() {
    register_widget('DigitasWidget_Newsletter');

    // Load plugin textdomain
    load_plugin_textdomain( 'dlbi-digitas-newletter-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('widgets_init', 'digitas_widget_init');
