<?php

class DigitasWebCallback_Client {

    public function __construct() {
	$show = get_field('web_callback_show', 'option');
	if ($show) {
	    add_action('wp_enqueue_scripts', array($this, 'digitas_webcallback_scripts'));
	    add_action('wp_footer', array($this, 'digitas_webcallback_render'));
	}
    }

    /**
     * Enqueue scripts and styles for the Web Callback
     */
    public static function digitas_webcallback_scripts() {
	wp_enqueue_script('linkeo-scripts', '//client.linkeo.com/chequecadeau.fr/prod/pack-wcb-digitas/js/LnkWcbPopinLoader.js', array(), '', true);
    }

    /**
     * Render the Web Callback
     */
    public static function digitas_webcallback_render() {
	$webcallback_id = get_field('web_callback_id', 'option');
	if ($webcallback_id) {
	    $html = '<div class="LnkWcbForm-trigger LnkWcbForm-trigger-position" title="Rappel immédiat et gratuit !"></div>';
	    $html .= '<input type="hidden" class="LnkWcbForm-canal" value="' . $webcallback_id. '" />';
	    
	    echo $html;
	}
    }

}
