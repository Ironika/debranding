(function () {
	var U = LnkWcb.util,
		params = U.urlDecode(window.location.search.substring(1)),
	/* ********** DEBUT CONFIG ********** */
	 INTE_CFG = {
			lang: 'fr',
			canal:  LnkWcb.jQuery('.LnkWcbForm-canal').val() || params.CODEBOUTON || 'LINKEO000F',
			// global selectors
			trigger: '.LnkWcbForm-trigger', // the trigger is the element that activates the container
			container: '.LnkWcbForm-container-pos', // the container that will be activated by the trigger
			containerHeight: 350,
			containerForm: '.LnkWcbForm-container', 
			backgroundImg: 'imgs/fond-wcb.png', 
			closer: '.LnkWcbForm-closer', // the container closer
			formId: 'LnkWcbForm',
			// relative selectors (relative to form root in 'formId')
			userStatus: '.user-status',
			errorStatus: '.user-errors',
			calleeInput: 'input[name="callee"]',
			dateInput: '.LnkWcbDateDiff',
			dateElem: '.lnk-wcb-date',
			timeElem: '.lnk-wcb-time',
			hoursSelectorClass: 'select.lnk-wcb-hours',
			minutesSelectorClass: 'select.lnk-wcb-minutes'
		},
	/* ********** FIN CONFIG ********** */
		$ = LnkWcb.jQuery;
		
/**
	 * This helper function does the setup of the trigger button (aka "Le Bouton").
	 */
	function setupWcbTrigger() {
		var params = U.urlDecode(window.location.search.substring(1));

		if (params.effect === 'fade') {
			$(INTE_CFG.trigger).parent().addClass('trigger-parent'); // stick the container to a fixed posision
		}

		$(INTE_CFG.trigger).click(function () {
			var elem = $(INTE_CFG.container),
				trigger = $(INTE_CFG.trigger),
				b = LnkWcb.inte1.montrerBouton;
			if (!b) {
				trigger.fadeOut();
			}
			else {
				// elem.slideDown().fadeIn(); // override the 'cacher' class
				trigger.slideUp().fadeOut();
				elem.slideDown("slow",function(){
   					
				 });
				
			}
		});
		
	}

	/**
	 * Setup the open/close animation for CNIL legal notice.
	 * This custom animation is required to allow this inline element to move accordingly.
	 * The jQuery UI/Effects/Slide would not suit this need.
	 * 
	 * @param F {jQuery} a jQuery search result holding the WCB form
	 */
	function setupCnilAnimation(F) {
		var opening = false, open = false, closing = false;
		F.find('.LnkWcbCnil').mouseenter(function (event) {
			var E = $(this).find('.depliant'),
				W = E.parent('.enveloppe'),
				distance;
			if (opening || open) {
				return;
			}
			opening = true;
			E.show();
			W.css('width', 'auto');
			distance = E.width();
			W.css('width', 0);
			E.css({ position: 'relative', left: -distance }).animate({ left: '+=' + distance }, {
				duration: 'slow', queue: false,
				step: function () {
					W.css('width', String(distance + parseInt(E.css('left'), 10))+'px')
				},
				complete: function () {
					E.css({ position: 'static', left: 'auto' }).show();
					W.css('width', 'auto');
					opening = false;
					open = true;
					E.dequeue();
				}
			});
		}).mouseleave(function (event) {
			var E = $(this).find('.depliant'),
				W = E.parent('.enveloppe'),
				distance;
			if (closing || !open) {
				return;
			}
			closing = true;
			E.show();
			W.css('width', 'auto');
			distance = E.width();
			E.css({ position: 'relative', left: 0 }).animate({ left: '-=' + distance }, {
				duration: 'slow', queue: false,
				step: function () {
					W.css('width', String(distance + parseInt(E.css('left'), 10))+'px')
				},
				complete: function () {
					E.css({ position: 'static', left: 'auto' }).hide();
					W.css('width', 'auto');
					closing = false;
					open = false;
					E.dequeue();
				}
			});
		});
	}

	/**
	 * This helper function creates one integration bouton.
	 * 
	 * @param I {Object} the integration namespace
	 * @param cfgBouton {Object} the bouton config
	 * @param cfgCalendar {Object} the date picker config
	 */
	function setupWcbForm(I, cfgBouton, cfgCalendar) {
		var F = $(cfgBouton.formSel),
			ackEnded;
		I.montrerBouton = false;
		/* ----- Bouton ----- */
		I.bouton = new LnkWcb.BoutonJquery(cfgBouton);
		I.bouton.useHttps(document.location.protocol === 'https:');
		I.bouton.showUserStatus = function (msg) { // permettre le HTML dans les messages des intermédiaires d'appel
			$(this.cfg.formSel).find(this.cfg.userStatusElemRelSel).html(msg);
		};
	
		/* On masque le bloc contact au debut du chargement - Visibility hidden pour corriger pb affichage IE sur la recuperation des infos */
		$(INTE_CFG.container).css('visibility','hidden');
		$(INTE_CFG.containerForm).css('visibility','hidden');
		window.onorientationchange = detectOrientation;
		var orientationScreen='portrait';
 		function detectOrientation (orientationScreen) {
			if ( orientation == 0 ) {
				orientationScreen='portrait';
			}
			else if ( orientation == 90 ) {
			 	orientationScreen='landscape';
			}
			else if ( orientation == -90 ) {
				 orientationScreen='landscape';
			}
			else if ( orientation == 180 ) {
				 orientationScreen='landscape';
			}
		 };
/* Redimensionnement */
	function redimensionnement(){ 
		/* CENTRE LES ELEMENTS POur ouverture en popin centre*/
		 /* Popin On centre le popup par rapport à la fenetre la fenetre*/
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		var containerWidth = $(INTE_CFG.container).width();
		var containerHeight = $(INTE_CFG.container).height();
		var alignWidth = (windowWidth/2)-(containerWidth/2);
		var alignHeight = windowHeight/2-INTE_CFG.containerHeight/2+$(document).scrollTop()+"px";
		// $(INTE_CFG.container).css("left",alignWidth);
		//  $(INTE_CFG.container).css("top",alignHeight);
		/* Detection taille de la fenetre */
		var body_width = $(window).width(); 
		var body_height = $(window).height();
		var container_height = $(INTE_CFG.container).height()+30; /* On ajoute le padding à la hauteur */
		// alert('Body Width : '+body_width+' - Body Height : '+body_height+' - Container: '+container_height);
		
		$(INTE_CFG.container).removeClass('lnkWcbForm-mobile').removeClass('lnkWcbForm-mobile-horiz').removeClass('lnkWcbForm-tablet');
		$(INTE_CFG.trigger).removeClass('lnkWcbBt-mobile').removeClass('lnkWcbBt-mobile-horiz').removeClass('lnkWcbBt-tablet');
			if ( body_height >= container_height && body_height !== container_height  && body_width <= 760) {
				$(INTE_CFG.container).addClass('lnkWcbForm-mobile');
				$(INTE_CFG.trigger).addClass('lnkWcbBt-mobile');
				$('#ui-datepicker-div').addClass('lnkWcbCal-mobile');
			  	
			} else if(body_height <= container_height && body_width <= 760 || windowWidth >= windowHeight && body_width <= 760) {
				$(INTE_CFG.container).addClass('lnkWcbForm-mobile-horiz');
				$(INTE_CFG.trigger).addClass('lnkWcbBt-mobile-horiz'); 
				// $('#ui-datepicker-div').addClass('lnkWcbCal-mobile');
			  	// zalert(containerHeight+'--'+windowHeight);
			} else if(body_height >= container_height && body_width <= 961) {
					$(INTE_CFG.container).addClass('lnkWcbForm-tablet');
					$(INTE_CFG.trigger).addClass('lnkWcbBt-tablet');
					// $('#ui-datepicker-div').removeClass('lnkWcbCal-mobile');
			} else {
				$(INTE_CFG.container).removeClass('lnkWcbForm-mobile').removeClass('lnkWcbForm-mobile-horiz').removeClass('lnkWcbForm-tablet');
				$(INTE_CFG.trigger).removeClass('lnkWcbBt-mobile').removeClass('lnkWcbBt-mobile-horiz').removeClass('lnkWcbBt-tablet');
				// $('#ui-datepicker-div').removeClass('lnkWcbCal-mobile');
			}   
		
		};
		/* Lancement de la fonction de redimensionnement */
		$(window).resize(function(){ redimensionnement(); }); 
		// window.onload = function () {  $(INTE_CFG.trigger).removeClass('lnk-loader'); $(INTE_CFG.trigger).unbind().bind('click', clickTrigger); redimensionnement(); }
		 /* GESTION OUVERTURE FERMETURE */
		var clickTrigger = function() {
				var elem = $(INTE_CFG.container),
				trigger = $(INTE_CFG.trigger),
				b = LnkWcb.inte1.montrerBouton;
			if (!b) {
				// trigger.slideDown( "slow", function() {});
				trigger.fadeIn();
			}
			else {
				// trigger.slideUp( 400, function() {});
				trigger.fadeOut( 400, function() {});
				elem.fadeIn().slideDown("slow",function(){elem.css('visibility','visible');$(INTE_CFG.containerForm).css('visibility','visible');});
				redimensionnement();
				
			}
			
		};
		$(INTE_CFG.closer).click(function () {
			var elem = $(INTE_CFG.container),
				trigger = $(INTE_CFG.trigger),
				b = LnkWcb.inte1.montrerBouton;
			
				if (b) {
					// trigger.slideDown( 400, function() {});
					trigger.fadeIn( 400, function() {});
				}
				elem.fadeOut().slideUp();
		});
		/* ANIMATIONS - UTILISATEUR */
		$('input[name="callee"]').focus(function () {  // permettre de supprimer le contenus du champs telephone onFocus
				$(this).val('');
		});
		/* VERIFICATION ETAT DU CANAL / INITIALISATION */
		I.bouton.onChannelState(function (etat, etatOuverture) { // Bouton : masquer en canal inactif ou hors-limites
			if (!etat.estActif || !etat.peutRecevoirAppel) {
				$(INTE_CFG.trigger).hide();
				I.montrerBouton = false;
			}
			else { // satué, fermé, férié, ou ouvert
				if ($(INTE_CFG.container).css('display') === 'block') { // hide the container if displayed (not to have both trigger AND container displayed)
					$(INTE_CFG.container).hide();
				}
				if(etatOuverture==="FERME" ||etatOuverture==="FERIE" ||etatOuverture==="SATURE") {
					$(INTE_CFG.trigger).find('.LnkWcbForm-trigger-txt').hide();
					$(INTE_CFG.trigger).find('.LnkWcbForm-trigger-txt_close').show();
				}
				$(INTE_CFG.trigger).show();
				I.montrerBouton = true;
			}
		});
		I.bouton.onChannelState(function (etat, etatOuverture) { // Formulaire : différé en état non ouvert, et désactivé en inactif ou hors-limites
				if (!etat.estActif || !etat.peutRecevoirAppel) {
					I.afficherDiffere(false);
					F.find('.champ-wcb').hide(); 
				}
				else if (etatOuverture ==="FERME" || etatOuverture ==="FERIE" || etatOuverture ==="SATURE") {
					I.afficherDiffere(true);
					F.find('.champ-wcb').show();
				}
				else {
					F.find('.champ-wcb').show();
					I.afficherDiffere(!etat.estOuvert); // refreshes the calendar
				}
				
			});
		
		oldShowUserErrors = I.bouton.showUserErrors;
		I.bouton.showUserErrors = function () {
			var that = this, ret;
			ret = oldShowUserErrors.apply(this, arguments);
			F.find(that.cfg.userErrorsElemRelSel).show();
			setTimeout(function () {
				F.find(that.cfg.userErrorsElemRelSel).has("ul").fadeOut();
			}, 5000);
			return ret;
		};
		I.bouton.onSendCall(function (args, attrs) {
			F.find(this.cfg.userStatusElemRelSel).addClass('busy');
			F.find('input').attr("disabled", true); // prevent duplicate submits
			F.find('.champ-wcb').hide();
		});
		ackEnded = function (status, params) {
			if(params.cause === "QUOTA_DEPASSE"){
				F.find(this.cfg.userStatusElemRelSel).html('Vous avez atteint le quota maximum d\'appel autoris&eacute;, veuillez nous contacter par un autre moyen.');
			}
			var resultStatus = F.find(this.cfg.userStatusElemRelSel).html();
			console.log(resultStatus+'---'+status);
			F.find(this.cfg.userStatusElemRelSel).removeClass('busy');
			F.find('input').attr("disabled", false); // re-enable form submits
			F.find('.champ-wcb').show();
			
		};
		I.bouton.onError(ackEnded);
		I.bouton.onEnded(ackEnded);
		I.bouton.onFallback(ackEnded);
		I.bouton.onFallback(function (status, params) { // activer automatiquement le mode différé
			if (status.debordementCause === 'CAUSE_BLOQUE' ) {
				I.afficherDiffere(false);
				F.find('input').attr("disabled", true);
				F.find('.champ-wcb').hide();
			}
			else if (status.debordementCause === 'CAUSE_FERME' || status.debordementCause === 'CAUSE_FERIE' ||status.debordementCause === 'CAUSE_SATURE' ||status.debordementCause === 'CAUSE_DEBORDE') {
				F.find('.champ-wcb').show();
				I.afficherDiffere(true);
			}
			else {
				F.find('.champ-wcb').show();
				I.afficherDiffere(true);
			}
		});
		/* ----- Calendrier ----- */
		I.calendrier = new LnkWcb.CalendarJquery(U.putAll({
			bouton: I.bouton
		}, cfgCalendar));
		I.calendrier.onShow(function () {
			$(I.calendrier.dateField).datepicker('option', 'maxDate', '+4d');	
				var txtHr = F.find(INTE_CFG.hoursSelectorClass+' option:selected').val();
				var txtMin = F.find(INTE_CFG.minutesSelectorClass+' option:selected').val();
				F.find('.lnkWcbForm-select-mask-hours').find('.lnkWcbForm-select-text').empty().append(txtHr+'h');
				F.find('.lnkWcbForm-select-mask-minutes').find('.lnkWcbForm-select-text').empty().append(txtMin+'m');
				setTimeout(function () {
				F.find(INTE_CFG.hoursSelectorClass).bind('change', function(){
					var current = $(this+'option:selected').val();
					F.find('.lnkWcbForm-select-mask-hours').find('.lnkWcbForm-select-text').empty().append($(this).val()+'h');
					console.log(current);
				});
				F.find(INTE_CFG.minutesSelectorClass).bind('change', function(){
					var current = $(this+'option:selected').val();
					F.find('.lnkWcbForm-select-mask-minutes').find('.lnkWcbForm-select-text').empty().append($(this).val()+'m');
					console.log(current);
				});
			}, 500);
		});
		
		/* ----- Intégration ----- */
		I.afficherDiffere = function (afficher) {
			F.find('.panneau-date-differe').toggleClass('cacher', !afficher);
			if (afficher) {
				I.calendrier.refresh(); // vérif des horaires d'ouverture + démarrage du calendrier (destroy+init+show)
			}
			else {
				I.calendrier.hide();
			}
			
		};
	
		/* ----- Intégration Evènements Formulaire ----- */
		$('a[rel="external"]').attr('target', '_blank'); // corriger les liens ayant l'attribut rel="external"
		F.find('a.cnil').attr('href',
			// L'URL email ci-dessous est volontairement découpée en petits morceaux
			// pour emêcher que les spammeurs ne l'obtiennent trop facilement.
			[ 'mail','to',':cnil@','linkeo.','com?','subject=',encodeURIComponent('Accès données personnelles pour ' + cfgBouton.canal) ].join(''));
		setupCnilAnimation(F);
		F.find('.codeCanal').text(cfgBouton.canal);
		F.find('input[name="callee"]').keypress(function (event) { // filtrer les caractères saisis
			var c = String.fromCharCode(event.charCode);
			if (event.charCode && !/^[0-9+]$/.test(c)) { // interdire la saisie des caractères autres que '0' à '9' et '+'
				return false;
			}
		}).keyup(function (event) { // filtrer le contenu du champ pendant la saisie
			var E = $(this), val = E.val(), pos;
			val = val.replace(/[^0-9+]/g, ''); // éliminer les caractères autres que '0' à '9' et '+'
			val = (val.indexOf('+') === 0 ? '+' : '') + val.replace(/\+/g, ''); // supprimer les '+' qui ne sont pas au début
			val = val.replace(/^\+0+/, '+'); // éliminer les '0' qui suivent le '+' initial, s'il y en a un
			val = val.replace(/^000+/, '00'); // interdire plus de deux '0' initiaux
			val = val.replace(/(\d{10})([0-9]){1,}/,'$1'); // Blocage 10 chiffre
			// val = val.replace(/(\d{2})/g,'$1 '); Formatage du numéro
			
			if (val !== E.val()) {
				if (!E.parent().hasClass('warning')) {
					//E.wrap('<span class="warning"/>'); // afficher un avertissement en cas de modification
				}
				E.val(val);
			}
		});
		F.submit(function () {
			var callee, date;
			callee = F.find(I.bouton.cfg.calleeInputRelSel).val();
			callee = callee.replace(/\s/g, ''); // On retire les espaces du formatage
			if (!F.find('.panneau-date-differe').hasClass('cacher')) {
				date = I.calendrier.getWcbDate(); // TODO: resolve the validation issue: input should be validated before it is converted to a JS Date object
				//LnkLog.log('date: [' + date + ']');
				//date = I.calendrier.getDateTime();
			}
			I.bouton.rappeler(callee, date);
			return false;
		});
		$(INTE_CFG.container).find(INTE_CFG.closer).click(function () {
			I.bouton.raccrocher();
		});
		/* ----- Lancement ----- */
		$(INTE_CFG.trigger).removeClass('lnk-loader'); $(INTE_CFG.trigger).unbind().bind('click', clickTrigger); redimensionnement(); 
		I.bouton.estOuvert(); // vérif de l'état du canal + refresh cal (à faire après le setup du bouton et du calendrier)
		
	};

	/**
	 * This helper function creates integration namespaces,
	 * and collects config options in order to create all integration boutons.
	 * <p>
	 * Customize here to add new boutons.
	 */
	function setupAllWcbForms() {
		var I, cfgBouton, cfgCalendar,
			params = U.urlDecode(window.location.search.substring(1));
		try {
			/* ----- Inits ----- */
			LnkWcb.intl.setLang(INTE_CFG.lang);
			// setupWcbTrigger();
			$(INTE_CFG.trigger).addClass('lnk-loader');
			$(INTE_CFG.trigger).append('<div class="LnkWcbForm-trigger-global LnkWcbForm-trigger-design"><div class="LnkWcbForm-trigger-content"><div class="LnkWcbForm-trigger-content-elem_1"><span class="LnkWcbForm-trigger-title">Besoin d\'aide ?</span><span class="LnkWcbForm-trigger-txt">Un conseiller vous rappelle immédiatement !</span><span class="LnkWcbForm-trigger-txt_close">Un conseiller vous rappelle !</span><a class="LnkWcbForm-trigger-bt">Cliquez ici</a></div><div class="LnkWcbForm-trigger-content-elem_2"><span class="LnkWcbForm-trigger-pic"></span></div></div>');
			$(document.body).append('<div class="LnkWcbForm-container-pos" xml:lang="fr"> </div>');
			/* ----- Lancement des enchainements d'initialisations ----- */
$(INTE_CFG.container).empty().html('<div class="LnkWcbForm-container"><div class="LnkWcbForm-closer">X</div><div class="LnkWcbForm-container-hidden"><div class="LnkWcbForm-header"><h1>Besoin d\'aide ?</h1></div><div class="LnkWcbForm-text"><fieldset><form id="LnkWcbForm" action="" method="post"><div class="status-bar"><div class="user-status"></div></div><div class="user-errors errors"></div><div><div class="panneau-date-differe cacher"><div class="LnkWcbDateDiff"><label for="lnk-wcb-date">Date et heure&nbsp;:</label><input type="text" class="lnk-wcb-date" readonly="readonly" /></div><div class="lnkWcbForm-select-btn"><span class="lnkWcbForm-select-mask-hours"><span class="lnkWcbForm-select-text">Heure</span></span><span class="lnkWcbForm-select-mask-minutes"><span class="lnkWcbForm-select-text">Minutes</span></span><span class="lnk-wcb-time"></span></div></div><div class="champ-wcb"><label for="callee">Saisissez votre n° de t&eacute;l&eacute;phone et validez</label><input type="tel" name="callee" value="Votre num&eacute;ro" class="LnkWcbForm-input-tel" /><input type="submit" name="call" value="Valider" class="LnkWcbForm-bt-valider" /></div></div> </div></form></fieldset></div></div>');
			I = LnkWcb.inte1 = LnkWcb.inte1 || {}; // création du namespace de l'intégration n°1
			cfgBouton = {
				canal: INTE_CFG.canal,
				grabbedFormId: INTE_CFG.formId,				// enable the 'grabForm' Trait
				formSel: '#'+INTE_CFG.formId,				// '#LnkWcbForm' by default
				userStatusElemRelSel: INTE_CFG.userStatus,	// '.user-status' by default
				userErrorsElemRelSel: INTE_CFG.errorStatus,	// '.user-errors' by default
				calleeInputRelSel: INTE_CFG.calleeInput,	// 'input[name="callee"]' by default
				dateInputRelSel: INTE_CFG.dateInput			// '.LnkWcbDateDiff' by default
			};
			cfgCalendar = {
				formSel: '#'+INTE_CFG.formId, // '#LnkWcbForm' by default
				dateContainerRelSel: INTE_CFG.dateElem, // '.lnk-wcb-date' by default
				timeContainerRelSel: INTE_CFG.timeElem // '.lnk-wcb-time' by default
				// , hoursSelectorClass: 'lnk-wcb-hours', // this is the default
				// minutesSelectorClass: 'lnk-wcb-minutes' // this is the default
				// , displayType: 'INLINE' // this is the default
			};
			setupWcbForm(I, cfgBouton, cfgCalendar);
		}
		catch (exc) {
			//LnkLog.log('LnkWcb.inte', exc);
		}
	};

	/**
	 * Create WCB integration when document is ready.
	 */
	try {
		$(setupAllWcbForms); // execute when the document is ready
	} catch (ignoredExc) {
		// LnkLog.log('integration', ignoredExc);
	}
})();
