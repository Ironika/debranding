var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']); 
/* jQuery Piwik */ 
LnkWcb.LnkWcbPiwik = LnkWcb.LnkWcbPiwik || {};
(function () {
	var u=(('https:' == document.location.protocol) ? 'https' : 'http') + '://piwik-gc.linkeo.com/piwik/';
	// var u=(('https:' == document.location.protocol) ? 'https' : 'http') + '://si.linkeo.it/piwik/';
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 142]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	/* ********** DEBUT CONFIG ********** */
	var  INTE_CFG = {
			// LinkBox
			idSite: 142,
			trigger: '.LnkWcbForm-trigger', // the trigger is the element that activates the container
			container: '.LnkWcbForm-container-pos' // the container that will be activated by the trigger
		},
	/* ********** FIN CONFIG ********** */
		$ = LnkWcb.jQuery;
		LnkWcb.LnkWcbPiwik = function () {
			try {
		/* Show the trigger */
		var showTrigger = function() {
				if ($(INTE_CFG.container).css('display') === 'block') { $(INTE_CFG.container).hide();}
				$(INTE_CFG.trigger).show();
		};
		var initOperator = function (clientValue, parametersValue, parametersOp) {		 
		  var statutResultScript = false;	
		  console.log(clientValue+ ' - '+parametersValue + parametersOp); 
		  switch (parametersOp) {
				case 'est égal à':
					if(clientValue.indexOf(parametersValue)!= -1  ) { 
						console.log('Current page '+clientValue +' - '+parametersValue+' - op_num : '+parametersOp);
						statutResultScript= true;
					} 
				break;
				case 'n\'est pas égal à':
					if(clientValue.indexOf(parametersValue)=== -1  ) { 
							statutResultScript= true;
					}
				break;
				case 'ne contient pas':
					if(clientValue.indexOf(parametersValue)=== -1  ) { 
							statutResultScript= true;
					}
				break;
				case 'contient':
					if(clientValue.indexOf(parametersValue)!=-1  ) {
							statutResultScript= true;
					}
				break;
				case 'commence par':
					if(clientValue.indexOf(parametersValue)!=-1 && clientValue.indexOf(parametersValue)<=2 ) {
							statutResultScript= true;
					}
				break;
				case 'se termine par':
					if(clientValue.lastIndexOf(parametersValue)!=-1  ) {
							statutResultScript= true;
					}
				break;
			} 
			return statutResultScript;
		};
		
		// Evaluate rule for the canal
   var evaluateCanalRule = function (visitorId) {
		var urlButtonRules ='//piwik-gc.linkeo.com/buttonrules/service.do';
	/* Function crossdomain */
	function crossDomainAjax (url, successCallback) {
		// IE8 & 9 only Cross domain JSON GET request
				if ('XDomainRequest' in window && window.XDomainRequest !== null) {
					var xdr = new XDomainRequest(); // Use Microsoft XDR
					xdr.open('get', url+'?action=evaluate&codeCanal=' + LnkWcb.inte1.bouton.cfg.canal + '&idVisitor=' + visitorId + '&idSite=' + INTE_CFG.idSite);
					xdr.onload = function () {
						var dom  = new ActiveXObject('Microsoft.XMLDOM'),
							JSON = $.parseJSON(xdr.responseText);
						dom.async = false;
						if (JSON == null || typeof (JSON) == 'undefined') {
							JSON = $.parseJSON(data.firstChild.textContent);
						}
			
						successCallback(JSON); // internal function
					};
			
					xdr.onerror = function() {
						_result = false;  
					};
			
					xdr.send();
				} 
			
				// IE7 and lower can't do cross domain
				else if (navigator.userAgent.indexOf('MSIE') != -1 &&
						 parseInt(navigator.userAgent.match(/MSIE ([\d.]+)/)[1], 10) < 8) {
				   return false;
				}    
				// Do normal jQuery AJAX for everything else          
				else {
				 LnkWcb.jQuery.ajax({
					url: urlButtonRules+'?action=evaluate&codeCanal=' + LnkWcb.inte1.bouton.cfg.canal + '&idVisitor=' + visitorId + '&idSite=' + INTE_CFG.idSite,
					dataType: 'json', 
				//   url: 'http://si.linkeo.it/buttonrules/service.do?action=evaluate&codeCanal=' + LnkWcb.inte1.bouton.cfg.canal + '&idVisitor=' + visitorId + '&idSite=' + INTE_CFG.idSite, 
				  success : function(data) {
					  
						successCallback(data);
				  },
				  error: function () {
						   console.log('Erreur  ');
						   return false;
					}
				});
				}
			}
			/*End Function Crossdomain */
		/* Crossdomain Launcher */
		crossDomainAjax(urlButtonRules, function (data) {
			console.log(data);
				I = LnkWcb.inte1 = LnkWcb.inte1 || {};
				if ( data.evaluationResult===false && LnkWcb.inte1.montrerBouton === false && !data.errorCode || LnkWcb.inte1.montrerBouton === false) {
					$(INTE_CFG.trigger).hide(); 
				}
				else {  
					if ( data.evaluationResult === true && LnkWcb.inte1.montrerBouton === true && data.delegateToScript.length === 0 || data.errorCode ===100 ) { 
						showTrigger();
					} else if(data.evaluationResult === false ) {
						return false;
					} else {
						var timerParametersValue = 0;
						var arrDelegateToScript = [];
						for(i=0 ; i<data.delegateToScript.length ; i++) {
							  var dataDelegateToScript = data.delegateToScript[i]; 
							switch (dataDelegateToScript) { 
									case 'currentPage.js':
										var cp_parametersOp = data.delegateToScriptParameters[i].op;
										var cp_parametersValue = data.delegateToScriptParameters[i].value;
										var cp_current_win = window.location.href;
										arrDelegateToScript.push(initOperator(cp_current_win, cp_parametersValue, cp_parametersOp));   
									break;
									case 'previousPage.js':
										var pp_parametersOp = data.delegateToScriptParameters[i].op;
										var pp_parametersValue = data.delegateToScriptParameters[i].value;
										var pp_referrer_win = document.referrer;
										// console.log('referrer url : '+pp_referrer_win +' - Regle parametree'+pp_parametersValue);
										arrDelegateToScript.push(initOperator(pp_referrer_win, pp_parametersValue, pp_parametersOp)); 
									break;
									case 'timer.js':
										var parametersOp = data.delegateToScriptParameters[i].op;
										var parametersValue = data.delegateToScriptParameters[i].value;
										// console.log(parametersOp+' - '+parametersValue);
										parametersValue = parseInt(parametersValue);
										timerParametersValue = parseInt(parametersValue);
										arrDelegateToScript.push('timer.js');
									break;
								} 
						}
						
						function launchTimer(parametersValue) {  
							 var start = new Date, timer;
										var LnkWcbTimer = setInterval(function() {
											var timer = parseInt((new Date - start) / 1000); 
											console.log('Timer OFF : ' + timer +' - parmetersValue'+ parametersValue);
											if(timer >= parametersValue  ) { 
												showTrigger();
												clearInterval(LnkWcbTimer);
											}
										}, 1000);
						};
						/*	function allAreTrue(tab) {  
							var allAreTrueResult = true;
							for(var i = 0; i < tab.length; i++) {
								console.log(tab[i]);
								if(tab[i] === false) {
									allAreTrueResult = false;
								}  
							}
							return allAreTrueResult;  
						};
						var resultAllAreTrue = allAreTrue(arrDelegateToScript);
							console.log(  resultAllAreTrue);
						if(resultAllAreTrue === true || resultAllAreTrue !='') { 
							if(timerParametersValue != 0) { launchTimer(timerParametersValue) } else {   showTrigger(); }
						}else {
							$(INTE_CFG.trigger).hide();
						}
						
						*/
						for(var i = 0; i < arrDelegateToScript.length; i++) { 
								if(arrDelegateToScript[i] === false) {
									$(INTE_CFG.trigger).hide();
								} else {
									if(timerParametersValue != 0) { launchTimer(timerParametersValue) } else {   showTrigger(); return false; }
								}
							}
					}
				}
			});
		/* End Crossdomain Launcher */
		};
			// _paq.push(['setSiteId', INTE_CFG.idSite]);
			var visitor_id;
				_paq.push([ function() { visitor_id = this.getVisitorId();  evaluateCanalRule(visitor_id);}]);
				$(window).on('hashchange', function(e){_paq.push([ function() { visitor_id = this.getVisitorId();  evaluateCanalRule(visitor_id);}]);});
			
		} catch (exc) {			
			console.log('Erreur Piwik');
		}
	}
	try {
		 LnkWcb.LnkWcbPiwik();
	} catch (ignoredExc) {
	}
})();