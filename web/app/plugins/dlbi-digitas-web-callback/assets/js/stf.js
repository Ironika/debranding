(function () {
	var $ = LnkWcb.jQuery,
		U = LnkWcb.util,
		DOC = document,
		testMode = false;
	$(function () {
			var NS = {
					'LnkWcbForm': 'LnkWcb.inte1.bouton'
				},
				styleSheet, cssRules, n;
			if (testMode) {
				return;
			}
			styleSheet = DOC.styleSheets[DOC.styleSheets.length - 1];
			if (DOC.implementation.hasFeature("CSS", "2.0")) {
				styleSheet.insertRule(".STf-trigger { float: right; }", styleSheet.cssRules.length);
			}
			else if (styleSheet.addRule && styleSheet.rules) { // IE kludge: non standard DOM implementation
				styleSheet.addRule(".STf-trigger", "float: right;", styleSheet.rules.length);
			}
			n = U.createMarkupNode({ div: [ { 'class': 'STf-trigger' }, { a: [ { href: 'javascript:void(0);' }, 'Activer le mode test' ] } ] }, DOC);
			$('.menu-bar', DOC).before(n);
			
			$('.STf-trigger', DOC).click(function () {
				var E = $(this, DOC),
					src = 'stf.html', n;
				src += '?btn=' + NS[E.parent().find('form').attr('id')] /* + '.bouton' */;
				// src += '\u0026cal=' + NS[E.parent().find('form').attr('id')].replace('.bouton', '.calendrier');
				n = U.createMarkupNode({
					iframe: [ {
						id:'menu-bar-frame',
						src: src,
						style: 'position: absolute; right: 0px; z-index:1; width: 100%; height: 15em; background-color: white;' // IE kludge: use 460px instead of 66ex wrongly computed as 1ex=8px
					} ]
				}, DOC);
				d = U.createMarkupNode({ div: [ { 'class': 'menu-bar-close' }, { a: [ { href: 'javascript:window.location.reload();' }, 'Désactiver le mode test' ] } ] }, DOC);
				E.after(n);
				E.after(d);
				E.hide();
			});
			$('.menu-bar-close', DOC).click(function () {
				$('#menu-bar-frame').hide();
			});
	$('.menu-bar-open', DOC).click(function () {
				$('#menu-bar-frame').fadeIn();
	});
			testMode = true;
			
	});
	
	
/*	

$('input').each(function(i){
if($(this).is('[type=checkbox],[type=radio]')){
var input = $(this);
// get the associated label using the input's id
var label = $('label[for='+input.attr('id')+']');
//get type, for classname suffix
var inputType = (input.is('[type=checkbox]')) ? 'checkbox' : 'radio';
// wrap the input + label in a div
$('<div class="custom-'+ inputType +'"></div>').insertBefore(input).append(input, label);
// find all inputs in this set using the shared name attribute
var allInputs = $('input[name='+input.attr('name')+']');
// necessary for browsers that don't support the :hover pseudo class on labels
label.hover(
function(){
$(this).addClass('hover');
if(inputType == 'checkbox' && input.is(':checked')){
$(this).addClass('checkedHover');
}
},
function(){ $(this).removeClass('hover checkedHover'); }
);
//bind custom event, trigger it, bind click,focus,blur events
input.bind('updateState', function(){
if (input.is(':checked')) {
if (input.is(':radio')) {
allInputs.each(function(){
$('label[for='+$(this).attr('id')+']').removeClass('checked');
});
};
label.addClass('checked');
}
else { label.removeClass('checked checkedHover checkedFocus'); }
})
.trigger('updateState')
.click(function(){
$(this).trigger('updateState');
})
.focus(function(){
label.addClass('focus');
if(inputType == 'checkbox' && input.is(':checked')){
$(this).addClass('checkedFocus');
}
})
.blur(function(){ label.removeClass('focus checkedFocus'); });
}
}); */
})();