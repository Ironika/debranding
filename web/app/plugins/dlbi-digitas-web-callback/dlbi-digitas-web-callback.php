<?php

/*
  Plugin Name:DLBI Digitas web callback
  Description: Plugin for display and manage web callback
  Version: 1.0
  Author: Digitas LBI
  Author URI: https://www.digitaslbi.com/
  License: GPLv2 or later
  Text Domain: dlbi-digitas-web-callback
 */


require 'plugin-update-checker/plugin-update-checker.php';
$MyUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		'https://wp-update.dev.soxodlbi.xyz/?action=get_metadata&slug=dlbi-digitas-visit-our-faq-widget', //Metadata URL.
		__FILE__, //Full path to the main plugin file.
		'dlbi-digitas-web-callback' //Plugin slug. Usually it's the same as the name of the directory.
);

// Plugin Consts
define('DIG_WEBC_VERSION', '1.0');
define('DIG_WEBC_URL', plugins_url('', __FILE__));
define('DIG_WEBC_DIR', dirname(__FILE__));

//Load class
require( DIG_WEBC_DIR . '/inc/class-client.php' );
require( DIG_WEBC_DIR . '/inc/class-admin.php' );

//Init widget our apps
function dlbi_digitas_web_callback_init() {

    new DigitasWebCallback_Client();
    
    if(is_admin()){
	new DigitasWebCallback_Admin;
    }

    // Load plugin textdomain
    load_plugin_textdomain('dlbi-digitas-visit-our-faq-widget', false, dirname(plugin_basename(__FILE__)) . '/languages/');
}

add_action('plugins_loaded', 'dlbi_digitas_web_callback_init');
