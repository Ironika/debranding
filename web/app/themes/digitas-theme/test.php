<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
get_header();

$need = get_post(get_the_ID());
?>
<h1>NEED</h1>
<div class="col-md-12">
    <div class="container">
        <div class="row">
            <h4>Header</h3>
                <div class="col-sm-12">
                    <b>title</b> : <blockquote><?php echo get_post_meta(get_the_ID(), 'need_header_title', true); ?></blockquote>
                    <b>description</b> : <blockquote><?php echo get_post_meta(get_the_ID(), 'need_header_description', true); ?></blockquote>

                    <?php // Get image
                    $image = get_field('need_header_image', get_the_ID());

                    if (!empty($image)) :
                        ?>
                        <div class="col-lg-5 col-md-6 offset-lg-1 align-self-center">
                            <div class="advantage-img">
                                <img class="img-fluid" src="<?php echo $image['sizes']['large'] ?>" />
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php // check if the repeater field has rows of data
                    if( have_rows('needs_header_pictos') ):
                        foreach(get_field('needs_header_pictos') as $row): ?>

                            <b>needs_header_picto</b> :
                            <?php
                            if (isset($row['needs_header_picto'])) {
                                echo '<img src="'.$row['needs_header_picto']['url'].'" alt="'.$row['needs_header_picto']['alt'].'" height="'.$row['needs_header_picto']['height'].'" width="'.$row['needs_header_picto']['width'].'" />';
                            }
                            ?>

                            <b>needs_header_value : valeur avec %</b> :
                            <?php
                            if (isset($row['needs_header_value'])) {
                                echo $row['needs_header_value'];
                            }
                            ?>
                            <b>needs_header_description</b> :
                            <?php
                            if (isset($row['needs_header_description'])) {
                                echo $row['needs_header_description'];
                            }
                            ?>

                        <?php
                        endforeach;
                    endif;
                    ?>

                </div>
        </div>
        <div class="row">
            <h4><?php  echo __('Products highlight', 'lbi-digitas-theme');?></h3>
                <?php
                // check if the repeater field has rows of data
                if( have_rows('need_products_highlight') ):
                    foreach(get_field('need_products_highlight') as $row): ?>
                        <div class="col-sm-12">

                            <?php// print_r($row['need_product_color_text']);?>

                            <b>product_highlight_title</b> : <blockquote><?= $row['need_product_highlight_title']; ?></blockquote>
                            <b>product_highlight_description</b> : <blockquote><?= $row['need_product_highlight_description']; ?></blockquote>

                            <b>product_highlight_link</b> :<?php
                            $linked_post_id = $row['need_product_highlight_link']->ID;
                            ?>
                            <a href="<?php the_permalink($linked_post_id); ?>" class="block-posts-grid_content--item">discover more</a>

                            <b>product_highlight_background_image</b> :
                            <?php
                            if (isset($row['need_product_highlight_background_image'])) {
                                echo '<img src="'.$row['need_product_highlight_background_image']['url'].'" alt="'.$row['need_product_highlight_background_image']['alt'].'" height="'.$row['need_product_highlight_background_image']['height'].'" width="'.$row['need_product_highlight_background_image']['width'].'" />';
                            }
                            ?>
                            <b>product_highlight_background_color</b> : <?= isset($row['need_product_highlight_background_color']) ? '<blockquote style="background-color:'.$row['need_product_highlight_background_color'].'; color: white; text-align: center; width: 70px;">'.$row['need_product_highlight_background_color'].'</blockquote>' : ''; ?>
                            <b>product_highlight_product_image</b> :
                            <?php
                            if (isset($row['need_product_highlight_product_image'])) {
                                echo '<img src="'.$row['need_product_highlight_product_image']['url'].'" alt="'.$row['need_product_highlight_product_image']['alt'].'" height="'.$row['need_product_highlight_product_image']['height'].'" width="'.$row['need_product_highlight_product_image']['width'].'" />';
                            }
                            ?>
                            <hr/>
                        </div>
                    <?php
                    endforeach;
                endif;
                ?>
        </div>
        <div class="row">
            <h4><?php  echo __('Content', 'lbi-digitas-theme');?></h3>
                <div class="col-sm-12">
                    <?php
                    echo do_shortcode($need->post_content);
                    ?>
                </div>
        </div>
        <div class="row">
            <h4><?php  echo __('Others needs', 'lbi-digitas-theme');?></h3>
                <div class="col-sm-12">
                    <b>title</b> : <?php echo get_post_meta(get_the_ID(), 'need_others_title', true); ?>
                    <!-- Others needs -->
                    <?php
                    $relatedNeeds = get_posts(
                        array(
                            'post_type' => 'need',
                            'numberposts' => 5,
                            'post__not_in' => array(get_the_ID())
                        )
                    );
                    foreach ($relatedNeeds as $relatedNeed) : ?>
                        <div class="article">
                            <?php
                            // Get Image
                            if (has_post_thumbnail($relatedNeed)) :
                                echo get_the_post_thumbnail($relatedNeed->ID, 'large', array('class' => 'blog-component--container_image'));
                            endif;
                            ?>

                            <?php echo get_the_date('d.m.Y'); ?>
                            <div class="relatedcontent">
                                <h3>
                                    <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>">
                                        <?php the_title(); ?>
                                    </a>
                                </h3>
                                <?php the_content(); ?>
                            </div>
                        </div>
                    <?php endforeach;
                    wp_reset_postdata();
                    ?>
                </div>
        </div>
    </div>
</div>
<div class="breadcrumb">
    <?php
    if(function_exists('bcn_display')):

        bcn_display();
    endif; ?>
</div>

<?php get_footer(); ?>
