<?php
/**
 * The template for displaying all single posts.
 *
 * @package understrap
 */
get_header();
?>
<div class="hero">
    <?php
    if (has_post_thumbnail(get_the_ID())) : ?>
	   <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'soxo-hero-header'); ?>" alt=""/>
    <?php endif; ?>
</div>
<div class="col-md-12 blog-article">
    <div class="container">
	<div class="row">
	    <div class="col-sm-12 col-lg-8">
		<!-- display content -->
		<?php while (have_posts()) : the_post(); ?>
    		<!-- Get content -->
		    <?php get_template_part('loop-templates/content-single'); ?>

    		<!-- Pagination -->
    		<div class="row pager">
    		    <div class="col-sm-6">
			    <?php if (get_next_post_link()): ?>
				<p class="blog-component--container_link prev">
				    <i class="fa fa-arrow-left fa-lg" aria-hidden="true"></i><span><?php next_post_link('%link', __('Previous article', 'lbi-digitas-theme')) ?></span>
				<?php endif; ?>
    			</p>
    		    </div>
    		    <div class="col-sm-6">
			    <?php if (get_previous_post_link()): ?>
				<p class="blog-component--container_link next">
				    <span><?php previous_post_link('%link', __('Next article', 'lbi-digitas-theme')) ?></span><i class="fa fa-arrow-right fa-lg" aria-hidden="true"></i></p>
			    <?php endif; ?>
    		    </div>
    		</div>
		<?php endwhile; // end of the loop.  ?>

	    </div>
	    <!-- Do the right SIDEBAR -->
	    <?php get_sidebar('right-single'); ?>
	</div>
    </div>
</div>


<!-- Article from the same category -->
<?php
$related = get_posts(
	array('category__in' => wp_get_post_categories(get_the_ID()),
	    'numberposts' => 1,
	    'post__not_in' => array(get_the_ID()))
);
if ($related) :
    foreach ($related as $post) :
	?>
	<div class="related-article">
	    <div class="container">
		<div class="row">
		    <div class="col-12">
			<div class="blog-item">
			    <?php
			    // Get Image
			    if (has_post_thumbnail($post->ID)) : ?>
				      <div class="media">
                <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'soxo-blog-medium'); ?>" alt="<?php echo $post->post_title; ?>" class="blog-component--container_image"/>
              </div>
			    <?php endif; ?>
			    <div class="blog-component--container">
				<?php
				// Get category article
				$categories = get_the_category();
				if (!empty($categories)) :
				    ?>
	    			<p class="blog-component--container_cat"><?php echo esc_html($categories[0]->name); ?></p>
				<?php endif; ?>
				<p class="blog-component--container_date"><?php echo get_the_date('d.m.Y'); ?></p>

				<p class="blog-component--container_title"><a href="<?php echo get_permalink($post->ID); ?>" rel="bookmark" title="<?php echo $post->post_title; ?>">
					<?php echo $post->post_title; ?>
				    </a></p>
				<div class="blog-component--container_content">
				    <?php echo wp_trim_words(strip_shortcodes($post->post_content), 70); ?>
				</div>
			    </div>
			</div>
		    </div>
		</div>
	    </div>
	</div>
    <?php
    endforeach;
    wp_reset_postdata();
    ?>
<?php endif ?>


<!-- PANEL PRODUCTS: retrieve product sheets set in posts -->
<?php
$orig_post = $post;

$postslist = get_field('featured_products');

if ($postslist) :
    foreach ($postslist as $post) :
	// feature image
	if (has_post_thumbnail($post->ID)) :
	    echo get_the_post_thumbnail($post->ID, 'large', array('class' => 'blog-component--container_image'));
	endif;
	$data = get_post($post->ID);
	echo apply_filters('the_content', $data->post_content);
	?>
	<a class="btn-digitas btn-digitas-white" href="<?php echo get_permalink($post->ID) ?>"><?php echo get_the_title($post->ID); ?></a>
    <?php endforeach; ?>
    <ul class="nav nav-pills">
        <li role="presentation"><a href="<?php echo get_post_type_archive_link(SOD_PRO_PTYPE); ?>"><?php echo __('ALL PRODUCTS', 'lbi-digitas-theme'); ?></a></li>
    </u>
<?php endif; ?>

<div class="breadcrumb">
    <?php
    if (function_exists('bcn_display')):
	bcn_display();
    endif;
    ?>
</div>
<?php get_footer(); ?>
