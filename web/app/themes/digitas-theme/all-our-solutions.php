<?php
/*
  Template Name: All our solutions
 */
get_header();

//$all_our_solutions_block_title = get_field('all_our_solutions_block_title');
//$all_our_solutions_block_show = get_field('all_our_solutions_block_show');
//$all_our_solutions_block_list = get_field('all_our_solutions_block_list');
$all_our_solutions_image = get_field('all_our_solutions_background');
?>

<?php if ($all_our_solutions_image) : ?>
    <div class="page-cover">
	    <div class="page-cover_container">
        <?php echo wp_get_attachment_image( $all_our_solutions_image, 'soxo-hero-header' ); ?>
	    </div>
    </div>
<?php endif; ?>

<?php get_template_part( 'layouts-acf/block_posts-grid' ); // Load post grid layout ?>

<?php the_content() ?>

<div class="breadcrumb">
  <?php
   if(function_exists('bcn_display')):
            bcn_display();
   endif; ?>
</div>
<?php
get_footer();
