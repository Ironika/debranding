<?php
/*
  Template Name: Accessibility
 */
get_header();
?>

<?php while (have_posts()) : the_post(); ?>
    <?php if (has_post_thumbnail()) : ?>
	<div class="page-cover">
	    <div class="page-cover_container">
		<?php the_post_thumbnail('soxo-hero-header'); ?>
	    </div>
	</div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
    	<div class="col-md-12">
    	    <h1 class="digitas-title"><?php the_title(); ?></h1>
    	</div>
        </div>
        <div class="row">
    	<div class="col-md-12">
    	    <div class="edito-content static-content">
		    <?php the_content(); ?>
    	    </div>
    	</div>
        </div>
    </div>
<?php endwhile; ?>

<?php get_footer(); ?>
