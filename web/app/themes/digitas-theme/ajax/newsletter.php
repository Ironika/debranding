<?php

// Load WordPress
$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp/wp-load.php';
//$wp_load = $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php';
require_once( $wp_load );


if ((isset($_POST['userEmail']) && !empty($_POST['userEmail']))) {

    $recipients = wp_list_pluck(get_field('newsletter_mails', 'option'), 'newsletter_mail');

    if ($recipients) {
	$clients = '';
	foreach ($recipients as $recipient) {
	    $clients .= $recipient . ',';
	}
	$confirmation = get_field('newsletter_confirmation_text', 'option');
	$subject = get_field('newsletter_subject', 'option');
	$message = get_field('newsletter_email_body', 'option') . ' : ' . $_POST['userEmail'];
	wp_mail($clients, $subject, $message);

	echo get_field('newsletter_confirmation_text', 'option');
    }
}









