<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
get_header();

$post_id = get_the_ID();
$simplelink = 'Simple link';
$typeform = 'Typeform';
$youtube = 'Youtube';
?>

<section class="prod-head">

    <!-- Product head block -->
    <?php $prodheadimgbg = get_field('product_header_image_background'); ?>
    <div class="prod-head--background" style="background-image: url('<?php echo $prodheadimgbg['url']; ?>')">
	<div class="prod-head--content">
	    <div class="container">
		<div class="row">
		    <div class="col-12 col-md-6">
			<div class="prod-head--content_textblock hidden-sm-down">
			    <h1 class="digitas-page-head-title"><?php echo get_field('product_header_title') ?></h1>
			    <div class="digitas-page-head-description"><?php echo get_field('product_header_description') ?></div>
			    <?php
			    if (is_array(get_field('product_header_link_simulate'))):
				$link = get_field('product_header_link_simulate');
				if ($link["url"]):
				    ?>
				    <a onclick="dataLayer.push({'event': 'click-cta', 'location': 'header', 'name': '<?php echo $link["title"]; ?>'});"
				    <?php if (get_field('product_header_select_a_link_type') == $simplelink) : ?>
	    			       class="btn-digitas btn-digitas-red" href="<?php echo $link["url"]; ?>" <?php if ($link["target"]): ?> target="_blank" <?php endif ?>
				       <?php elseif (get_field('product_header_select_a_link_type') == $typeform) : ?>
	    			       class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $link["url"]; ?>"
				       <?php elseif (get_field('product_header_select_a_link_type') == $youtube) : ?>
	    			       class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $link["url"]; ?>"
				       <?php endif; ?>><?php echo $link["title"]; ?></a>
				   <?php endif; ?>
			       <?php endif; ?>

			    <?php
			    if (is_array(get_field('product_header_cta_2'))):
				$link2 = get_field('product_header_cta_2');
				if ($link2["url"]) :
				    ?>
				    <a onclick="dataLayer.push({'event': 'click-cta', 'location': 'header', 'name': '<?php echo $link2["title"]; ?>'});"
				    <?php if (get_field('product_header_select_a_link_type2') == $simplelink): ?>
	    			       class="btn-digitas btn-digitas-red" href="<?php echo $link2["url"]; ?>" <?php if ($link2["target"]): ?> target="_blank" <?php endif ?>
				       <?php elseif (get_field('product_header_select_a_link_type2') == $typeform) : ?>
	    			       class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $link2["url"]; ?>"
				       <?php elseif (get_field('product_header_select_a_link_type2') == $youtube) : ?>
	    			       class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $link2["url"]; ?>"
				       <?php endif; ?>>
					   <?php echo $link2["title"]; ?>
				    </a>
				<?php endif; ?>
			    <?php endif; ?>
			</div>
		    </div><!--.product-head-block-->
		    <div class="col-12 col-md-6 align-self-end text-center">
			<?php
			$prodheadimg = get_field('product_header_image');
			if (!empty($prodheadimg)) :
			    ?>
    			<img class="prod-head--content_image" src="<?php echo $prodheadimg['url']; ?>" alt="<?php echo $prodheadimg['alt']; ?>" />
			<?php endif; ?>
		    </div>
		</div>
	    </div>
	</div>
    </div>
    <div class="prod-head--content_mobile hidden-md-up">
	<div class="container-fluid">
	    <div class="row">
		<div class="col-md-12">
		    <div class="prod-head--content_textblock">
			<h1 class="digitas-page-head-title_mobile"><?php echo get_field('product_header_title') ?></h1>
			<div class="digitas-page-head-description"><?php echo get_field('product_header_description') ?></div>
			<?php
			if (is_array(get_field('product_header_link_simulate'))):
			    $link = get_field('product_header_link_simulate');
			    if ($link["url"]):
				?>
				<a onclick="dataLayer.push({'event': 'click-cta', 'location': 'header', 'name': '<?php echo $link["title"]; ?>'});"
				<?php if (get_field('product_header_select_a_link_type') == $simplelink): ?>
	    			   class="btn-digitas btn-digitas-red" href="<?php echo $link["url"]; ?>" <?php if ($link["target"]): ?> target="_blank" <?php endif ?>
				   <?php elseif (get_field('product_header_select_a_link_type') == $typeform) : ?>
	    			   class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $link["url"]; ?>"
				   <?php elseif (get_field('product_header_select_a_link_type') == $youtube) : ?>
	    			   class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $link["url"]; ?>"
				   <?php endif; ?>><?php echo $link["title"]; ?></a>
			       <?php endif; ?>
			   <?php endif; ?>
			   <?php
			   if (is_array(get_field('product_header_cta_2'))):
			       $link2 = get_field('product_header_cta_2');
			       if ($link2["url"]):
				   ?>
				<a onclick="dataLayer.push({'event': 'click-cta', 'location': 'header', 'name': '<?php echo $link2["title"]; ?>'});"
				<?php if (get_field('product_header_select_a_link_type2') == $simplelink): ?>
	    			   class="btn-digitas btn-digitas-red" href="<?php echo $link2["url"]; ?>" <?php if ($link2["target"]): ?> target="_blank" <?php endif ?>
				   <?php elseif (get_field('product_header_select_a_link_type2') == $typeform) : ?>
	    			   class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $link2["url"]; ?>"
				   <?php elseif (get_field('product_header_select_a_link_type2') == $youtube) : ?>
	    			   class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $link2["url"]; ?>"
				   <?php endif; ?>
				   >
				       <?php echo $link2["title"]; ?>
				</a>
			    <?php endif; ?>
			<?php endif; ?>
		    </div>
		</div>
	    </div>
	</div>
    </div>
    <!--.product-head-block-->

</section>
<?php
$back_link = get_field('back_to_the_merchant_page', 'option');
$back_link_category = get_field('back_to_the_merchant_page_category', 'option');
$is_cat = wp_list_pluck(get_the_terms(get_the_ID(), 'category-product'), 'term_id');

if ($back_link && in_array($back_link_category[0], $is_cat)):
    ?>
    <section class="back-link">
        <div class="container">
    	<div class="row">
    	    <div class="col-xs-12 col-lg-8">
    		<a href="<?php echo $back_link['url'] ?>" class="back-link-page">
    		    <i class="icon-arrow-left" aria-hidden="true"></i>
    		    <span><?php echo $back_link['title']; ?></span>
    		</a>
    	    </div>
    	</div>
        </div>
    </section>
<?php endif ?>
<!-- Product page cta block -->
<?php if (get_field('product_page_cta_block_show')): ?>
    <section class="prod-ctas">

        <div class="container">



	    <?php if (get_field('product_page_cta_block_title')): ?>
		<div class="row">
		    <div class="col-md-12">
			<h2 class="digitas-title"><?php echo get_field('product_page_cta_block_title') ?></h2>
		    </div>
		</div>
	    <?php endif ?>
	    <?php if (get_field('product_page_cta_block_description')): ?>
		<div class="prod-ctas--text row no-gutters justify-content-center">
		    <div class="col-md col-lg-9">
			<?php echo get_field('product_page_cta_block_description') ?></h2>
		    </div>
		</div>
	    <?php endif ?>
	    <?php $steps = get_field('product_page_cta_block_list'); ?>
	    <?php if ($steps) : ?>


		<div class="prod-ctas--block">
		    <div class="row no-gutters justify-content-center">
			<?php foreach ($steps as $step): ?>
	    		<div class="col-12 col-sm">
	    		    <div class="prod-ctas--container">
				    <?php if ($step['product_page_cta_block_icon']): ?>
					<div class="prod-ctas--container_image" style="background-image: url('<?php echo $step['product_page_cta_block_icon']['sizes']['large'] ?>')"></div>
				    <?php endif ?>
				    <?php if ($step['product_page_cta_block_value']): ?>
					<p class="prod-ctas--container_number"><?php echo $step['product_page_cta_block_value'] ?></p>
				    <?php endif ?>
				    <?php if ($step['product_page_cta_block_text']): ?>
					<p class="prod-ctas--container_title"><?php echo $step['product_page_cta_block_text'] ?></p>
				    <?php endif ?>
				    <?php if ($step['product_page_cta_block_read__more']): ?>
					<a href="<?php echo get_permalink($step['product_page_cta_block_read__more']); ?>" class="prod-ctas--container_read-more">
					    <i class="fa fa-arrow-right fa-lg"></i><span><?php echo _e('Read more') ?></span>
					</a>
				    <?php endif ?>
				    <?php
				    if (is_array($step['product_page_cta_block_btn'])):
					$cta_block_btn = $step['product_page_cta_block_btn'];
					if ($cta_block_btn["url"]) :
					    ?>
		    			<a
					    <?php if ($step['product_page_cta_block_select_a_link_type'] == $simplelink) : ?>
						    class="btn-digitas btn-digitas-white" href="<?php echo $cta_block_btn["url"]; ?>" <?php if ($cta_block_btn["url"]): ?> target="_blank" <?php endif ?>
						<?php elseif ($step['product_page_cta_block_select_a_link_type'] == $typeform) : ?>
						    class="btn-digitas btn-digitas-white" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $cta_block_btn["url"]; ?>"
						<?php elseif ($step['product_page_cta_block_select_a_link_type'] == $youtube) : ?>
						    class="btn-digitas btn-digitas-white" data-fancybox href="<?php echo $cta_block_btn["url"]; ?>"
						<?php endif ?>><?php echo $cta_block_btn["title"] ?></a>
					    <?php endif; ?>
					<?php endif; ?>
	    		    </div>
	    		</div>
			<?php endforeach; ?>
		    </div>
		</div>
	    <?php endif; ?>

        </div>

    </section>
<?php endif ?>

<?php if (get_field('simulator_show_block')): ?>
    <section class="simulator-wrapper" id="simulator">
        <div class="container">
	    <?php if (get_field('simulator_title')): ?>
		<div class="row">
		    <div class="col-md-12">
			<h2 class="digitas-title"><?php echo get_field('simulator_title') ?></h2>
		    </div>
		</div>
	    <?php endif ?>
    	<div id="root" class="">
    	</div>
        </div>
    </section>

    <!-- Modal -->
    <div class="modal fade"  id="sendmailmodal" tabindex="-1" role="dialog" aria-labelledby="sendmailmodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
    	<div class="modal-content">
    	    <div class="modal-header">
    		<h5 class="modal-title"><?php if (get_field('pdf_modal_title', 'option')) echo get_field('pdf_modal_title', 'option'); ?></h5>
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
    	    </div>
    	    <div class="modal-body">

    		<form id="mail-pdf-form" action="<?php echo site_url(); ?><?php if (get_field('pdf_action_url_adress', 'option')) echo get_field('pdf_action_url_adress', 'option'); ?>?<?php if (get_field('select_a_simulator')) echo "simulator=" . get_field('select_a_simulator') . "&"; ?>socialSecurityTaxes=1&totalFacialBenefits=2&totalAnnualTaxWithoutSalarySacrifice=134" method="post">
    		    <div class="form-group">
    			<div class="input-group">

    			    <input type="email" name="send_pdf_mail" class="form-control" id="send_pdf" aria-describedby="sendPdfByMail" placeholder="<?php if (get_field('modal_text_content', 'option')) echo strip_tags(get_field('modal_text_content', 'option')); ?>" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
    			    <span class="input-group-btn">
    				<button class="btn btn-secondary" type="submit">OK</button>
    			    </span>
    			</div>
    			<small id="sendPdfByMail" class="form-text sr-only"><?php if (get_field('pdf_email_field_error_message', 'option')) echo get_field('pdf_email_field_error_message', 'option'); ?></small>
    			<small id="pdf_modal_error" class="form-text hidden-xs-up"><?php if (get_field('pdf_email_error', 'option')) echo get_field('pdf_email_error', 'option'); ?></small>
    			<small id="pdf_modal_success" class="form-text hidden-xs-up"><?php if (get_field('pdf_email_success', 'option')) echo get_field('pdf_email_success', 'option'); ?></small>
    		    </div>

    		</form>
    	    </div>
    	    <div class="modal-footer">
    		<button type="button" class="btn btn-default" data-dismiss="modal" id="close-modal-pdf"><?php if (get_field('pdf_modal_close_text', 'option')) echo get_field('pdf_modal_close_text', 'option'); ?></button>
    	    </div>
    	</div>
        </div>
    </div>
<?php endif ?>

<div id="wrapper-product-megatab">

    <section class="prod-tabs">

	<?php
	$tabs = get_field('product_tabs');
	if ($tabs):
	    ?>


    	<div class="prod-tabs--content">

    	    <div class="container">

    		<div class="row">
    		    <div class="col-md-12">

    			<!-- Nav tabs -->
    			<ul class="nav nav-tabs nav-justified hidden-sm-down" id="product-tabs" role="tablist">
				<?php
				$g = 0;
				foreach ($tabs as $tab):
				    if ($g === 0) {
					$firstItem = $tab['product_tab_name'];
				    }
				    ?>
				    <li class="nav-item" role="presentation">
					<a class="tab-nav-link nav-link <?php echo $g == 0 ? 'active' : ''; ?>" data-toggle="tab" href="#tab<?php echo sanitize_title($tab['product_tab_name']) ?>" aria-controls="<?php echo sanitize_title($tab['product_tab_name']) ?>" role="tab">
					    <?php echo $tab['product_tab_name'] ?>
					</a>
				    </li>
				    <?php $g++; ?>
				<?php endforeach; ?>
    			</ul>

    			<div class="fake-select hidden-md-up">
    			    <div class="fake-select_content">
    				<a class="fake-select-link" href="#" data-toggle="modal" data-target="#MegaTabMenuModal">
    				    <span><?php echo $firstItem ?></span><i class="icon icon-arrow-down"></i>
    				</a>
    			    </div>
    			</div>

    			<div class="menu-modal modal" id="MegaTabMenuModal" tabindex="-1" role="dialog" aria-labelledby="MegaTabMenuModal" aria-hidden="true">
    			    <div class="modal-dialog" role="document">
    				<div class="modal-content">
    				    <div class="modal-header">
    					<h5 class="modal-title"><?php echo __('Select an advantages', 'lbi-digitas-theme'); ?></h5>
    					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    					    <span aria-hidden="true"><i class="icon icon-arrow-down"></i></span>
    					</button>
    				    </div>
    				    <div class="modal-body">
    					<ul>
						<?php $g = 0; ?>
						<?php foreach ($tabs as $tab): ?>
						    <li class="nav-item" role="presentation">
							<a class="modal-nav-link nav-link <?php echo $g == 0 ? 'active' : ''; ?>" data-toggle="tab" href="#tab<?php echo sanitize_title($tab['product_tab_name']) ?>" aria-controls="<?php echo sanitize_title($tab['product_tab_name']) ?>" role="tab">
							    <?php echo $tab['product_tab_name'] ?>
							</a>
						    </li>
						    <?php $g++; ?>
						<?php endforeach; ?>
    					</ul>
    				    </div>
    				</div>
    			    </div>
    			</div>

    			<!-- Tab panes -->
    			<div class="tab-content">
				<?php
				$i = 0;
				foreach ($tabs as $tab):
				    ?>
				    <div role="tabpanel" class="tab-pane fade <?php echo $i == 0 ? 'show active' : ''; ?>" id="tab<?php echo sanitize_title($tab['product_tab_name']) ?>" role="tabpanel">

					<div class="first-block">
					    <div class="row">
						<div class="col-md-12">
						    <?php if ($tab['product_tab_title']): ?>
	    					    <h2 class="digitas-title digitas-title--white"><?php echo $tab['product_tab_title'] ?></h2>
						    <?php endif ?>
						</div>
					    </div>
					    <div class="row">
						<?php if ($tab['product_tab_image'] && array_key_exists('sizes', $tab['product_tab_image'])): ?>
	    					<div class="col-lg-5 col-md-6 offset-lg-1 align-self-center">
	    					    <div class="advantage-img">
	    						<img class="img-fluid" src="<?php echo $tab['product_tab_image']['sizes']['large'] ?>" />
	    					    </div>
	    					</div>
						<?php endif ?>
						<?php
						$list_tab = $tab['product_tab_list'];

						if ($list_tab):
						    $h = 1;
						    ?>
	    					<div class="col-lg-5 col-md-6">
	    					    <ul class="advantage-list">
							    <?php
							    foreach ($list_tab as $item):
								$hide_class = $h > 4 ? 'style="display:none;"' : '';
								?>
								<li <?php echo $hide_class; ?>>
								    <?php if ($item['product_tab_list_title']): ?>
		    						    <h3 class="soxo-check-icon"><?php echo $item['product_tab_list_title'] ?></h3>
								    <?php endif; ?>
								    <?php echo $item['product_tab_list_text'] ?>
								</li>
								<?php
								$h++;
							    endforeach;
							    ?>
	    					    </ul>
							<?php if (count($list_tab) > 4): ?>
							    <div class="see-more-advantages">
								<i class="fa fa-plus" aria-hidden="true"></i>
								<a class="btn-show-more-advantages" href="#">
								    <?php echo __('See more advantages', 'lbi-digitas-theme'); ?>
								</a>
							    </div>
							<?php endif ?>
	    					</div>
						<?php endif ?>
					    </div>

					    <div class="row tab-content-footer product-tab-brochure-link">

						<div class="col-lg-5 col-md-6 offset-lg-1">
						    <?php if ($tab['product_tab_brochure_link']): ?>
	    					    <div class="download-brochure">
	    						<a class="soxo-download-icon" href="<?php echo $tab['product_tab_brochure_link']['url'] ?>" target="_blank"><?php echo $tab['product_tab_brochure_link']['title'] ?></a>
	    					    </div>
						    <?php endif ?>
						</div>

						<div class="col-lg-5 col-md-6">
						    <div class="tab-content-footer-more ask-for-a-quote-link">
							<?php
							if (is_array($tab['product_tab_ask_for_a_quote_link'])):
							    $quote_link = $tab['product_tab_ask_for_a_quote_link'];
							    if ($quote_link['url']):
								?>
								<a onclick="dataLayer.push({'event': 'click-get-quote'});"
								<?php if ($tab['product_tab_ask_for_a_quote_select_a_link_type'] == $simplelink): ?>
		    						   class="btn-digitas btn-digitas-red" href="<?php echo $quote_link['url'] ?>" <?php if ($quote_link["target"]): ?> target="_blank" <?php endif ?>
								   <?php elseif ($tab['product_tab_ask_for_a_quote_select_a_link_type'] == $typeform) : ?>
		    						   class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $quote_link['url'] ?>"
								   <?php elseif ($tab['product_tab_ask_for_a_quote_select_a_link_type'] == $youtube): ?>
		    						   class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $quote_link['url'] ?>"
								   <?php endif; ?>
								   ><?php echo $quote_link['title'] ?></a>
							       <?php endif; ?>
							   <?php endif; ?>

							<?php
							if (is_array($tab['product_page_place_an_order_link'])):
							    $order_link = $tab['product_page_place_an_order_link'];
							    if ($order_link['url']):
								?>
								<a onclick="dataLayer.push({'event': 'click-order-online'});"
								<?php if ($tab['product_page_place_an_order_select_a_link_type'] == $simplelink): ?>
		    						   class="btn-digitas btn-digitas-red" href="<?php echo $order_link['url']; ?>" <?php if ($order_link["target"]): ?> target="_blank" <?php endif ?>
								   <?php elseif ($tab['product_page_place_an_order_select_a_link_type'] == $typeform): ?>
		    						   class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $order_link['url'] ?>"
								   <?php elseif ($tab['product_page_place_an_order_select_a_link_type'] == $youtube): ?>
		    						   class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $order_link['url']; ?>"
								   <?php endif ?>
								   ><?php echo $order_link['title']; ?></a>
							       <?php endif ?>
							   <?php endif ?>

						    </div>
						</div>

					    </div><!-- END row tab-content-footer -->
					</div><!--.first-block-->

					<?php if ($tab['product_show_getting_pass_block']): ?>
	    				<div class="getting-pass-block">
						<?php if ($tab['product_getting_pass_title']): ?>
						    <div class="row">
							<div class="col-lg-10 col-md-12 offset-lg-1">
							    <h2 class="digitas-title"><?php echo $tab['product_getting_pass_title'] ?></h2>
							</div>
						    </div>
						<?php endif ?>


						<?php if ($tab['product_getting_pass_steps']) : ?>
						    <div class="getting-pass-steps  getting-pass-steps--desktop">
							<div class="row">
							    <?php
							    $j = 1;
							    foreach ($tab['product_getting_pass_steps'] as $step):
								?>

		    					    <div class="col-12 col-sm">
								    <?php if ($step['product_getting_pass_step_icon'] && array_key_exists('sizes', $step['product_getting_pass_step_icon'])): ?>

									<div class="step-img-container">
									    <div class="step-img-container2">
										<img class="img-fluid" src="<?php echo $step['product_getting_pass_step_icon']['sizes']['large'] ?>" />
									    </div>
									    <?php
									    //if ($step != end($tab['product_getting_pass_steps'])) {
									    if ($step === reset($tab['product_getting_pass_steps'])) {
										?>
			    						    <div class="trait-container">
			    							<!--<div class="trait-inter"></div>-->
			    						    </div>
										<?php
									    }
									    ?>
									</div>
								    <?php endif ?>
                    
								    <?php if ($step['product_getting_pass_step_title']) : ?>
									    <h3><?php echo $step['product_getting_pass_step_title']; ?></h3>
								    <?php else: ?>
									    <h3><?php echo __('Step', 'lbi-digitas-theme') . ' ' . $j; ?></h3>
								    <?php endif; ?>		
                    	
								    <?php if ($step['product_getting_pass_step_text']): ?>
									    <?php echo $step['product_getting_pass_step_text'] ?>
								    <?php endif ?>
		    					    </div>

								<?php
								$j++;
							    endforeach
							    ?>
							</div>
						    </div>
						<?php endif ?>

						<?php if ($tab['product_getting_pass_steps']) : ?>
						    <div class="getting-pass-steps getting-pass-steps--mobile">

							<div class="getting-pass-steps-slick">
							    <?php
							    $j = 1;
							    foreach ($tab['product_getting_pass_steps'] as $step):
								?>

		    					    <div>
								    <?php if ($step['product_getting_pass_step_icon'] && array_key_exists('sizes', $step['product_getting_pass_step_icon'])): ?>
									<div class="step-img-container">
									    <img class="img-fluid" src="<?php echo $step['product_getting_pass_step_icon']['sizes']['large'] ?>" />

									    <?php
									    if ($step != end($tab['product_getting_pass_steps'])) {
										?>
			    						    <div class="trait-container">
			    							<div class="trait-inter"></div>
			    						    </div>
										<?php
									    }
									    ?>

									</div>
                <?php endif; ?>
                      
                    <?php if ($step['product_getting_pass_step_title']): ?>
                      <h3><?php echo $step['product_getting_pass_step_title']; ?></h3>
                    <?php else: ?>
		    						  <h3><?php echo __('Step', 'lbi-digitas-theme') . ' ' . $j; ?></h3>
                    <?php endif; ?>	
                    
  								  <?php if ($step['product_getting_pass_step_text']): ?>
  									  <?php echo $step['product_getting_pass_step_text']; ?>
                    <?php endif; ?>

		    					    </div>
								<?php
								$j++;
							    endforeach
							    ?>

							</div>
						    </div>
						<?php endif ?>
						<?php if ($tab['know_more_select_a_link_type']): ?>
						    <div class="row getting-pass-knowmore">
							<div class="col-md-12 text-center">
							    <?php
							    if (is_array($tab['know_more_link'])):
								$more_link = $tab['know_more_link'];
								if ($more_link['url']):
								    ?>
								    <a onclick="dataLayer.push({'event': 'click-cta','location': 'steps','name': '<?php if ($more_link["title"]) : echo $more_link["title"]; else : echo __('Know more', 'lbi-digitas-theme'); endif; ?>'});" 
								    <?php if ($tab['know_more_select_a_link_type'] == $simplelink): ?>
			    					       class="btn-digitas btn-digitas-white" href="<?php echo $more_link['url']; ?>" <?php if ($more_link["target"]): ?> target="_blank" <?php endif ?>
								       <?php elseif ($tab['know_more_select_a_link_type'] == $typeform): ?>
			    					       class="btn-digitas btn-digitas-white" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $more_link['url']; ?>"
								       <?php elseif ($tab['know_more_select_a_link_type'] == $youtube): ?>
			    					       class="btn-digitas btn-digitas-white" data-fancybox href="<?php echo $more_link['url']; ?>"
								       <?php endif ?>
								       >
									   <?php if ($more_link["title"]) : echo $more_link["title"]; else : echo __('Know more', 'lbi-digitas-theme'); endif; ?>
								    </a>
								<?php endif ?>
							    <?php endif ?>
							</div>
						    </div>
						<?php endif; ?>
	    				</div><!--.getting-pass-block-->
					<?php endif ?>
					<?php if ($tab['product_show_regulation_pass_block']): ?>
	    				<div class="show-regulation-pass-block">
						<?php if ($tab['product_regulation_pass_title']): ?>
						    <div class="row">
							<div class="col-md-12">
							    <h2 class="digitas-title"><?php echo $tab['product_regulation_pass_title'] ?></h2>
							</div>
						    </div>
						<?php endif ?>
						<?php if ($tab['product_regulation_pass_list']): ?>
						    <div class="row">
							<?php foreach ($tab['product_regulation_pass_list'] as $item): ?>
							    <?php if ($item['product_regulation_pass_list_text']): ?>
								<div class="col-sm-6">
								    <div class="regulation-pass-list-item">
									<i class="icon-check" aria-hidden="true"></i>
									<?php echo $item['product_regulation_pass_list_text'] ?>
								    </div>
								</div>
							    <?php endif ?>
							<?php endforeach; ?>
						    </div>
						<?php endif ?>

						<?php if ($tab['product_show_download_brochure_block']): ?>

						    <div class="row tab-content-footer show-download-brochure-block">

							<div class="col-sm-6">
							    <?php if ($tab['product_download_brochure_link']): ?>
		    					    <div class="download-brochure">
		    						<a target="_blank" class="soxo-download-icon" href="<?php echo $tab['product_download_brochure_link']['url'] ?>"><?php echo $tab['product_download_brochure_link']['title'] ?></a>
		    					    </div>
							    <?php endif ?>
							</div>

							<div class="col-sm-6">
							    <div class="tab-content-footer-more">
								<?php
								if (is_array($tab['product_dowload_brochure_ask_for_a_quote_link'])):
								    $aquote_link = $tab['product_dowload_brochure_ask_for_a_quote_link'];
								    if ($aquote_link['url']):
									?>
									<a onclick="dataLayer.push({'event': 'click-cta', 'location': 'regulation', 'name': '<?php echo $aquote_link['title'] ?>'});"
									<?php if ($tab['product_dowload_brochure_ask_for_a_quote_select_a_link_type'] == $simplelink): ?>
			    						   class="btn-digitas btn-digitas-red" href="<?php echo $aquote_link['url']; ?>" <?php if ($aquote_link["target"]): ?> target="_blank" <?php endif ?>
									   <?php elseif ($tab['product_dowload_brochure_ask_for_a_quote_select_a_link_type'] == $typeform): ?>
			    						   class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $aquote_link['url']; ?>"
									   <?php elseif ($tab['product_dowload_brochure_ask_for_a_quote_select_a_link_type'] == $youtube) : ?>
			    						   class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $aquote_link['url']; ?>"
									   <?php endif ?>
									   ><?php echo $aquote_link['title'] ?></a>
								       <?php endif ?>
								   <?php endif ?>
								   <?php
								   if (is_array($tab['product_download_brochure_place_an_order_link'])):
								       $an_order_link = $tab['product_download_brochure_place_an_order_link'];
								       if ($an_order_link['url']):
									   ?>
									<a onclick="dataLayer.push({'event': 'click-cta', 'location': 'regulation', 'name': '<?php echo $an_order_link['title'] ?>'});"
									<?php if ($tab['product_download_brochure_place_an_order_select_a_link_type'] == $simplelink): ?>
			    						   class="btn-digitas btn-digitas-red" href="<?php echo $an_order_link['url']; ?>" <?php if ($an_order_link["target"]): ?> target="_blank" <?php endif ?>
									   <?php elseif ($tab['product_download_brochure_place_an_order_select_a_link_type'] == $typeform): ?>
			    						   class="btn-digitas btn-digitas-red" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $an_order_link['url']; ?>"
									   <?php elseif ($tab['product_download_brochure_place_an_order_select_a_link_type'] == $youtube): ?>
			    						   class="btn-digitas btn-digitas-red" data-fancybox href="<?php echo $an_order_link['url']; ?>"
									   <?php endif ?>
									   ><?php echo $an_order_link['title'] ?></a>
								       <?php endif ?>
								   <?php endif ?>
							    </div>
							</div>

						    </div>
						<?php endif ?>

	    				</div><!--.show-regulation-pass-block-->
					<?php endif ?>

					<?php if ($tab['product_show_apps_block']): ?>
	    				<div class="download-apps-block">
	    				    <div class="row">
						    <?php if ($tab['product_show_apps_image'] && array_key_exists('sizes', $tab['product_show_apps_image'])): ?>
							<div class="col-12 col-md-6">
							    <div class="download-apps-block-image" style="background-image:url('<?php echo $tab['product_show_apps_image']['sizes']['large'] ?>')"></div>
							</div>
						    <?php endif ?>
	    					<div class="col-12 col-md-6">
	    					    <div class="download-apps-block-text">
							    <?php if ($tab['product_show_apps_title']): ?>
								<h3><?php echo $tab['product_show_apps_title'] ?></h3>
							    <?php endif ?>
							    <?php if ($tab['product_show_apps_text']): ?>
								<?php echo $tab['product_show_apps_text'] ?>
							    <?php endif ?>
							    <?php if ($tab['product_show_apps_list']): ?>
								<ul class="product-show-apps-list">
								    <?php foreach ($tab['product_show_apps_list'] as $app): ?>
									<?php if ($app['product_show_apps_select_a_link_type']) : ?>
									    <li>
										<?php
										if (is_array($app['product_show_apps_link'])) :
										    $apps_link = $app['product_show_apps_link'];
										    if ($apps_link['url']) :
											?>
											<a onclick="dataLayer.push({'event': 'click-cta', 'location': 'app-block'});"
											<?php if ($app['product_show_apps_select_a_link_type'] == $simplelink): ?>
				    							   href="<?php echo $apps_link['url']; ?>" <?php if ($apps_link["target"]): ?> target="_blank" <?php endif ?>
											   <?php elseif ($app['product_show_apps_select_a_link_type'] == $typeform) : ?>
				    							   href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $apps_link['url']; ?>"
											   <?php elseif ($app['product_show_apps_select_a_link_type'] == $youtube) : ?>
				    							   data-fancybox href="<?php echo $apps_link['url']; ?>"
											   <?php endif; ?>
											   ><img src="<?php echo $app['product_show_apps_image']['sizes']['large'] ?>" /></a>
										       <?php endif; ?>
										   <?php endif; ?>
									    </li>
									<?php endif; ?>
								    <?php endforeach; ?>
								</ul>
							    <?php endif; ?>
	    					    </div>
	    					</div>
	    				    </div>
	    				</div><!--.download-apps-block-->
					<?php endif ?>


					<?php if ($tab['product_show_faq'] && $tab['product_faq']): ?>
	    				<div class="faq-block">
	    				    <div class="row">
	    					<div class="col-md-12">
	    					    <h2 class="digitas-title"><?php echo $tab['product_faq_title'] ?></h2>
	    					</div>
	    				    </div>
	    				    <div class="row">
	    					<div class="col-lg-10 col-md-12 offset-lg-1">

	    					    <!-- ACCORDION -->
	    					    <div class="panel-group" id="accordion-faq<?php echo $i; ?>" role="tablist" aria-multiselectable="true">

							    <?php
							    $z = 0;
							    foreach ($tab['product_faq'] as $faq_list):

								$faq_id = $faq_list->ID;
								$faq = get_field('faq_questions', $faq_id);
								?>

								<?php
								if ($faq):
								    foreach ($faq as $question):
									?>
									<div class="panel panel-default">
									    <div class="panel-heading" role="tab" id="heading<?php echo $i . $z; ?>">
										<h4 class="panel-title">
										    <a class="faq-title collapsed" role="button" data-toggle="collapse" data-parent="#accordion-faq<?php echo $i; ?>" href="#collapse<?php echo $i . $z; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i . $z; ?>">
											<?php echo $question['faq_question'] ?>
										    </a>
										</h4>
									    </div>
									    <div id="collapse<?php echo $i . $z; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $i . $z; ?>">
										<div class="panel-body">
										    <?php echo $question['faq_answer'] ?>
										</div>
									    </div>
									</div>
									<?php
								    endforeach;

								endif
								?>

								<?php
								$z++;

							    endforeach;
							    ?>

	    					    </div>
	    					    <!-- END ACCORDION -->
	    					</div>
	    				    </div>
						<?php if ($tab['product_faq_select_a_link_type']): ?>
						    <div class="row">
							<div class="col-md-12">
							    <div class="faq-block--footer-btn">
								<?php
								if (is_array($tab['product_all_faq_links'])):
								    $all_faq_links = $tab['product_all_faq_links'];
								    if ($all_faq_links["url"]):
									?>
									<a <?php if ($tab['product_faq_select_a_link_type'] == $simplelink): ?>
			    						    class="btn-digitas btn-digitas-white" href="<?php echo $all_faq_links["url"]; ?>" <?php if ($all_faq_links["target"]): ?> target="_blank" <?php endif ?>
									    <?php elseif ($tab['product_faq_select_a_link_type'] == $typeform) : ?>
			    						    class="btn-digitas btn-digitas-white" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $all_faq_links["url"]; ?>"
									    <?php elseif ($tab['product_faq_select_a_link_type'] == $youtube) : ?>
			    						    class="btn-digitas btn-digitas-white" data-fancybox href="<?php echo $all_faq_links["url"]; ?>"
									    <?php endif ?>
									    >
										<?php if ($all_faq_links["title"]): ?>
										    <?php echo $all_faq_links["title"]; ?>
										<?php else : ?>
										    <?php echo __('See all faq', 'lbi-digitas-theme'); ?>
										<?php endif ?>
									</a>
								    <?php endif ?>
								<?php endif ?>
							    </div>
							</div>
						    </div>
						<?php endif; ?>
	    				</div><!--.faq-block-->
					<?php endif ?>

				    </div><!--.tab-pane-->

				    <?php
				    $i++;
				endforeach;
				?>
    			</div><!--.tabs-content-->

    		    </div><!--.col-md-12-->
    		</div><!--.row-->

    	    </div><!--.container-->

    	</div><!--.products-tabs-content-->

	<?php endif ?>

    </section><!--.prod-tabs-->

</div><!--#wrapper-product-megatab-->

<div id="wrapper-product-content">

    <?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>

	    <?php /*
	      <div class="row">
	      <div class="col-md-12">
	      <div class="container-fluid">
	     */ ?>

	    <?php the_content(); ?>

	    <?php /*
	      </div>
	      </div>
	      </div>
	     */ ?>

	<?php endwhile; ?>
    <?php endif; ?>

</div>

<div class="breadcrumb">
    <?php
    if (function_exists('bcn_display')):
	bcn_display();
    endif;
    ?>
</div>

<?php get_footer(); ?>
