<?php
// SEARCH
if (get_field('search_show_block', 'option')): ?>
    <li>
        <button type="button" class="search icon-search" aria-controls="lmt-search" aria-expanded="false">
            <span>Search</span>
        </button>
        <div id="lmt-search" class="lmt-layer-search" aria-hidden="true">
            <button type="button" class="lmt-close">
                <span>Close overlay</span>
            </button>
            <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="GET">
                <div class="header">
                    <label for="s" class="tt"><?php esc_attr_e( 'Search', 'lbi-digitas-theme' ); ?></label>
                </div>
                <p>
                    <input type="search" name="s" id="s" value="<?php echo get_search_query() ?>" placeholder="<?php echo get_field('search_title', 'option'); ?>">
                    <button type="submit" class="submit-search icon-search">
                        <span><?php esc_attr_e( 'Search', 'lbi-digitas-theme' ); ?></span>
                    </button>
                </p>
            </form>
        </div>
    </li>
<?php endif ?>