<?php
$default = get_field('lead_toolbar_custom_blocks', 'option');
$lmt_layout = get_field('lead_managment_toolbar', get_the_ID());
$show_wcb = get_field('web_callback_show', 'option');
$wcb_id = get_field('web_callback_id', 'option');
if ($lmt_layout) {
    $lmt_layout = $lmt_layout[0];
}
$custom = get_field('lead_toolbar_custom_blocks', $lmt_layout);
$blocks = $custom ? $custom : $default;
// template to display background image if any uploaded in BO
$sBackgroundTemplate = 'style="background-image:url(\'%s\');background-size:contain;background-repeat:no-repeat;background-position:left"';

if ($blocks) :
    foreach ($blocks as $block) :
	$show_block = $block['show_block'];
	$block_type = $block['type_of_block'][0]['acf_fc_layout'];
	if ($show_block && $block_type == 'search') :
	    $title = $block['type_of_block'][0]['search_title'];
	    $customPicto = isset($block['type_of_block'][0]['search_picto']) ? $block['type_of_block'][0]['search_picto'] : '';
	    $sStyleBackground = $sClassIcon = '';
	    if ($customPicto) :
		$sStyleBackground = sprintf($sBackgroundTemplate
			, $customPicto['sizes']['medium']
		);
	    else :
		$sClassIcon = 'icon-search';
	    endif;
	    ?>

	    <div class="lmt-panel">
	        <button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'search'});" type="button" class="lmt-btn search <?php echo $sClassIcon ?>" data-toggle="collapse" data-target="#collapseSearch" aria-controls="collapseSearch" aria-expanded="false" <?php echo $sStyleBackground ?>>
	    	<span class="lmt-button-title"><?php echo __('Search', 'lbi-digitas-theme'); ?></span>
	        </button>
	        <div class="collapse" id="collapseSearch">
	    	<div class="lmt-layer lmt-layer-search">
	    	    <button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseSearch" aria-controls="collapseSearch" aria-expanded="false">
	    		<span class="sr-only"><?php echo __('Close overlay', 'lbi-digitas-theme'); ?></span>
	    	    </button>
	    	    <form action="<?php echo esc_url(home_url('/')); ?>" method="GET">
	    		<div class="header">
	    		    <label for="s" class="tt"><?php esc_attr_e('Search', 'lbi-digitas-theme'); ?></label>
	    		</div>
	    		<div class="lmt-layer-search_block">
	    		    <input type="search" name="s" id="s" value="<?php echo get_search_query() ?>" placeholder="<?php echo $title; ?>">
	    		    <button type="submit" class="submit-search icon-search">
	    			<span class="sr-only"><?php esc_attr_e('Search', 'lbi-digitas-theme'); ?></span>
	    		    </button>
	    		</div>
	    		<input type="hidden" name="origin" value="LMT">
	    	    </form>
	    	</div>
	        </div>
	    </div>

	<?php elseif ($show_block && $block_type == 'call_back') : ?>

	    <?php
	    $elements = $block['type_of_block'][0]['call_back_repeater'];
	    $nbElement = count($elements);
	    ?>
	    <div class="lmt-panel">
		<?php
		$customPictoCallBack = isset($block['type_of_block'][0]['call_back_picto']) ? $block['type_of_block'][0]['call_back_picto'] : '';
		$sStyleBackground = $sClassIcon = '';
		if ($customPictoCallBack) :
		    $sStyleBackground = sprintf($sBackgroundTemplate
			    , $customPictoCallBack['sizes']['medium']
		    );
		else :
		    $sClassIcon = 'icon-phone';
		endif;
		?>
	        <button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'contact-us'});" type="button" class="lmt-btn contact-us <?php echo $sClassIcon ?>" data-toggle="collapse" data-target="#collapseContact" aria-controls="collapseContact" aria-expanded="false" <?php echo $sStyleBackground ?>>
	    	<span class="lmt-button-title"><?php echo __('Contact us', 'lbi-digitas-theme') ?></span>
	        </button>

	        <div class="collapse" id="collapseContact">
	    	<div class="lmt-layer lmt-layer-contact">
	    	    <button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseContact" aria-controls="collapseContact" aria-expanded="false">
	    		<span class="sr-only"><?php echo __('Close overlay', 'lbi-digitas-theme'); ?></span>
	    	    </button>

			<?php if ($nbElement > 1) : ?>
			    <form action="" method="POST">
				<div class="header">
				    <p class="tt">
					<label for="contact-input"><?php echo __('Contact us', 'lbi-digitas-theme') ?></label>
					<select class="custom-select">
					    <?php
					    // One builds the options of the select which display the div
					    foreach ($elements as $row):
						$select_value = $row['call_back_select_a_value'];
						if ($select_value):
						    ?>
						    <option value="<?php echo sanitize_title($select_value); ?>"><?php echo $select_value; ?></option>
						    <?php
						endif;
					    endforeach;
					    ?>
					</select>
				    </p>
				</div>
			    </form>
			<?php endif; ?>

			<?php
			if ($elements):
			    foreach ($elements as $key => $row):
				$select_value = $row['call_back_select_a_value'];
				if ($key === 0 && $nbElement > 1)
				    continue;
				?>

		    	    <div id="<?php echo sanitize_title($select_value); ?>" class="lmt-contact-info <?php
				if ($nbElement === 1) {
				    echo "active";
				}
				?>">
		    		<article>
		    		    <header>
		    			<div class="lmt-contact-info-icon">
						<?php
						$picture = $row['call_back_icon_1'];
						if ($picture) {
						    ?>
						    <img src="<?php echo $picture['url']; ?>" />
						<?php } else { ?>
						    <span class="assistance-icon">
							<svg viewBox="85.5 13.5 45.6 39.9">
							<path d="M128.3,27.2l-3.7,0l-0.7,0.1l-0.1-0.7c-1.3-7.6-7.9-13.1-15.6-13.1c-7.7,0-14.2,5.5-15.6,13.1l-0.1,0.7
							      l-0.8-0.1l-3.6,0c-1.5,0-2.7,1.2-2.7,2.7v7c0,1.5,1.2,2.7,2.7,2.7l4.3,0l0,1.2c0,4.8,3.8,8.8,8.6,9.1l0.7,0v0.7
							      c0,1.5,1.2,2.7,2.7,2.7h7c1.5,0,2.7-1.2,2.7-2.7v-3.5c0-1.5-1.2-2.7-2.7-2.7h-7c-1.5,0-2.7,1.2-2.7,2.7v0.8l-0.8-0.1
							      c-3.7-0.4-6.5-3.4-6.5-7.2V29.3c0-7.6,6.2-13.8,13.8-13.8c7.6,0,13.8,6.2,13.8,13.8l0,7.6c0,1.5,1.2,2.7,2.7,2.7h3.5
							      c1.5,0,2.7-1.2,2.7-2.7v-7C131,28.4,129.8,27.2,128.3,27.2z M92.4,37.7h-5v-8.5h5V37.7z M103.7,46.5l0.8,0h7.8v5h-8.5V46.5z
							      M129.1,37.7h-5v-8.5h5V37.7z M110.1,37c-0.4,0.6-1,1-1.7,1.1c-0.8,0.1-1.6-0.3-2.1-1c-0.3-0.5-1-0.7-1.5-0.3
							      c-0.5,0.3-0.7,1-0.3,1.5c0.8,1.3,2.2,2.1,3.7,2.1c0.1,0,0.3,0,0.4,0c1.4-0.1,2.7-0.9,3.5-2.2c0.3-0.5,0.1-1.2-0.4-1.5
							      C111.1,36.3,110.4,36.4,110.1,37z"/>
							</svg>
						    </span>
						<?php } ?>
		    			</div>

					    <?php
					    $title = $row['call_back_title'];
					    if ($title):
						?>
						<h3 class="tt"><?php echo $title; ?></h3>
					    <?php endif; ?>
		    		    </header>

					<?php
					$text = $row['call_back_text'];
					if ($text):
					    ?>
					    <p><?php echo $text; ?></p>
					    <?php
					endif;

					$phone = $row['call_back_telephone'];
					if ($phone):
					    ?>
					    <a target="_blank" href="tel:<?php echo str_replace([" ", "-", "_"], '', $phone); ?>"><?php echo $phone; ?></a>
					    <?php
					endif;

					$link = $row['call_back_link'];
					$label = $row['call_back_link_label'];
					if ($link):
					    ?>
					    <p class="lmt-contact-email">

						<?php
						$picture = $row['call_back_icon_2'];
						if ($picture) {
						    ?>

			    			<a href="mailto:<?php echo $link; ?>">
			    			    <img src="<?php echo $picture['url']; ?>" /><br>
							<?php
							if ($label) {
							    echo $label;
							} else {
							    echo __('Send us an email', 'lbi-digitas-theme');
							}
							?>
			    			</a>

						<?php } else { ?>

			    			<a href="mailto:<?php echo $link; ?>">
			    			    <i class="icon-envelop"></i>
			    			    <br>
							<?php
							if ($label) {
							    echo $label;
							} else {
							    echo __('Send us an email', 'lbi-digitas-theme');
							}
							?>
			    			</a>

						<?php } ?>

					    </p>
					<?php endif; ?>

					<?php if ($show_wcb && $wcb_id) : ?>
					    <div class="lmt-webcallback">
						<a class="btn-digitas btn-digitas-red LnkWcbForm-trigger"><?php echo get_field('web_callback_cta_label', 'option'); ?></a>
					    </div>
					<?php endif ?>

		    		</article>
		    	    </div>
				<?php
			    endforeach;
			endif;
			?>
	    	</div>
	        </div>		
	    </div>

	<?php elseif ($show_block && $block_type == 'request_a_quote') : ?>

	    <div class="lmt-panel">

		<?php
		// LINK
		$link = $block['type_of_block'][0]['request_a_quote_link'];
		if ($link) :
		    $title = $block['type_of_block'][0]['request_a_quote_title'];
		    $customPictoRequestQuote = isset($block['type_of_block'][0]['request_a_quote_picto']) ? $block['type_of_block'][0]['request_a_quote_picto'] : '';
		    $sStyleBackground = $sClassIcon = '';
		    if ($customPictoRequestQuote) :
			$sStyleBackground = sprintf($sBackgroundTemplate
				, $customPictoRequestQuote['sizes']['medium']
			);
		    else :
			$sClassIcon = 'icon-messages';
		    endif;
		    ?>
		    <button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'request-a-quote'});" type="button" class="lmt-btn quote <?php echo $sClassIcon ?>" data-toggle="collapse" data-target="#collapseQuote" aria-controls="collapseQuote" aria-expanded="false" <?php echo $sStyleBackground ?>>
			<span class="lmt-button-title"><?php echo $title ?></span>
		    </button>

		    <div class="collapse" id="collapseQuote" data-iframe="<?php echo $link ?>">
			<div id="lmt-quote" class="lmt-layer lmt-layer-quote form-layer" aria-hidden="true">

			    <button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseQuote" aria-controls="collapseQuote" aria-expanded="false">
				<span class="sr-only"><?php echo __('Close overlay', 'lbi-digitas-theme'); ?></span>
			    </button>
			    <?php if ($title) : ?>
		    	    <div class="header">
		    		<p class="tt"><?php echo $title ?></p>
		    	    </div>
			    <?php endif; ?>

			    <?php if ($link) : ?>
		    	    <div class="iframe-container"></div>
			    <?php endif; ?>
			</div>
		    </div>

		<?php endif; ?>

	    </div>

	<?php elseif ($show_block && $block_type == 'external_link') : ?>

	    <div class="lmt-panel">

		<?php
		// LINK
		$linkExtLink = $block['type_of_block'][0]['external_link'];
		if ($linkExtLink) :
		    $extLink = $block['type_of_block'][0]['external_link'];
		    $customPictoExtLink = isset($block['type_of_block'][0]['external_link_picto']) ? $block['type_of_block'][0]['external_link_picto'] : '';
		    $sStyleBackground = $sClassIcon = '';
		    if ($customPictoExtLink) :
			$sStyleBackground = sprintf($sBackgroundTemplate
				, $customPictoExtLink['sizes']['medium']
			);
		    else :
			$sClassIcon = 'icon-shopping';
		    endif;
		    // if ($customPicto) :
		    ?>
		    <a onclick="dataLayer.push({'event': 'click-LMT', 'value': 'external-link'});" class="lmt-link" href="<?php echo $extLink['url']; ?>" target="<?php echo $extLink['target']; ?>">
			<button class="lmt-btn extlink  <?php echo $sClassIcon ?>" <?php echo $sStyleBackground ?>>
			    <span class="lmt-button-title"><?php echo $extLink['title']; ?></span>
			</button>
		    </a>
		<?php endif; ?>

	    </div>

	<?php elseif ($show_block && $block_type == 'store_locator') : ?>

	    <div class="lmt-panel">

		<?php
		$linkStoreLocaTitle = $block['type_of_block'][0]['store_locator_lmt_title'];
		$linkStoreLoca = $block['type_of_block'][0]['links'];
		$customPictoStoreLocator = isset($block['type_of_block'][0]['store_locator_picto']) ? $block['type_of_block'][0]['store_locator_picto'] : '';
		?>
		<?php if ($linkStoreLoca && count($linkStoreLoca) == 1) : ?>
		    <a onclick="dataLayer.push({'event': 'click-LMT', 'value': 'store-locator'});" class="lmt-link" href="<?php echo $linkStoreLoca[0]['link']['url'] ?>" target="_blank">
			<button class="lmt-btn extlink <?php if (!$customPictoStoreLocator): ?>icon-locator<?php endif ?>" <?php if ($customPictoStoreLocator): ?>style="background-image:url('<?php echo $customPictoStoreLocator['sizes']['medium']; ?>');background-size:contain;background-repeat:no-repeat;background-position:left"<?php endif ?>>
			    <span class="lmt-button-title"><?php echo $linkStoreLocaTitle; ?></span>
			</button>
		    </a>
		<?php else : ?>
		    <button onclick="dataLayer.push({'event': 'click-LMT', 'value': 'store-locator'});" type="button" class="lmt-btn locator <?php if (!$customPictoStoreLocator): ?>icon-locator<?php endif ?>" data-toggle="collapse" data-target="#collapseStoreloca" aria-controls="collapseStoreloca" aria-expanded="false" <?php if ($customPictoStoreLocator): ?>style="background-image:url('<?php echo $customPictoStoreLocator['sizes']['medium']; ?>');background-size:contain;background-repeat:no-repeat;background-position:left"<?php endif ?>>
			<span class="lmt-button-title"><?php echo $linkStoreLocaTitle; ?></span>
		    </button>
		<?php endif ?>
		<?php if ($linkStoreLoca && count($linkStoreLoca) > 1) : ?>

		    <div class="collapse" id="collapseStoreloca">
			<div class="lmt-layer lmt-layer-storeloca">
			    <button type="button" class="lmt-close" data-toggle="collapse" data-target="#collapseStoreloca" aria-controls="collapseStoreloca" aria-expanded="false">
				<span class="sr-only"><?php echo __('Close overlay', 'lbi-digitas-theme'); ?></span>
			    </button>
			    <div class="header">
				<p class="tt"><?php echo $linkStoreLocaTitle; ?></p>
			    </div>
			    <div class="lmt-layer-storeloca_block">
				<?php foreach ($linkStoreLoca as $link): ?>
		    		<a class="btn-digitas btn-digitas-red" href="<?php echo $link['link']['url']; ?>" target="<?php echo $link['link']['target']; ?>">
					<?php echo $link['link']['title']; ?>
		    		</a>
				<?php endforeach; ?>
			    </div>
			</div>
		    </div>

		<?php endif; ?>

	    </div>

	<?php endif; ?>

    <?php endforeach; ?>

<?php endif; ?>
