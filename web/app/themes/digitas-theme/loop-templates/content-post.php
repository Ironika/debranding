<?php
/**
 * Partial template for content in homepage-blog.php
 *
 * @package understrap
 */

// Load posts
$date = get_the_date( 'd.m.Y',  get_the_ID());
// $content = apply_filters('the_content', the_content());
// $content = str_replace(']]>', ']]&gt;', the_content());
?>

<a href="<?php the_permalink() ?>">

	<?php
		if ( has_post_thumbnail( get_the_ID()) ) : ?>
			<img src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium'); ?>" alt="<?php echo the_title(); ?>" />
		  <?php // echo get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'blog-component--container_image' ) ); ?>
		<?php endif; ?>
	<div class="blog-component--container">

		<?php
		$categories = get_the_category();

		if ( !empty( $categories ) ) :
		?>
			<p class="blog-component--container_cat"><?php echo esc_html( $categories[0]->name ); ?></p>
		<?php endif; ?>

		<p class="blog-component--container_date"><?php echo $date; ?></p>
		<p class="blog-component--container_title"><?php echo the_title(); ?></p>
		<?php if(get_post_type() != SOD_TES_PTYPE) : ?>
		<div  class="blog-component--container_content"><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></div>
		<?php endif ?>
		<p class="blog-component--container_link icon-arrow-right">
			<span><?php echo __('Read more', 'lbi-digitas-theme') ?></span>
		</p>
	</div>
</a>
