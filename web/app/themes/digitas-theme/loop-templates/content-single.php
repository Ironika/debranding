<?php
/**
 * Single post partial template.
 *
 * @package understrap
 */

?>
<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">


	<header class="entry-header">
		<!-- display title -->
		<?php the_title( '<h1 class="digitas-article-title">', '</h1>' ); ?>
		<!-- display author -->
		<p class="article-infos"><?php echo get_the_date( 'd M Y'); ?></p>
	</header><!-- .entry-header -->

	<div class="entry-content static-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
