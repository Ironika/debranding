<?php
/*
Template Name: Life Quality
*/
get_header();
?>
<?php if (have_posts()) : $page = get_post(get_the_ID()); ?>
  <div class="banner-home banner-life-quality <?php if (get_field('life_quality_banner_color') == 'blacktxt') : echo 'lighten'; endif; ?>">

<div class="banner-home-type <?php
if (get_field('life_quality_enable_banner_video')) : echo 'video-on';
else : echo 'video-off';
endif;
?>">

<div class="banner-home-type-image">
  <?php
  // we alaways show the image on mobile because the autoplay function is disabled on mobile
  $hp_image = get_field('life_quality_banner_image');
  if ($hp_image) :
    ?>
    <img src="<?php echo $hp_image['url'] ?>" alt="<?php echo $hp_image['alt'] ?>" />
  <?php endif; ?>
</div>


<?php if (get_field('life_quality_enable_banner_video')) : ?>

  <div class="banner-home-type-video">
    <div class="banner-home-type-video-container">

      <?php

      // The iframe is placed on the image if enabled but hidden on mobile (no autoplay)
      $url = get_field('life_quality_banner_video', false, false);
      preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
      $video_id = $matches[1];


      // $url = get_field('life_quality_banner_video', false, false);
      //
      // parse_str(parse_url($url, PHP_URL_QUERY), $vars);
      // $video_id = $vars['v'];
      ?>

      <div id="digitasVideoPlayerBanner"></div>

      <script>
      var tag = document.createElement('script');

      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      function onYouTubeIframeAPIReady() {
        var player;
        player = new YT.Player('digitasVideoPlayerBanner', {
          videoId: '<?php echo $video_id; ?>',
          // width: 2000,       // Player width (in px)
          // height: 850,       // Player height (in px)
          playerVars: {
            playlist: '<?php echo $video_id; ?>',
            autoplay: 1,
            controls: 0,
            showinfo: 0,
            modestbranding: 1,
            loop: 1,
            fs: 0,
            cc_load_policy: 0,
            iv_load_policy: 3,
            autohide: 0
          },
          events: {
            'onReady' : onPlayerReady
          }
        });
      }

      function onPlayerReady(event) {
        event.target.mute();
        // event.target.playVideo();
      }
      </script>

    </div>
  </div>

<?php endif; ?>

</div>

<div class="banner-home-text">
    <?php
    $hp_text = get_field('life_quality_banner_text');
    $hp_text_subtitle = get_field('life_quality_banner_text_subtitle');
    if ($hp_text) { ?>
      <h1><?php echo $hp_text; ?></h1>
    <?php }
    if ($hp_text_subtitle) { ?>
      <p class="subtitle"><?php echo $hp_text_subtitle; ?></p>
    <?php } ?>
  <?php /* <a href="#"><i class="fa fa-angle-down fa-lg" aria-hidden="true"></i></a */ ?>
</div>

</div>

<?php get_template_part( 'layouts-acf/block_col-offers' ); // Load columns offers layout ?>

<?php get_template_part( 'layouts-acf/block_image-list' ); // Load image and list layout ?>

</div>
<?php echo do_shortcode($page->post_content); ?>
<?php endif; ?>

<div class="breadcrumb">
    <?php
    if(function_exists('bcn_display')):
        bcn_display();
    endif; ?>
</div>

<?php get_footer(); ?>
