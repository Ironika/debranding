// (function lmtContact() {
//
//     var els = {},
//         openedItem;
//
//     /**
//      * Attach event handlers
//      */
//     function bind() {
//         els.selects.forEach( function( select ) {
//             select.addEventListener( 'change', toggleLayer );
//         });
//     }
//
//
//     /**
//      * Register needed DOM elements
//      */
//     function dom() {
//         els.selects = [].slice.call( document.querySelectorAll( '.lmt-layer-contact .custom-select' ));
//     }
//
//
//     /**
//      * Init everything
//      */
//     function init() {
//         dom();
//
//         if ( !els.selects ) {
//             return;
//         }
//
//         bind();
//     }
//
//
//     /**
//      * Toggle layer on/off
//      * @param {object} e Change event object
//      */
//     function toggleLayer( e ) {
//         var target = document.getElementById( e.target.value );
//
//         // close previous opened layer
//         if ( openedItem ) {
//             openedItem.style.display = 'none';
//         }
//
//         if ( target ) {
//             openedItem = target;
//             target.style.display = 'block';
//         }
//     }
//
//
//     // kickstart module
//     document.addEventListener( 'DOMContentLoaded', init );
// })();
