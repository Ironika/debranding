/*global responsiveCarousel: true*/
/*global loadMore: true*/
/*global storeIframe: true*/
/**
* Main scripts
**/

jQuery(document).ready(function($) {

  // Menu top Tag click
  $('#menu-top-nav a').click(function() {
    var menutopitemname = $(this).text();
    dataLayer.push({
      'event':'click-header-menu',
      'name':menutopitemname
    });
  });

  // Burger menu
  $('#menu-burger-menu li.ask-for-a-quote a').click(function() {
    dataLayer.push({
      'event':'click-get-quote'
    });
  });

  // Download .pdf tag click
  $('a[href$=".pdf"]').click(function() {
    var pdfname = $(this).text();
    dataLayer.push({
      'event':'download',
      'name':pdfname
    });
  });

  $('.prod-tabs--content a.btn-show-more-advantages').click(function(e) {
    e.preventDefault();
    $(this).closest('.see-more-advantages').hide();
    $('.prod-tabs--content .advantage-list li').show();
  });

  // prevent carousel from jumping if clicking while sliding
  // $('.carousel-control-next, .carousel-control-prev').on('click', function(e) {
  //   e.preventDefault();
  // });

  // $(window).on('resize load', $.throttle( 250, function(){
  //   responsiveCarousel();
  // }));

  // Match height elements
  $('.sameheight').matchHeight();


  // Slick sliders init

  $('.top-menu-carousel-titles-slick').slick({
    swipe: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    speed: 200,
  });

  $('.otherneeds-carousel-slick').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
  });

  var $slider = $('.top-menu-carousel-slick').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    asNavFor: '.top-menu-carousel-titles-slick',
    responsive: [
      {
        breakpoint: 767,
        settings: {
          dots: false,
        }
      }
    ]
  });

  $('.choose-digitas-slick').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    autoplay: true,
  });

  // product page steps mobile
  $('.getting-pass-steps-slick').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
  });

  // Product page cross sell mobile
  $('.cross-sell-product-slick').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
  });

  // Product page cross sell mobile
  $('.become-affiliate-slick').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
  });





  // Go to the right slide when hovering the menu
  $('.top-menu--tabs li a').click(function(e) {
    e.preventDefault();
    var slideId = $(this).data('slide-to');
    $slider.slick('slickGoTo', slideId);
    $('html, body').animate({
      scrollTop: $(".top_menu ").offset().top - 100
    }, 300);
  });

  // On before slide change add .active class on the menu
  $('.top-menu-carousel-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $('.top-menu--tabs-popup li a').removeClass('active');
    $('.top-menu--tabs-menu li a').removeClass('active');
    var mySlideNumber = nextSlide;
    $('.top-menu--tabs-popup li a').eq(mySlideNumber).addClass('active');
    $('.top-menu--tabs-menu li a').eq(mySlideNumber).addClass('active');
  });

  // Go to the wanted slide on click
  $('.top-menu--header_top-solution').click(function(e) {
    e.preventDefault();
    var slideId = $(this).data('slide-to');
    $slider.slick('slickGoTo', slideId);
  });

  $('.single-carousel-slick').slick({
    autoplay: true,
    autoplaySpeed: 6000,
    dots: true,
    infinite: true,
    speed: 600,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true
  });

  $('.carousel-theytrustus-slick').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 5,
    slidesToScroll: 5,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          dots: false,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });


  // Add class to navbar when scrolling
  var header = $("body.home #navbar-combined");
  $(window).scroll(function() {
      var scroll = $(window).scrollTop();
      if (scroll > 700) {
          header.addClass("sticky-nav");
      } else {
          header.removeClass("sticky-nav");
      }
  });

  $(document).on('click', '[data-load-more]', function( e ){
    // load more
    e.preventDefault();
    loadMore(e);
  });


  // detect swipe
  var xDown = null;

  function handleStart(e) {
    xDown = e.touches[0].clientX;
  }
  function handleTouchMove(e) {
      if ( !xDown ) {
          return;
      }

      var xUp = e.touches[0].clientX,
          xDiff = xUp - xDown;

      if ( xDiff > 5 ) {
        $('#sideNav').collapse('hide');
      }
      /* reset values */
      xDown = null;
  }

  // prevent body from scrolling if layer is open
  $('#sideNav').bind('show.bs.collapse', function (e) {
    if( e.currentTarget !== e.target ){
      return;
    }
    $('body').css({
      'top': -window.pageYOffset +'px'
    }).addClass('prevent-scroll');

    // Focus disabled (ES ticket : https://tools.publicis.sapient.com/jira/browse/SOXINT-261)
    // window.requestAnimationFrame(function(){
    //   $('#sideNav input:text:visible:first').focus();
    // });

    // bind swipe listener to close nav on mobiles
    document.addEventListener('touchstart', handleStart, false);
    document.addEventListener('touchmove', handleTouchMove, false);

  });
  $('#sideNav').bind('hidden.bs.collapse', function (e) {
    if( e.currentTarget !== e.target ){
      return;
    }
    var top = parseInt( $('body').css('top'), 10) *-1;
    $('body').removeClass('prevent-scroll').css({
      'top': ''
    });
    window.scrollTo(0, top);

    // unbind swipe listener to close nav on mobiles
    document.removeEventListener('touchstart', handleStart);
    document.removeEventListener('touchmove', handleTouchMove);
  });


    // Toggle class on element
    $( document ).on( 'click', "[data-toggle-class]", function( e ){
      e.preventDefault();
      $( '#'+ $(this).data('toggle-class') ).toggleClass('active');
    });

    // When a tab link is clicked
    $( "a.tab-nav-link" ).click(function() {
      // Get the text of the clicked link
      var navlink = $(this);
      var htmlString = navlink.text();
      // Put it in the fake select
      $( "a.fake-select-link span" ).text(htmlString);

      // Get his href attributes
      var hreflink = navlink.attr("href");

      // Sync the tab with the modal menu by toggling the active class on tabs
      $( "a.modal-nav-link" ).removeClass('active');
      $( "a.modal-nav-link[href$="+hreflink+"]" ).addClass('active');

      // Hide the modale
      $('#MegaTabMenuModal').modal('hide');
    });

    // When link in modal is clicked (product page)
    $( "a.modal-nav-link" ).click(function() {
      // Get the text of the clicked link
      var navlink = $(this);
      var htmlString = navlink.text();
      // Put it in the fake select
      $( "a.fake-select-link span" ).text(htmlString);

      // Get his href attributes
      var hreflink = navlink.attr("href");

      // Sync the tab with the modal menu by toggling the active class on tabs
      $( "a.tab-nav-link" ).removeClass('active');
      $( "a.tab-nav-link[href$="+hreflink+"]" ).addClass('active');

      // Hide the modale
      $('#MegaTabMenuModal').modal('hide');
    });

    // When link in modal is clicked (homepage top carousel)
    $( "a.top_menu-nav-link" ).click(function(e) {
      e.preventDefault();
      // Go to the wanted slide on click
      var slideId = $(this).data('slide-to');
      $slider.slick('slickGoTo', slideId);

      // Hide the modale
      $('#CarouselMenuModal').modal('hide');
    });


    // Open tabs with URL Hash on product page
    var url = document.location.toString();
    if (url.match('#tab')) {
      $('#wrapper-product-megatab .nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
      $('html, body').animate({
        scrollTop: $("#wrapper-product-megatab").offset().top
      }, 300);
    }
    // Change hash in URL for page-reload
    $('#wrapper-product-megatab .nav-tabs a, #MegaTabMenuModal .nav-item a').click(function (e) {
      window.location.hash = e.target.hash;
    })


  // Blog HP masonry grid
   $(window).load( function(){
    $('.blog-component--block ul').masonry({
      itemSelector: '.blog-item',
      columnWidth: '.blog-item',
      percentPosition: true
    });
   })

  // detect if is on IE11 and add class ie11 to body element
  /*
  var isIE11 = !!window.MSInputMethodContext && !!document.documentMode;
  if (isIE11) {
   $('body').addClass('ie11');
  }
  */

  $('#document-needed-selector').change(function(){
    $('.type-contents').hide();
    $('#back-document-needed').show();
    $('#select-document-needed').hide();
    $('.info-blocks_pretitle-title').hide();
    $('#' + $(this).val()).show();
  });

  $(document).on('click', '#back-document-needed', function(event) {
        event.preventDefault();
        $('#select-document-needed').show();
        $('#back-document-needed').hide();
        $('.type-contents').hide();
        $('.info-blocks_pretitle-title').show();
    });

    // -------------------------------------------------------------------------
    // INSCRIPTION NEWSLETTER FOOTER
    //-------------------------------------------------------------------------
    //
    // Fonction ouvre popin
    function openPopinNews($titrePopin, $textPopin) {
      if(! $("#modalNewsletter").length > 0 ) {
        $( '<div id=\"modalNewsletter\" class=\"modal fade \" tabindex=\"-1\"><div class=\"modal-dialog\"><div class=\"modal-content\"><div class=\"modal-header\"><button class=\"close\" type=\"button\" data-dismiss=\"modal\">×</button><h4 id=\"modalNewsletterLabel\" class=\"modal-title\"></h4></div><div class=\"modal-body\">'+$textPopin+'</div><div class=\"modal-footer\"><button class=\"btn btn-default\" type=\"button\" data-dismiss=\"modal\">' + closetext + '</button></div></div></div></div>'  ).appendTo("body");
      }
      // Open modal
      $('#modalNewsletter').modal('show');
    }

    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

    $("form.form_insc_news").submit(function( e ) {

      e.preventDefault();

      var newsFormInput = $(this).find('input[type=email]');
      var userEmail = newsFormInput.val();

      if (validateEmail(userEmail)) { // email validation check

        // recuperer data
        var newsFormAction = $(this).attr("action");
        var attrPosition = newsFormInput.attr("data-layer-position");

        // Envoyer data Ajax
        $.ajax({
          method: "POST",
          url: newsFormAction,
          data: { userEmail: userEmail },
          success: function (data) {
            openPopinNews ("titre" , data);
            dataLayer.push({
              'event':'form-newsletter',
              'location':attrPosition
            });
          },
            error: function(xhr, status, error) {
              openPopinNews ("error" , error);
            }

        }) //END AJAX

      }

    }); // END Form submit


/*
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
 ACTIONS SPECIFIQUES A CERTAINES PAGES
–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
*/

		/*
		   ACTIONS HOME
		––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
		if ($("body").is(".home")){

			// Click fleche bas banner
			$("#banner_arrow_down").on( 'click', function(e) {
				$('html, body').animate({
					scrollTop: $(".top_menu ").offset().top - 100
				}, 1000);
			});

		}
		/*
		   ACTIONS ARCHIVE FAQS
		––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––– */
		else if ($("body").is(".post-type-archive-faq")){

			if (location.href.indexOf("#topics") != -1) {
				$('html, body').animate({
					scrollTop: $("#topics").offset().top - $("#MenuTop").height() - 10
				}, 500);
			}

		}



	/*
	–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
	 ACTIONS POUR TOUS LES ACCORDIONS
	–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
	*/

		$('.panel-group .panel-default').on('click', function () {

			$('.panel-collapse.in').collapse('hide');

		});



    // LMT autoclose & overlay
    $('#lead-management-tools .collapse').on('show.bs.collapse', function () {
      $('#lead-management-tools .collapse.show').collapse('hide');
      $('#lmt-overlay').addClass('show');
    })

    $('#lead-management-tools .collapse').on('hide.bs.collapse', function () {
      $('#lmt-overlay').removeClass('show');
    })

    $('#lmt-overlay').on("click", function () {
      $('#lead-management-tools .collapse.show').collapse('hide');
    })

    // LMT popin iframe load
    $('#lead-management-tools #collapseQuote').on('shown.bs.collapse', function () {
      var collapsedquote = $(this);
      var recipientquote = collapsedquote.data('iframe');
      collapsedquote.find('.iframe-container').html('<iframe src="' + recipientquote + '" border="0"></iframe>')
    })

    // Footer popin contact-us
    $('#contactPopinFooter .collapse').on('show.bs.collapse', function () {
      $('#contactPopinFooter .collapse.show').collapse('hide');
    })

    // Footer popin iframe load
    $('#contactPopinFooter #collapseMessagefooter').on('shown.bs.collapse', function () {
      var collapsedpopfooter = $(this);
      var recipientpopfooter = collapsedpopfooter.data('iframe');
      collapsedpopfooter.find('.iframe-container').html('<iframe src="' + recipientpopfooter + '" border="0"></iframe>');
    })

    /*
  	–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
  	 ACTIONS CLICK LEAD MANAGEMENT
  	–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
  	*/

    // function focusInputSearch(){
    //     $("#collapseSearch").find( "input[type='search']" ).focus();
    // }

    // Focus sur le search
    $('#lead-management-tools #collapseSearch').on('shown.bs.collapse', function () {
      $(this).find( "input[type='search']" ).focus();
    });

    /*
  	–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
  	 ACTIONS CLICK lmt-layer-contact & contact footer popin
  	–––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––
  	*/
    $('.lmt-layer-contact select.custom-select').on('change', function ( e ) {

        // Hide all divs
        $(this).closest("form").parent().find(".lmt-contact-info").hide();

        // Show div
        var target = $(this).find("option:selected").attr("value");
        $('#'+target).show();
    });





  if( document.querySelector('[data-iframe-map]') ){
    storeIframe( document.querySelector('[data-iframe-map]') );
  }

  /*Send pdf function*/
  $( "#mail-pdf-form" ).submit(function( event ) {


  	 var formurl=$("#mail-pdf-form").attr("action");
	 var usermail=$('#send_pdf').val();
	//$("#mail-pdf-form").html(formurl);

  if (validateEmail(usermail)) { // email validation check

	 $.ajax({
     	 type: "POST",
		 url: formurl,
		 data: { send_pdf_mail: usermail},
		 cache: false,
         success: function(){

                 $("#pdf_modal_success").removeClass("hidden-xs-up");
				 $('#mail-pdf-form .input-group').addClass("hidden-xs-up");
         },
		 error: function(){
 				$("#pdf_modal_error").removeClass("hidden-xs-up");
 		}
       });
     }

	  event.preventDefault();
  });


  // Outdatedbrowser event listener: DOM ready
  function addLoadEvent(func) {
      var oldonload = window.onload;
      if (typeof window.onload != 'function') {
          window.onload = func;
      } else {
          window.onload = function() {
              if (oldonload) {
                  oldonload();
              }
              func();
          }
      }
  }

  // Outdatedbrowser call plugin function after DOM ready
  addLoadEvent(function() {
      outdatedBrowser({
          bgColor: '#f25648',
          color: '#ffffff',
          lowerThan: 'borderImage'
      })
  });

  $('#typeformModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('typeform') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-body').html('<iframe src="' + recipient + '" border="0"></iframe>')
  });


    var posts_per_page = 16; // Post per page
    var pageNumber = 1;
    var cat =  $("#more_posts").data('category') // Extract info from data-* attributes;

    function load_posts(){
        pageNumber++;

        var str = '&cat=' + cat + '&pageNumber=' + pageNumber + '&posts_per_page=' + posts_per_page + '&action=blog_action';
        $.ajax({
            type: "POST",
            dataType: "html",
            url: ajaxurl,
            data: str,
            success: function(data){
                var $data = $(data);
                if($data.length){
                    $("#ajax-posts").append($data);
                    $("#more_posts").attr("disabled",false);
                    $("#more_posts").hide();
                } else{
                    $("#more_posts").attr("disabled",true);
                }
            },
            error : function(jqXHR, textStatus, errorThrown) {
                $loader.html(jqXHR + " :: " + textStatus + " :: " + errorThrown);
            }
        });
        return false;
    }
    $("#more_posts").on("click",function(event){ // When btn is pressed.
        event.preventDefault();
        $("#more_posts").attr("disabled",true); // Disable the button, temp.
        load_posts();
    });


    window.mobileAndTabletcheck = function() {
      var check = false;
      (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
      return check;
    };

    if (window.mobileAndTabletcheck() == true) {
      $('.banner-home-type .banner-home-type-video').hide();
    }

});
