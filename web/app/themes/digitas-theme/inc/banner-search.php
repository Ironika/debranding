
<form method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<div class="input-group">

		<input class="field form-control" id="s" name="s" type="text" value="<?php echo get_search_query() ?>"
			placeholder="<?php esc_attr_e( 'Search', 'lbi-digitas-theme' ); ?>">
		<span class="input-group-btn">
			<input class="submit btn btn-primary" id="searchsubmit" name="submit" type="submit"
			value="<?php esc_attr_e( 'Search', 'lbi-digitas-theme' ); ?>">
	</span>
	</div>
	<input type="hidden" name="search" value="faq">
  <input type="hidden" name="origin" value="FAQ">
</form>

<?php
/*
// Output a search for that only searches portfolio post types
$search = get_search_form(false);
$find = '</form>';
// PASSING THIS TO TRIGGER THE ADVANCED SEARCH RESULT PAGE FROM functions.php
$replace = '' . $find;
$search = str_replace($find, $replace, $search);
echo $search;
*/
?>