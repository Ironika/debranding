<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
get_header();
?>
<div class="blog-homepage">
    <?php
// Get template slider
    get_template_part('templates/content', 'slider-hp-blog');

// Get link ALL
    $args = [
	'post_type' => 'page',
	'fields' => 'ids',
	'nopaging' => true,
	'meta_key' => '_wp_page_template',
	'meta_value' => 'homepage-blog.php'
    ];
    $pages = get_posts($args);
    $plugin = 'dlbi-digitas-testimonials/dlbi-digitas-testimonials.php';
    ?>

    <div class="bg-grey">
	<div class="container">
	    <div class="content">

		<h1 class="digitas-title">
		    <?php if (is_post_type_archive('testimonial')): ?>
			<?php echo __('TESTIMONIALS', 'lbi-digitas-theme'); ?>
			<?php
		    elseif (is_category()):
			$cat = get_the_category();
			if (count($cat) > 0):
			    ?>
			    <?php echo esc_html($cat[0]->name) ?>
			<?php endif; ?>
		    <?php else : ?>
			<?php echo __('ALL', 'lbi-digitas-theme'); ?>
		    <?php endif; ?>
		</h1>

		<!-- Barre de nav -->
		<div class="nav-dropdown mb-5">
		    <ul class="nav nav-pills nav-categories" id="blog-categories">
			<li role="presentation" class="trigger">
			    <?php if (is_post_type_archive('testimonial')): ?>
    			    <button data-toggle-class="blog-categories"><?php echo __('TESTIMONIALS', 'lbi-digitas-theme'); ?></button>
				<?php
			    elseif (is_category()):
				$cat = get_the_category();
				if (count($cat) > 0):
				    ?>
				    <button data-toggle-class="blog-categories"> <?php echo esc_html($cat[0]->name) ?></button>
				<?php endif; ?>
			    <?php else : ?>
    			    <button data-toggle-class="blog-categories"><?php echo __('ALL', 'lbi-digitas-theme'); ?></button>
			    <?php endif; ?>
			</li>

			<li role="presentation" <?php if (!is_post_type_archive('testimonial') and count(get_the_category()) < 1): ?> class="current" <?php endif; ?>>
			    <a href="<?php echo get_permalink($pages[0]) ?>" >
				<?php echo __('ALL', 'lbi-digitas-theme'); ?>
			    </a>
			</li>
			<?php
			// Check if plugin is enabled
			if (is_plugin_active($plugin)) :
			    ?>
    			<li role="presentation" <?php if (is_post_type_archive('testimonial')): ?> class="current" <?php endif; ?>>
    			    <a href="<?php echo get_post_type_archive_link(SOD_TES_PTYPE); ?>" >
				    <?php echo __('TESTIMONIALS', 'lbi-digitas-theme'); ?>
    			    </a>
    			</li>
			    <?php
			endif;

			// Get list category
			$categories = get_categories(array(
			    'orderby' => 'name',
			    'post_type' => 'post'
				)
			);

			foreach ($categories as $category) :
			    ?>
			    <?php
			    if ($category->term_id):
				$idcat = '';
				$cat = get_the_category();
				if (count($cat) > 0):
				    $idcat = $cat[0]->term_id;
				endif;
				?>
				<li role="presentation" <?php if (is_category() and ( $idcat == $category->term_id and $idcat != '')): ?> class="current" <?php endif; ?> ><a href="<?php echo esc_url(get_category_link($category->term_id)) ?>" >
					<?php echo esc_html($category->name) ?>
				    </a></li>
			    <?php endif; ?>
			<?php endforeach; ?>
		    </ul>
		</div>

		<!-- Blog content -->
		<section class="blog-component">
		    <div id="ajax-posts" class="blog-component--block">
			<ul class="row" id="blog-articles">
			    <?php
			    //Protect against arbitrary paged values
			    $paged = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;
			    $array_posts = array('post');
			    // Check if plugin is enabled
			    if (is_plugin_active($plugin)) :
				$array_posts[] = 'testimonial';
			    endif;
			    $is_posts = get_field('show_posts', 'option');

			    if ($is_posts) {
				if (($key = array_search('post', $array_posts)) !== false) {
				    unset($array_posts[$key]);
				}
			    }

			    $args = array(
				'post_type' => $array_posts,
				'posts_per_page' => 16,
				'paged' => $paged
			    );
			    $the_query = new WP_Query($args);
			    $nb_element = $wp_query->found_posts;
			    ?>
			    <?php
			    if ($the_query->have_posts() && is_page_template('homepage-blog.php')) :

				$count_posts = 0;
				while ($the_query->have_posts()) : $the_query->the_post();
				    $count_posts = $the_query->current_post + 1; //counts posts in loop
				    // At position 3 you will see the newsletter
				    if ($count_posts == 3):
					// Get template widget
					echo('<li class="blog-item col-sm-6 col-lg-3">');
					//get_template_part('templates/content', 'widget-newsletter');
					get_sidebar('right-single');
					echo('</li>');
				    endif;
				    if ($count_posts == 9):
					// Display product
					echo('<li class="blog-item col-sm-6 col-lg-3">');
					get_template_part('templates/content', 'product-blog-hp');
					echo('</li>');
				    endif;
				    // Display posts
				    echo('<li class="blog-item col-sm-6 col-lg-3">');
				    get_template_part('loop-templates/content-post');
				    echo('</li>');

				endwhile;
				wp_reset_postdata();
			    elseif (have_posts()) :
				$count_posts = 0; //counts posts in loop
				while (have_posts()) : the_post();
				    echo('<li class="blog-item col-sm-6 col-lg-3">');
				    get_template_part('loop-templates/content-post');
				    echo('</li>');
				    $count_posts++;
				endwhile;
				//echo paginate_links();
				?>
			    <?php endif; ?>
			</ul>
		    </div>
		    <?php /* if ($the_query->have_posts() && is_page_template('homepage-blog.php')) : ?>
		      <div class="search-paginate text-center">
		      <?php
		      // Pagination
		      $big = 999999999; // need an unlikely integer
		      echo paginate_links(array(
		      'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
		      'format' => '?paged=%#%',
		      'current' => max(1, get_query_var('paged')),
		      'total' => $the_query->max_num_pages
		      )); */
		    ?>
		    <!-- </div>-->
		    <?php //endif; ?>
		    <?php if ($nb_element > 16): ?>
    		    <div class="testimonials-cta">
    			<a href="#" id="more_posts" data-category="<?php echo $idcat; ?>"><?php echo __('See more articles', 'lbi-digitas-theme'); ?></a>
    		    </div>
		    <?php endif; ?>
		</section>

	    </div>
	</div>
    </div>
    <div class="breadcrumb">
	<?php
	if (function_exists('bcn_display')):
	    bcn_display();
	endif;
	?>
    </div>
    <?php get_footer(); ?>
