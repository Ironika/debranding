<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package understrap
 */
?>

<div class="col-lg-4 widget-area" id="right-sidebar" role="complementary">
    <?php if (!is_page_template('homepage-blog.php')): ?>
        <div class="social">
    	<span><?php echo __('Share', 'lbi-digitas-theme'); ?> : </span>
    	<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank"><img class="facebook" src="<?php echo get_stylesheet_directory_uri(); ?>/src/assets/images/icon-facebook.svg" alt="Facebook" /></a>
    	<a href="https://twitter.com/share?url=<?php echo get_permalink(); ?>&text=<?php echo get_the_title(); ?>" target="_blank"><img class="twitter" src="<?php echo get_stylesheet_directory_uri(); ?>/src/assets/images/icon-twitter.svg" alt="Twitter" /></a>
        </div>
        <div class="row">
				<div class="blog-item col-md-6 col-lg-12">
			<?php endif; ?>

	    <!-- Display sidebar -->
	    <?php dynamic_sidebar('right-single'); ?>

			<?php if (!is_page_template('homepage-blog.php')): ?>
				</div>

	    <!-- Display related articles -->
		<?php
		$orig_post = get_the_ID();
		$categories = get_the_category($orig_post);

		if ($categories):

		    $category_ids = array();
		    foreach ($categories as $individual_category)
			$category_ids[] = $individual_category->term_id;

		    $args = array(
			'category__in' => $category_ids,
			'post__not_in' => array($orig_post),
			'posts_per_page' => 4 // Number of related posts that will be shown.
		    );

		    $query = new WP_Query($args);
		    if ($query->have_posts()) :
			?>

	    	    <div id="related_posts" class="posts-section col-md-6 col-lg-12">
	    		<h3 class="tt"><?php echo __('RELATED ARTICLES', 'lbi-digitas-theme'); ?></h3>
	    		<ul>
				<?php
				while ($query->have_posts()) : $query->the_post();
				    if ($orig_post != get_the_ID()) :
					?>
		    		    <li>
		    			<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">
		    			    <div class="relatedthumb">
						    <?php if (has_post_thumbnail(get_the_ID())) : ?>
							<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'thumbnail'); ?>" class="blog-component--container_image" />
							<?php // echo get_the_post_thumbnail( get_the_ID(), 'medium', array( 'class' => 'blog-component--container_image' ) ); ?>
						    <?php endif; ?>
		    			    </div>
		    			    <div class="relatedcontent">
		    				<span class="date"><?php echo get_the_date('d.m.Y'); ?></span>
		    				<div><?php echo wp_trim_words(get_post_field('post_content', get_the_ID()), 10); ?></div>
		    			    </div>
		    			</a>
		    		    </li>
				    <?php endif; ?>
				<?php endwhile; ?>
	    		</ul>
	    	    </div>
		    <?php else: ?>
	    	    <p><?php echo __('Sorry, no posts matched your criteria', 'lbi-digitas-theme'); ?></p>
		    <?php
		    endif;
		endif;
		$post = $orig_post;
		wp_reset_query();
		?>

            </div>
    <?php endif ?>
</div>
