<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */
get_header();

$need = get_post(get_the_ID());
?>
<div class="hero-push large hero-push-need">
    <div class="inner">
        <div class="item alt">
            <div class="media-container">
		<?php
		// Get image
		$image = get_field('need_header_image', get_the_ID());

		if (!empty($image)) :
		    ?>
    		<img class="img-fluid" src="<?php echo $image['sizes']['large'] ?>" />
		<?php endif; ?>
            </div>
            <div class="container pushTopContainer">
                <h1 class="title"><?php echo get_post_meta(get_the_ID(), 'need_header_title', true); ?></h1>
                <div class="content"><?php echo get_post_meta(get_the_ID(), 'need_header_description', true); ?></div>

                <div class="push-stats hidden-sm-down">

		    <?php
		    // check if the repeater field has rows of data
		    if (have_rows('needs_header_pictos')):
			foreach (get_field('needs_header_pictos') as $row):
			    ?>
			    <dl class="col-sm push-stats--piece">
				<?php /*
				  if (isset($row['needs_header_picto'])) {
				  echo '<img src="'.$row['needs_header_picto']['url'].'" alt="'.$row['needs_header_picto']['alt'].'" height="'.$row['needs_header_picto']['height'].'" width="'.$row['needs_header_picto']['width'].'" />';
				  }
				 */ ?>
				<dt class="figure"><span>
					<?php
					if (isset($row['needs_header_value'])) {
					    echo $row['needs_header_value'];
					}
					?>
				    </span></dt>
				<dd class="figure-ex">
				    <?php
				    if (isset($row['needs_header_description'])) {
					echo $row['needs_header_description'];
				    }
				    ?>
				</dd>
			    </dl>

			    <?php
			endforeach;
		    endif;
		    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="push-stats container hidden-md-up">

    <?php
    // check if the repeater field has rows of data
    if (have_rows('needs_header_pictos')):
	foreach (get_field('needs_header_pictos') as $row):
	    ?>
	    <dl class="push-stats--piece">
		<?php /*
		  if (isset($row['needs_header_picto'])) {
		  echo '<img src="'.$row['needs_header_picto']['url'].'" alt="'.$row['needs_header_picto']['alt'].'" height="'.$row['needs_header_picto']['height'].'" width="'.$row['needs_header_picto']['width'].'" />';
		  }
		 */ ?>
		<dt class="figure"><span>
			<?php
			if (isset($row['needs_header_value'])) {
			    echo $row['needs_header_value'];
			}
			?>
		    </span></dt>
		<dd class="figure-ex">
		    <?php
		    if (isset($row['needs_header_description'])) {
			echo $row['needs_header_description'];
		    }
		    ?>
		</dd>
	    </dl>

	    <?php
	endforeach;
    endif;
    ?>
</div>

<div class="push-list">
    <!-- <h2 class="digitas-title"><?php echo __('Products highlight', 'lbi-digitas-theme'); ?></h2> -->
    <?php
// check if the repeater field has rows of data
    if (have_rows('need_products_highlight')):
	foreach (get_field('need_products_highlight') as $row):
	    ?>
	    <div class="item<?php
	    if (is_array($row['need_product_color_text']) && in_array('white', $row['need_product_color_text'])) {
		echo " colored";
	    }
	    ?>" <?php echo isset($row['need_product_highlight_background_color']) ? 'style="background-color:' . $row['need_product_highlight_background_color'] . ';' : '' ?>">
		<div class="container">
		    <div class="row">
			<div class="col-6">
			    <h2 class="choose-digitas--container_subtitle"><?php echo $row['need_product_highlight_title']; ?></h2>
			    <p><?php echo $row['need_product_highlight_description']; ?></p>
			    <p class="btns"><a href="<?php echo get_permalink($row['need_product_highlight_link']->ID); ?>" class="btn-digitas btn-digitas-grey"><?php echo __('Discover More', 'lbi-digitas-theme'); ?></a></p>
			</div>
			<div class="col-6">
			    <?php
			    if (isset($row['need_product_highlight_background_image'])) {
				echo '<img src="' . $row['need_product_highlight_background_image']['url'] . '" alt="' . $row['need_product_highlight_background_image']['alt'] . '" height="' . $row['need_product_highlight_background_image']['height'] . '" width="' . $row['need_product_highlight_background_image']['width'] . '" />';
			    }
			    ?>
			    <?php
			    if (isset($row['need_product_highlight_product_image'])) {
				echo '<img class="pdt" src="' . $row['need_product_highlight_product_image']['url'] . '" alt="' . $row['need_product_highlight_product_image']['alt'] . '" height="' . $row['need_product_highlight_product_image']['height'] . '" width="' . $row['need_product_highlight_product_image']['width'] . '" />';
			    }
			    ?>
			</div>
		    </div>
		</div>
	    </div>
	    <?php
	endforeach;
    endif;
    ?>
</div>




<div class="needs-aside">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
		<?php
		echo do_shortcode($need->post_content);
		?>
            </div>
        </div>
    </div>

    <div class="otherneeds">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="digitas-title"><?php echo __('Others needs', 'lbi-digitas-theme'); ?></h2>
                </div>
            </div>
		    <?php
		    $relatedNeeds = get_posts(
			    array(
				'post_type' => 'need',
				'numberposts' => 5,
				'post__not_in' => array(get_the_ID())
			    )
		    );
		    ?>

      <div class="row">
        <div class="col-sm-12">

          <div class="otherneeds-mobile">

      		<div class="otherneeds-carousel-slick">

			    <?php
			    $needsIndex = 0;
			    foreach ($relatedNeeds as $relatedNeed) :
				  ?>
					<div>

							<?php	// Get Image
							$imgSrc = "";
							if (has_post_thumbnail($relatedNeed)) :
									$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $relatedNeed->ID ), "medium" );
									$imgSrc = $thumbnail[0];
								//echo get_the_post_thumbnail($relatedNeed->ID, 'medium', array('class' => 'otherneeds_image'));
							endif; ?>

						<div class="img-other-need-cont" style="background-image:url('<?php echo $imgSrc ; ?>')">
						</div>

    				<div class="relatedcontent">
  				    <h3 class="tt">
      					<a href="<?php echo get_the_permalink($relatedNeed->ID); ?>" rel="bookmark" title="<?php echo $relatedNeed->post_title; ?>">
  						    <?php echo $relatedNeed->post_title; ?>
      					</a>
  				    </h3>
    				</div>

          </div>

				<?php
				$needsIndex++;
			    endforeach;
			    wp_reset_postdata();
			    ?>
        </div><!-- .otherneeds-carousel-slick -->

        </div><!-- .otherneeds-mobile -->





        <div class="otherneeds-desktop">

        <div class="otherneeds-items">

          <div class="row justify-content-center">

        <?php
        $needsIndex = 0;
        foreach ($relatedNeeds as $relatedNeed) :
        ?>
        <div class="col">

            <?php	// Get Image
            $imgSrc = "";
            if (has_post_thumbnail($relatedNeed)) :
                $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $relatedNeed->ID ), "medium" );
                $imgSrc = $thumbnail[0];
              //echo get_the_post_thumbnail($relatedNeed->ID, 'medium', array('class' => 'otherneeds_image'));
            endif; ?>

          <div class="img-other-need-cont" style="background-image:url('<?php echo $imgSrc ; ?>')">
          </div>

          <div class="relatedcontent">
            <h3 class="tt">
              <a href="<?php echo get_the_permalink($relatedNeed->ID); ?>" rel="bookmark" title="<?php echo $relatedNeed->post_title; ?>">
                <?php echo $relatedNeed->post_title; ?>
              </a>
            </h3>
          </div>

        </div>

      <?php
      $needsIndex++;
        endforeach;
        wp_reset_postdata();
        ?>
      </div>
      </div><!-- .otherneeds-carousel-slick -->

    </div><!-- .otherneeds-desktop -->

      </div><!-- .col-sm-12 -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .other-needs -->
</div><!-- .needs-aside -->

<div class="breadcrumb">
    <?php
    if (function_exists('bcn_display')):
	    bcn_display();
    endif;
    ?>
</div>

<?php get_footer(); ?>
