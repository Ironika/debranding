<?php if (get_field('posts_grid_show_title_page')) {

$simplelink = 'Simple link';
$typeform = 'Typeform';
$youtube='Youtube';
?>
<section class="block-posts-grid">

    <div class="container">

      <?php if (get_field('posts_grid_block_title')) { ?>
    	    <div class="row">
    		<div class="col-md-12">
    		    <h1 class="digitas-title"><?php echo get_field('posts_grid_block_title'); ?></h1>
    		</div>
    	    </div>
	    <?php } ?>

	    <div class="row align-items-stretch">
		<?php
		if (have_rows('posts_grid')) :
		    foreach (get_field('posts_grid') as $row) :

			$type = $row['posts_grid_type'];
			$image = $row['posts_grid_image'];
			$image_link = $row['posts_grid_image_link'];

			if ($type != 'Image') {
			    $linked_post_id = $row['posts_grid_link']->ID;
			}

			if ($type == 'Image') :
                if ($row['posts_grid_image_select_a_link_type']):?>
                   <div class="col-12 col-sm-6 col-md-4 sameheight">
                      <div class="block-posts-grid_content block-posts-grid_image">
                        <?php if ($row['posts_grid_image_select_a_link_type'] == $simplelink):
                            if (is_array($row['posts_grid_image_link'])):
                            ?>
                                <?php if ($image_link["url"]): ?><a href="<?php echo $image_link["url"];?>" <?php if($image_link["target"]):?> target="_blank" <?php endif ?> ><?php endif ?><div class="block-posts-grid_content--item" style="background-image: url('<?php echo $row['posts_grid_image']['sizes']['soxo-medium'] ?>');"></div><?php if ($image_link["url"]): ?></a><?php endif ?>
                            <?php endif;?>
                        <?php elseif ($row['posts_grid_image_select_a_link_type'] == $typeform) : ?>
                            <a href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $image_link["url"];?>">
                                <div class="block-posts-grid_content--item" style="background-image: url('<?php echo $row['posts_grid_image']['sizes']['soxo-medium'] ?>');"></div>
                            </a>
                        <?php elseif ($row['posts_grid_image_select_a_link_type'] == $youtube) : ?>
                            <a data-fancybox href="<?php echo $image_link["url"];?>">
                                <div class="block-posts-grid_content--item" style="background-image: url('<?php echo $row['posts_grid_image']['sizes']['soxo-medium'] ?>');"></div>
                            </a>
                        <?php endif; ?>
                      </div>
                   </div>
                <?php endif;?>

			<?php else: ?>
	    		<div class="col-12 col-sm-6 col-md-4 sameheight">
	    		    <div class="block-posts-grid_content block-posts-grid_post">
	    			<a href="<?php the_permalink($linked_post_id); ?>" class="block-posts-grid_content--item">
	    			    <div class="block-posts-grid_content--item_image">
	    				<img src="<?php echo $image['sizes']['soxo-medium'] ?>" alt="" />
	    			    </div>
	    			    <div class="block-posts-grid_content--item_text">
	    				<h3><?php echo get_the_title($linked_post_id) ?></h3>
					    <?php if ($row['posts_grid_description']) { ?>
						<p><?php echo $row['posts_grid_description']; ?></p>
					    <?php } ?>
	    			    </div>
	    			</a>
	    		    </div>
	    		</div>
			<?php
			endif;

		    endforeach;
		    wp_reset_postdata();
		endif;
		?>
	    </div>

    </div>

</section>

<?php } ?>
