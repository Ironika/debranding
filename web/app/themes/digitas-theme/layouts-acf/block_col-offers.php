<?php if (get_field('col_offers_show')): ?>

<section class="block-col-offers">

  <div class="container">
    <div class="container-padded">

      <div class="block-col-offers_content">

        <div class="row">
          <div class="col-md-12">
            <?php if (get_field('col_offers_title')): ?>
              <h2 class="digitas-title"><?php echo get_field('col_offers_title'); ?></h2>
            <?php endif; ?>
            <?php if (get_field('col_offers_subtitle')): ?>
              <p class="digitas-sub-title"><?php echo get_field('col_offers_subtitle'); ?></p>
            <?php endif; ?>
          </div>
        </div>
        <?php if (get_field('col_offers_columns')): ?>
          <div class="row">
            <?php foreach (get_field('col_offers_columns') as $block) : ?>
              <div class="col-12 col-sm">
                <?php if ($block['col_offers_columns_image']) : ?>
                  <img src="<?php echo $block['col_offers_columns_image']['sizes']['medium'] ?>" />
                <?php endif; ?>
                <?php if ($block['col_offers_columns_title']) : ?>
                  <h3><?php echo $block['col_offers_columns_title'] ?></h3>
                <?php endif; ?>
                <?php if ($block['col_offers_columns_text']) : ?>
                  <p><?php echo $block['col_offers_columns_text'] ?></p>
                <?php endif; ?>
              </div>
            <?php endforeach; ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </div>

</section>

<?php endif; ?>
