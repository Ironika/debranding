<?php
// Post grid for All our solutions
$simplelink = 'Simple link';
$typeform = 'Typeform';
$youtube = 'Youtube';
?>
<section class="block-posts-grid block-posts-grid-loginpage">

    <div class="container">


	<div class="row">
	    <div class="col-md-12">
		<h2 class="digitas-title"><?php echo the_sub_field('posts_grid_block_title'); ?></h2>
	    </div>
	</div>


	<div class="row align-items-stretch">
	    <?php
	    if (have_rows('posts_grid')) :
		while (have_rows('posts_grid')) : the_row();
		    ?>

		    <?php
		    $type = get_sub_field('posts_grid_type');
		    $image = get_sub_field('posts_grid_image');
		    $image_link = get_sub_field('posts_grid_image_link');
		    $linked_post_id = get_sub_field('posts_grid_link');
		    $link_type = get_sub_field('posts_grid_image_select_a_link_type');

		    if ($type == 'Image') :
			?>
	    	    <div class="col-12 col-sm-6 col-md-4 sameheight">
	    		<div class="block-posts-grid_content block-posts-grid_image">
				<?php
				if ($link_type == $simplelink):
				    if (is_array(get_sub_field('posts_grid_image_link'))):
					?>
					<?php if ($image_link["url"]): ?><a href="<?php echo $image_link["url"]; ?>" <?php if ($image_link["target"]): ?> target="_blank" <?php endif ?> ><?php endif ?><div class="block-posts-grid_content--item" style="background-image: url('<?php echo $image['sizes']['soxo-medium'] ?>');"></div><?php if ($image_link["url"]): ?></a><?php endif ?>
				    <?php endif; ?>
				<?php elseif ($link_type == $typeform) : ?>
				    <a href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform=<?php echo $image_link["url"]; ?>">
				       <div class="block-posts-grid_content--item" style="background-image: url('<?php echo $image['sizes']['soxo-medium'] ?>');"></div>
				</a>
			    <?php elseif ($link_type == $youtube) : ?>
				<a data-fancybox href="<?php echo $image_link["url"]; ?>">
				    <div class="block-posts-grid_content--item" style="background-image: url('<?php echo $image['sizes']['soxo-medium'] ?>');"></div>
				</a>
			    <?php endif; ?>
	    	    </div>
	    	</div>

		<?php elseif ($type == 'Post') : ?>
	    	<div class="col-12 col-sm-6 col-md-4 sameheight">
	    	    <div class="block-posts-grid_content block-posts-grid_post">
	    		<a href="<?php echo get_permalink($linked_post_id->ID); ?>" class="block-posts-grid_content--item">
	    		    <div class="block-posts-grid_content--item_image">
	    			<img src="<?php echo $image['sizes']['soxo-medium'] ?>" alt="" />
	    		    </div>
	    		    <div class="block-posts-grid_content--item_text">
	    			<h3><?php echo get_the_title($linked_post_id->ID) ?></h3>
				    <?php if (get_sub_field('posts_grid_description')) { ?>
					<p><?php echo get_sub_field('posts_grid_description'); ?></p>
				    <?php } ?>
	    		    </div>
	    		</a>
	    	    </div>
	    	</div>
		<?php else : ?>
	    	<div class="col-12 col-sm-6 col-md-4 sameheight">
	    	    <div class="block-posts-grid_content block-posts-grid_post">
			    <?php
			    if (get_sub_field('posts_grid_image_select_a_link_type') == $simplelink):
				if ($image_link):
				    ?>
		    		<a href="<?php echo $image_link["url"]; ?>" <?php if ($image_link["target"]): ?> target="_blank" <?php endif ?> class="block-posts-grid_content--item">
				    <?php endif; ?>
				<?php elseif (get_sub_field('posts_grid_image_select_a_link_type') == $typeform) : ?>
				    <a class="block-posts-grid_content--item" href="javascript:;" data-toggle="modal" data-target="#typeformModal" data-typeform="<?php echo $image_link["url"]; ?>">
					<?php echo get_field('posts_grid_image_label'); ?>
				    </a>
				<?php elseif (get_sub_field('posts_grid_image_select_a_link_type') == $youtube) : ?>
				    <a class="block-posts-grid_content--item" data-fancybox href="<?php echo $image_link["url"]; ?>">
				    <?php endif; ?>

	    			<div class="block-posts-grid_content--item_image">
	    			    <img src="<?php echo $image['sizes']['soxo-medium'] ?>" alt="" />
	    			</div>
	    			<div class="block-posts-grid_content--item_text">
					<?php if (get_sub_field('posts_grid_title')) { ?>
					    <h3><?php echo get_sub_field('posts_grid_title'); ?></h3>
					<?php } ?>
					<?php if (get_sub_field('posts_grid_description')) { ?>
					    <p><?php echo get_sub_field('posts_grid_description'); ?></p>
					<?php } ?>
	    			</div>
				    <?php if (get_sub_field('posts_grid_image_select_a_link_type')): ?>
				    </a>
				<?php endif; ?>
	    	    </div>
	    	</div>
		<?php
		endif;

	    endwhile;
	    wp_reset_postdata();
	endif;
	?>
    </div>

</div>

</section>
