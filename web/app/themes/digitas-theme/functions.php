<?php

function understrap_remove_scripts() {

// Remove parent theme styles and scripts
    wp_dequeue_style('understrap-styles');
    wp_deregister_style('understrap-styles');

    wp_dequeue_script('understrap-scripts');
    wp_deregister_script('understrap-scripts');

// Remove js composer styles and scripts
    wp_dequeue_style('js_composer_front');
    wp_deregister_style('js_composer_front');

    wp_dequeue_script('wpb_composer_front_js');
    wp_deregister_script('wpb_composer_front_js');

// Removes the parent themes stylesheet and scripts from inc/enqueue.php
}

add_action('wp_enqueue_scripts', 'understrap_remove_scripts', 20);

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function theme_enqueue_styles() {
// Get the theme data
    $the_theme = wp_get_theme();

    wp_enqueue_style('digitas-styles', get_stylesheet_directory_uri() . '/dist/styles/main.min.css', array(), $the_theme->get('Version'));
    wp_enqueue_script('digitas-scripts-vendors', get_stylesheet_directory_uri() . '/dist/scripts/vendors.min.js', array(), $the_theme->get('Version'), true);
    wp_enqueue_script('digitas-scripts', get_stylesheet_directory_uri() . '/dist/scripts/main.min.js', array(), $the_theme->get('Version'), true);

// wp_enqueue_style('digitas-styles-widgets', plugins_url('/dlbi-digitas-vc-widget/dist/styles/widgets.min.css'), array(), $the_theme->get('Version'));
// wp_enqueue_script('digitas-scripts-widgets', plugins_url('/dlbi-digitas-vc-widget/dist/scripts/widgets.min.js'), array(), $the_theme->get('Version'), true);
// pass Ajax Url to script.js
    wp_localize_script('digitas-scripts', 'ajaxurl', admin_url('admin-ajax.php'));
    wp_localize_script('digitas-scripts-vendors', 'closetext', __('Close overlay', 'lbi-digitas-theme'));
}

/* Digitas custom functions starts here */

// Custom login logo
add_action('login_enqueue_scripts', 'digitas_login_logo');

function digitas_login_logo() {
    ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
    	background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-digitas-login.svg);
    	height: 70px;
    	width: 200px;
    	background-size: 200px 70px;
    	background-repeat: no-repeat;
        }
    </style>
    <?php
}

add_filter('login_headerurl', 'digitas_login_headerurl');

function digitas_login_headerurl() {
    return get_bloginfo('url');
}

add_filter('login_headertitle', 'digitas_login_headertitle');

function digitas_login_headertitle() {
    return get_bloginfo('name');
}

// Filter to replace default Visual Composer css class names for vc_row shortcode and vc_column with Bootstrap css class row and col-*-*
/*
  add_filter('vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 10, 2);

  function custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
  if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
  $class_string = str_replace('vc_row-fluid', 'row', $class_string); // This will replace "vc_row-fluid" with "row"
  }
  if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
  $class_string = preg_replace('/vc_col-sm-(\d{1,2})/', 'col-sm-$1', $class_string); // This will replace "vc_col-sm-%" with "col-sm-%"
  }
  return $class_string; // Important: you should always return modified or original $class_string
  }


  // Filter to delete default Visual Composer css class names
  /*
  add_filter('vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 10, 2);

  function custom_css_classes_for_vc_row_and_vc_column($class_string, $tag) {
  if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
  $class_string = str_replace(['vc_row-fluid','vc_row_inner','vc_row'], '', $class_string); // This will replace VC rows with nothing
  }
  if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
  $class_string = preg_replace('/vc_col-sm-(\d{1,2})/', '', $class_string); // This will replace VC columns with nothing
  }
  // return $class_string; // Important: you should always return modified or original $class_string
  }
 */

// Custom image size
add_image_size('digitas-medium', 650, 500, true);

// Hero image header
add_image_size('digitas-hero-header', 1500, 350, true);

// Image blog homepage large (= 510x320 format)
add_image_size('digitas-blog-medium', 590, 370, true);


// Image sub menu (= 200x125 format)
add_image_size('digitas-sub-menu', 200, 125, true);

/**
 * Register Sidebar.
 *
 * @since dlbi-digitas-theme 1.0
 *
 * @return void
 */
function lbi_digitas_theme_widgets_init() {
    /**
     * Sidebar that appears right in a single blog
     * page and single testimonials
     *
     */
    $args = array(
	'name' => __('Single Right Sidebar', 'dlbi-digitas-theme'),
	'id' => 'right-single',
	'description' => __('Right widget sidebar area for page single : blog and testimonials', 'dlbi-digitas-theme'),
	'class' => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h2 class="widgettitle">',
	'after_title' => '</h2>'
    );

// Register sidebar
    register_sidebar($args);

    /**
     * Footer 1
     *
     */
    $args = array(
	'name' => __('Footer 1', 'dlbi-digitas-theme'),
	'id' => 'footer-1',
	'description' => __('Footer 1', 'dlbi-digitas-theme'),
	'class' => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h2 class="widgettitle">',
	'after_title' => '</h2>'
    );

// Register sidebar
    register_sidebar($args);

    /**
     * Footer 2
     *
     */
    $args = array(
	'name' => __('Footer 2', 'dlbi-digitas-theme'),
	'id' => 'footer-2',
	'description' => __('Footer 2', 'dlbi-digitas-theme'),
	'class' => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h2 class="widgettitle">',
	'after_title' => '</h2>'
    );

// Register sidebar
    register_sidebar($args);
    /**
     * Footer 3
     *
     */
    $args = array(
	'name' => __('Footer 3', 'dlbi-digitas-theme'),
	'id' => 'footer-3',
	'description' => __('Footer 3', 'dlbi-digitas-theme'),
	'class' => '',
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h2 class="widgettitle">',
	'after_title' => '</h2>'
    );

// Register sidebar
    register_sidebar($args);

// Unregister sidebar
    unregister_sidebar('right-sidebar');
    unregister_sidebar('left-sidebar');
    unregister_sidebar('hero');
    unregister_sidebar('statichero');
    unregister_sidebar('footerfull');
}

/*
 * Init Sidebar
 */
add_action('widgets_init', 'lbi_digitas_theme_widgets_init', 11);


if (!function_exists('lbi_digitas_theme_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function lbi_digitas_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on lbi-digitas-theme, use a find and replace
	 * to change 'lbi-digitas-theme' to the name of your theme in all the template files.
	 */

//load_child_theme_textdomain('lbi-digitas-theme', get_template_directory_child() . '/languages');
	load_child_theme_textdomain('lbi-digitas-theme', get_stylesheet_directory() . '/languages');

// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support('title-tag');

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support('post-thumbnails');

	/*
	 * Unregister Understrap menus
	 */
	unregister_nav_menu('primary');

	/*
	 * Register the digitas menus
	 */
	register_nav_menus(array(
	    'top-nav' => 'Top Nav Menu', // Top nav
	    'top-menu-bar' => 'Main Menu', // Top Menu Bar
	    'burger-menu' => 'Burger Menu', // Buger side menu
	    'footer-nav' => 'Footer Menu'
	));


// This theme uses wp_nav_menu() in one location.
	lbi_digitas_create_menus('Top Menu Bar', 'top-menu-bar', array(
	    array(
		'menu-item-title' => __('Your needs'),
		'menu-item-classes' => 'your-needs',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Meal Solutions'),
		'menu-item-classes' => 'meal-solutions',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Gift Solutions'),
		'menu-item-classes' => 'gift-solutions',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Leisure Offers'),
		'menu-item-classes' => 'leisure-offers',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Public Solutions'),
		'menu-item-classes' => 'public-solutions',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Mobility Solutions'),
		'menu-item-classes' => 'mobility-solutions',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	));
	lbi_digitas_create_menus('Top Menu Bar affiliate', 'top-menu-affiliate-bar', array(
	    array(
		'menu-item-title' => __('Your needs'),
		'menu-item-classes' => 'your-needs',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	));
	update_option('top-menu-affiliate-bar', wp_get_nav_menu_items('top-menu-affiliate-bar'));

	lbi_digitas_create_menus('Top nav', 'top-nav', array(
	    array(
		'menu-item-title' => __('Check my balance'),
		'menu-item-classes' => 'check-my-balance',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'),
	    array(
		'menu-item-title' => __('Become on affiliate'),
		'menu-item-classes' => 'become-an-affiliate',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Place an order'),
		'menu-item-classes' => 'place-an-order',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	));

	lbi_digitas_create_menus('Burger Menu', 'burger-menu', array(
	    array(
		'menu-item-title' => __('Your Needs'),
		'menu-item-classes' => 'your-needs',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'),
	    array(
		'menu-item-title' => __('All Our Solutions'),
		'menu-item-classes' => 'all-our-solutions',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('About Digitas'),
		'menu-item-classes' => 'about-digitas',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Digitas Blog'),
		'menu-item-classes' => 'digitas-blog',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Contact & FAQ'),
		'menu-item-classes' => 'contact-faq',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Ask for a Quote'),
		'menu-item-classes' => 'ask-for-a-quote',
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	));

	update_option('top-menu-bar', wp_get_nav_menu_items('top-menu-bar'));

	lbi_digitas_create_menus('Footer nav', 'footer-nav', array(
	    array(
		'menu-item-title' => __('Accesibility'),
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'),
	    array(
		'menu-item-title' => __('Legal'),
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	    array(
		'menu-item-title' => __('Sitemap'),
		'menu-item-url' => home_url('/'),
		'menu-item-status' => 'publish'
	    ),
	));

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support('html5', array(
	    'search-form',
	    'comment-form',
	    'comment-list',
	    'gallery',
	    'caption',
	));

// Set up the WordPress core custom background feature.
	add_theme_support('custom-background', apply_filters('lbi_digitas_theme_custom_background_args', array(
	    'default-color' => 'ffffff',
	    'default-image' => '',
	)));

// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');
    }

endif;

if (!function_exists('lbi_digitas_create_menus')):

    function lbi_digitas_create_menus($menu_name, $menu_location, $items) {
	if (!empty($menu_name)) {
// If it doesn't exist, let's create it.
	    if (!wp_get_nav_menu_object($menu_name)) {
		$menu_id = wp_create_nav_menu($menu_name);


// Set up links and add them to the menu.
		if ($items) {
		    foreach ($items as $item) {
			wp_update_nav_menu_item($menu_id, 0, $item);
		    }
		}

// Grab the theme locations and assign our newly-created menu
		if (!has_nav_menu($menu_location)) {
		    $locations = get_theme_mod('nav_menu_locations');
		    $locations[$menu_location] = $menu_id;
		    set_theme_mod('nav_menu_locations', $locations);
		}
	    }
	}
    }

endif;

add_action('init', 'lbi_digitas_theme_setup');

// Add ACF option page
if (function_exists('acf_add_options_page')) {
// add parent
    $parent = acf_add_options_page(array(
	'page_title' => 'Theme General Settings',
	'menu_title' => 'Theme Settings',
	'redirect' => false
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Homepage Settings',
	'menu_title' => 'Homepage',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'URL',
	'menu_title' => 'URL',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Social Settings',
	'menu_title' => 'Social',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Lead Management Toolbar Settings',
	'menu_title' => 'Lead Management Toolbar',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page footer
    acf_add_options_sub_page(array(
	'page_title' => 'Footer Settings',
	'menu_title' => 'Footer',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Archives Settings',
	'menu_title' => 'Archives',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => '404 Useful Links Settings',
	'menu_title' => '404 Useful Links Settings',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Newsletter',
	'menu_title' => 'Newsletter',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'SMTP',
	'menu_title' => 'SMTP',
	'parent_slug' => $parent['menu_slug'],
    ));
// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Simulators Send by mail',
	'menu_title' => 'Simulators Send by mail',
	'parent_slug' => $parent['menu_slug'],
    ));
// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Analytics options',
	'menu_title' => 'Analytics options',
	'parent_slug' => $parent['menu_slug'],
    ));
// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'RSS Feed',
	'menu_title' => 'RSS Feed',
	'parent_slug' => $parent['menu_slug'],
    ));

// add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Web Callback settings',
	'menu_title' => 'Web Callback',
	'parent_slug' => $parent['menu_slug'],
    ));

    // add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Sticky menu',
	'menu_title' => 'Sticky menu',
	'parent_slug' => $parent['menu_slug'],
    ));

    // add sub page
    acf_add_options_sub_page(array(
	'page_title' => 'Home page blog',
	'menu_title' => 'Home page blog',
	'parent_slug' => $parent['menu_slug'],
    ));
}

// digitas_alter_query_main
add_action('pre_get_posts', 'digitas_alter_query_main', 11);

function digitas_alter_query_main($query) {
    if (is_category() && $query->is_main_query() && !is_admin()) {
	$query->set('posts_per_page', 16);
    }

// If this is a search
    if (is_search() && $query->is_main_query() && !is_admin()
// and more precisely of a faq search
	    && (get_query_var('search') && get_query_var('search') == 'faq')) {
	$query->set('post_type', array('faq'));
    } elseif (is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'need')) {
	$query->set('post_type', array('need'));
    } elseif (is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'pages')) {
	$query->set('post_type', array('page'));
    } elseif (is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'products')) {
	$query->set('post_type', array('product'));
    } elseif (is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'testimonials')) {
	$query->set('post_type', array('testimonial'));
    } elseif (is_search() && $query->is_main_query() && !is_admin() && (get_query_var('search') && get_query_var('search') == 'blog')) {
	$query->set('post_type', array('post'));
    } elseif (is_search() && $query->is_main_query() && !is_admin()) {
	$query->set('post_type', array('post', 'page', 'faq', 'testimonial', 'need', 'product'));
    }
}

// Download an alternative template
add_action('template_include', 'digitas_search_template');

function digitas_search_template($template) {
// If this is a search
    if (is_search()) {
//Load loads the search results template
	$new_template = locate_template(array('search-result.php'));
	if ('' != $new_template) {
	    return $new_template;
	}
    }
    return $template;
}

// Load ACF fields
add_filter('acf/settings/save_json', 'digitas_acf_json_save_point');
add_filter('acf/settings/load_json', 'digitas_acf_json_load_point');

function digitas_acf_json_save_point($path) {
// update path
    $path = get_template_directory() . '/acf-json';

// return
    return $path;
}

function digitas_acf_json_load_point($paths) {
// append path
    $paths[] = get_template_directory() . '/acf-json';

// return
    return $paths;
}

function get_template_directory_child() {
// get directory parent
    $directory_template = get_template_directory();
    $directory_child = str_replace('understrap', '', $directory_template) . 'dlbi-digitas-theme';

// return
    return $directory_child;
}

class Walker_Nav_Top_Menu extends Walker_Nav_Menu {

    public function start_lvl(&$output, $depth = 0, $args = array()) {

	$indent = str_repeat("\t", $depth);
	$output .= "\n$indent<div class=\"sub-menu\">\n";
	$output .= "<div class=\"container\">\n";
	$output .= "<ul class=\"submenu-container\">\n";
	$output .= "<div class=\"row\">\n";
    }

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
	global $wp_query;

	$indent = ( $depth ) ? str_repeat("\t", $depth) : '';

	if ($depth == 0) { // Main menu
	    $class_names = $value = '';
	    $classes = empty($item->classes) ? array() : (array) $item->classes;
	    $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
	    $class_names = ' class="' . esc_attr($class_names) . '"';

	    $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';

	    $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
	    $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
	    $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
	    $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

	    $description = !empty($item->description) ? '<span>' . esc_attr($item->description) . '</span>' : '';

	    $item_output = $args->before;

	    $item_output .= '<a' . $attributes . '>';

	    $item_output .= '<span data-hover="' . $item->title . '">';
	    $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID);
	    $item_output .= $description . $args->link_after;
	    $item_output .= '</span>';
	    $item_output .= '</a>';
	    $item_output .= $args->after;
	} elseif ($depth == 1) { // Submenu
	    $class_names = $value = '';
	    $classes = empty($item->classes) ? array() : (array) $item->classes;
//$class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
	    $class_names = ' class="col ' . esc_attr($class_names) . '"';

	    $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';

	    if (get_field('submenu_type', $item) == 'Image menu item') {

		$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
		$attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
		$attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
		$attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

		$item_output = $args->before;
		$item_output .= '<a' . $attributes . '>';

		$post_id = url_to_postid($item->url);
		$thumb_id = get_post_thumbnail_id($post_id);
		if (!empty($thumb_id)) {
		    $thumb_id_url = wp_get_attachment_image_url($thumb_id, 'digitas-sub-menu');
		    $item_output .= '<img src="' . $thumb_id_url . '" alt="" />';
		}

		$item_output .= '<span>' . $item->title . '</span>';
		$item_output .= '</a>';
		$item_output .= $args->after;
	    } elseif (get_field('submenu_type', $item) == 'List group') {
		if (have_rows('submenu_list_group', $item)):
		    $item_output = $args->before;
		    $item_output .= '<ul class="submenu-list-items">';
		    while (have_rows('submenu_list_group', $item)) : the_row();
			$submenulinksgroupsdata = get_sub_field('submenu_link_item');
			$item_output .= '<li class="submenu-list-item"><a href="' . $submenulinksgroupsdata['url'] . '" target="' . $submenulinksgroupsdata['target'] . '"><i class="fa fa-arrow-right" aria-hidden="true"></i>' . $submenulinksgroupsdata['title'] . '</a></li>';
		    endwhile;
		    $item_output .= '</ul>';
		    $item_output .= $args->after;
		endif;
	    }
	}
	$output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    public function end_lvl(&$output, $depth = 0, $args = array()) {

	$output .= "</div>\n";
	$output .= "</ul>\n";
	$output .= "</div>\n";
	$output .= "</div>\n";
    }

}


class Walker_Nav_Menu_Top extends Walker_Nav_Menu {

  function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {
    
    global $wp_query;

    $indent = ( $depth ) ? str_repeat("\t", $depth) : '';
    
    $output .= $indent . '<li id="menu-item-' . $item->ID . '">';
    
    $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
    $attributes .= !empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
    $attributes .= !empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
    $attributes .= !empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

    $item_output = $args->before;
    
    $itemIcon = get_field('top_menu_icon', $item);
    if ($itemIcon) {
      $item_output .= '<img src="' . $itemIcon['url'] . '" width="20" height="20" alt="" />';
    }
    
    $item_output .= '<a' . $attributes . '>';
    $item_output .= '<span>' . $item->title . '</span>';
    $item_output .= '</a>';
    $item_output .= $args->after;
    
    $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
  
  }

}

// Fix the login url issue
add_filter('login_url', 'digitas_login_page', 10, 2);

function digitas_login_page($login_url, $redirect) {
    return str_replace("wp-login.php", "wp/wp-login.php", $login_url);
}

add_action('login_form', 'replace_login_submit_form', 1);

function replace_login_submit_form() {
    $form_content = ob_get_contents();
    $form_content = str_replace("wp-login.php", "wp/wp-login.php", $form_content);
    ob_get_clean();
    echo $form_content;
}

/**
 * Customize WordPress Toolbar
 *
 * @param obj $wp_admin_bar An instance of the global object WP_Admin_Bar
 */
add_action('admin_bar_menu', 'digitas_customize_toolbar', 999);

function digitas_customize_toolbar($wp_admin_bar) {

    $logout = $wp_admin_bar->get_node('logout');

    $wp_admin_bar->remove_node('logout');

    $nonce = wp_create_nonce('logout-nonce');
    $wp_admin_bar->add_node(array(
	'parent' => 'user-actions',
	'id' => 'logout',
	'title' => __('Log Out'),
	'href' => get_site_url() . '?action=logout'
    ));
}

add_filter('template_redirect', 'digitas_logout_page', 10);

function digitas_logout_page($logout_url) {
    if (isset($_GET['action']) && $_GET['action'] == 'logout') {
	wp_logout();
    }
}

/**
 * [list_searcheable_acf list all the custom fields we want to include in our search query]
 *
 * @return [array] [list of custom fields]
 *
 */
function digitas_list_searcheable_acf() {
    $searcheable_acf = array(
	"faq_question",
	"faq_answer",
	"testimonial_quote",
	"testimonial_author"
    );
    return $searcheable_acf;
}

/**
 * [advanced_custom_search search that encompasses ACF/advanced custom fields and taxonomies and split expression before request]
 * @param  [query-part/string]      $where    [the initial "where" part of the search query]
 * @param  [object]                 $wp_query []
 * @return [query-part/string]      $where    [the "where" part of the search query as we customized]
 */
function digitas_advanced_custom_search($where, $wp_query) {

    global $wpdb;

    if (empty($where))
	return $where;

// get search expression
    $terms = $wp_query->query_vars['s'];
    $wpdb_prefix = $wpdb->prefix;

// explode search expression to get search terms
    $exploded = explode(' ', $terms);
    if ($exploded === FALSE || count($exploded) == 0)
	$exploded = array(0 => $terms);

// reset search in order to rebuilt it as we whish
    $where = '';

// get searcheable_acf, a list of advanced custom fields you want to search content in
    $list_searcheable_acf = digitas_list_searcheable_acf();



    foreach ($exploded as $tag) :
	$where .= "
          AND (
            (" . $wpdb_prefix . "posts.post_title LIKE '%" . $tag . "%')
            OR (" . $wpdb_prefix . "posts.post_content LIKE '%" . $tag . "%')
            OR EXISTS (
              SELECT * FROM " . $wpdb_prefix . "postmeta
	              WHERE post_id = " . $wpdb_prefix . "posts.ID
	                AND (";
	foreach ($list_searcheable_acf as $searcheable_acf) :

	    if ($searcheable_acf == $list_searcheable_acf[0]):
		$where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%" . $tag . "%') ";
	    else :
		$where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%" . $tag . "%') ";
	    endif;

	endforeach;
	$where .= ")
            )
            OR EXISTS (
              SELECT * FROM " . $wpdb_prefix . "terms
              INNER JOIN " . $wpdb_prefix . "term_taxonomy
                ON " . $wpdb_prefix . "term_taxonomy.term_id = " . $wpdb_prefix . "terms.term_id
              INNER JOIN " . $wpdb_prefix . "term_relationships
                ON " . $wpdb_prefix . "term_relationships.term_taxonomy_id = " . $wpdb_prefix . "term_taxonomy.term_taxonomy_id
              WHERE (
                       taxonomy = 'post_tag'
            		OR taxonomy = 'category'
            		OR taxonomy = 'topic'
            		OR taxonomy = 'profile'
          		)
              	AND object_id = " . $wpdb_prefix . "posts.ID
              	AND " . $wpdb_prefix . "terms.name LIKE '%" . $tag . "%'
            )
        )";
    endforeach;
    return $where;
}

//add_filter('posts_search', 'digitas_advanced_custom_search', 500, 2);
// Change search function globally to search only post, page and portfolio post types
function prefix_limit_post_types_in_search($query) {

    if ($query->is_post_type_archive('faq') && !is_admin() && !is_search()) {
	$query->set('posts_per_page', 6);
    }

    return $query;
}

add_filter('pre_get_posts', 'prefix_limit_post_types_in_search', 10);

// Display the last viewed product in homepage
function digitas_display_product_in_header() {
    if (function_exists('cn_cookies_accepted') && cn_cookies_accepted()) {
	if (is_singular(DIG_PRO_PTYPE)) {
	    setcookie('product_homepage', get_the_ID(), time() + 2592000, '/');
	} elseif (isset($_COOKIE['product_homepage']) && is_singular(DIG_PRO_PTYPE)) {
	    unset($_COOKIE['product_homepage']);
	    setcookie('product_homepage', get_the_ID(), time() + 2592000, '/');
	}
    }
}

add_action('template_redirect', 'digitas_display_product_in_header');


/**
 * Theme View
 *
 * Include a file and(optionally) pass arguments to it.
 *
 * @param string $file The file path, relative to theme root
 * @param array $args The arguments to pass to this file. Optional.
 * Default empty array.
 *
 * @return object Use render() method to display the content.
 */
if (!class_exists('Digitas_ThemeView')) {

    class Digitas_ThemeView {

	private $args;
	private $file;

	public function __get($name) {
	    return $this->args[$name];
	}

	public function __construct($file, $args = array()) {
	    $this->file = $file;
	    $this->args = $args;
	}

	public function __isset($name) {
	    return isset($this->args[$name]);
	}

	public function render() {
	    if (locate_template($this->file)) {
		include( locate_template($this->file) ); //Theme Check free. Child themes support.
	    }
	}

    }

}

/**
 * Digitas Get Template Part
 *
 * An alternative to the native WP function `get_template_part`
 *
 * @see PHP class Digitas_ThemeView
 * @param string $file The file path, relative to theme root
 * @param array $args The arguments to pass to this file. Optional.
 * Default empty array.
 *
 * @return string The HTML from $file
 */
if (!function_exists('digitas_get_template_part')) {

    function digitas_get_template_part($file, $args = array()) {
	$template = new Digitas_ThemeView($file, $args);
	$template->render();
    }

}

/**
 * Add styles and js : ACF Simulator
 * @param  none
 * @return none
 */
function digitas_enqueue_stylesheets() {
    global $post;

    if (isset($post->post_type)):

	$show_block = get_field('simulator_show_block', $post->ID);

// Add styles and JS for simulator : french / spain and chili
	if (($post->post_type == 'product') && $show_block):

	    $array = array('french', 'chile');
	    $country = '';
	    $widgetscript = 'widgets-script-';

	    if (get_field('select_a_country')):
		$country = get_field('select_a_country');
	    endif;

	    if (in_array($country, $array)):
		$simulator = $country;
	    else :
		$simulator = get_field('select_a_simulator');
	    endif;

	    $uri = get_stylesheet_directory_uri() . '/../../plugins/dlbi-digitas-' . $country . '-simulator/';
	    $uriapp = $uri . 'app-' . $simulator . '/dist/';
	    $styles = $uriapp . 'styles/main.css';

// Styles
	    wp_enqueue_style('widgets-style', $styles);

// Javascript
	    wp_enqueue_script($widgetscript . '1', $uriapp . 'manifest.js', '', '', true);
	    wp_enqueue_script($widgetscript . '2', $uriapp . 'normalize.js', '', '', true);
	    wp_enqueue_script($widgetscript . '3', $uriapp . 'vendor.js', '', '', true);
	    wp_enqueue_script($widgetscript . '4', $uriapp . 'main.js', '', '', true);
	endif;
    endif;
    return;
}

add_action('wp_enqueue_scripts', 'digitas_enqueue_stylesheets', 18);

function blog_action() {

    $posts_per_page = (isset($_POST["posts_per_page"])) ? $_POST["posts_per_page"] : 16;
    $page = (isset($_POST['pageNumber'])) ? $_POST['pageNumber'] : 0;
    $cat = (isset($_POST['cat'])) ? $_POST['cat'] : '';
    $plugin = 'dlbi-digitas-testimonials/dlbi-digitas-testimonials.php';
    $array_posts = array('post');

    header("Content-Type: text/html");
// Check if plugin is enabled
    if (is_plugin_active($plugin)) :
	$array_posts[] = 'testimonial';
    endif;
    $args = array(
	'post_type' => $array_posts,
	'posts_per_page' => $posts_per_page,
	'cat' => $cat,
	'paged' => $page,
    );
    $the_query = new WP_Query($args);

//Reste à faire : voir avec harold pour passer en variable is_page_template('homepage-blog.php')
// et faire un test dessus
    if ($the_query->have_posts()) :
	$count_posts = 0;
	?>
	<ul class="row" id="blog-articles">
	    <?php
	    while ($the_query->have_posts()) : $the_query->the_post();
		$count_posts = $the_query->current_post + 1; //counts posts in loop
		// At position 3 you will see the newsletter
		if ($count_posts == 3 && ($page <= 1)):
		    // Get template widget
		    echo '<li class="blog-item col-sm-6 col-lg-3">';
		    get_template_part('templates/content', 'widget-newsletter');
		    echo '</li>';
		endif;
		if ($count_posts == 9 && ($page <= 1)):
		    // Display product
		    echo '<li class="blog-item col-sm-6 col-lg-3">';
		    get_template_part('templates/content', 'product-blog-hp');
		    echo '</li>';
		endif;
		// Display posts
		echo'<li class="blog-item col-sm-6 col-lg-3">';
		get_template_part('loop-templates/content-post');
		echo '</li>';
	    endwhile;
	    ?>
	</ul>
	<?php
	wp_reset_postdata();
    elseif (have_posts()) :
	$count_posts = 0; //counts posts in loop
	while (have_posts()) : the_post();
	    echo '<li class="col-sm-6 col-lg-3">';
	    get_template_part('loop-templates/content-post');
	    echo '</li>';
	    $count_posts++;
	endwhile;
    endif;
}

add_action('wp_ajax_blog_action', 'blog_action');
add_action('wp_ajax_nopriv_blog_action', 'blog_action');

// Configure SMPT servor
add_action('phpmailer_init', 'digitas_phpmailer_init');

function digitas_phpmailer_init(PHPMailer $phpmailer) {
    $host = get_field('smtp_host', 'option');
    $smtp_port = get_field('smtp_port', 'option');
    $smtp_username = get_field('smtp_username', 'option');
    $smtp_password = get_field('smtp_password', 'option');
    $smtp_authentification = get_field('smtp_authentification', 'option');
    $smtp_secure = get_field('smtp_secure', 'option');

    $phpmailer->Host = $host;
    $phpmailer->Port = $smtp_port; // could be different
    if ($smtp_username) {
	$phpmailer->Username = 'your_username@example.com'; // if required
    }
    if ($smtp_password) {
	$phpmailer->Password = 'yourpassword'; // if required
    }

    $phpmailer->SMTPAuth = $smtp_authentification; // if required
    if ($smtp_secure) {
	$phpmailer->SMTPSecure = $smtp_secure; // enable if required, 'tls' is another possible value
    }


    $phpmailer->IsSMTP();
}

// Newsletter
add_action('wp_footer', 'digitas_display_newsletter_modal');

function digitas_display_newsletter_modal() {
    if ((isset($_POST['newsletter_email']) && !empty($_POST['newsletter_email']))) {
	$content = '<p>This is inserted at the bottom</p>';
	echo $content;
    }
}

// Flush cache
add_action('template_redirect', 'digitas_flush_cache');

function digitas_flush_cache() {
    shell_exec('/usr/local/bin/wp-try-flush-caches.sh');
}

// Redirect 404 for wrong faq page
add_action('template_redirect', 'digitas_archive_redirect', 100);

function digitas_archive_redirect() {
    $redirects = get_field('archives_redirection', 'option');
    if ($redirects) {
	if (sizeof($redirects) > 0) {
	    foreach ($redirects as $redirect) {
		if (is_post_type_archive($redirect['archive_post_type'])) {
		    wp_redirect($redirect['archive_url']);
		}
	    }
	}
    }
}

// Remove redirect for non caninocal link
add_filter('redirect_canonical', 'no_redirect_on_404');

function no_redirect_on_404($redirect_url) {
    if (is_404()) {
	return false;
    }
    return $redirect_url;
}

// Import posts from rss feed
function digitas_import_feed_items() {
    $feed = fetch_feed(get_field('feed_url', 'option'));

    if (!is_wp_error($feed)) {

	if ($last_import = get_option('last_import')) {
	    $last_import_time = $last_import;
	} else {
	    $last_import_time = false;
	}

	$items = $feed->get_items();

	$latest_item_time = false;

	foreach ($items as $item) {
	    $item_date = $item->get_date('Y-m-d H:i:s');
	    if ($last_import_time && ($last_import_time >= strtotime($item_date))) {
		continue;
	    }
	    $content = $item->get_content();

	    $post = array(
		'post_content' => $content,
		'post_date' => $item_date,
		'post_title' => $item->get_title(),
		'post_status' => 'publish',
		'post_type' => 'post'
	    );

// Insert post
	    $post_id = wp_insert_post($post);
	    update_post_meta($post_id, 'post_feed', $item->get_link());

//Add thumbnail
	    $enclosure = $item->get_enclosure();
	    if ($enclosure) {
		digitas_insert_attachment_from_url($enclosure->get_link(), $post_id);
	    }

// Add categories
	    $post_categories = get_field('feed_post_categories', 'option');
	    if ($post_categories) {
		wp_set_post_categories($post_id, $post_categories);
	    }

	    if (strtotime($item_date) > $latest_item_time) {
		$latest_item_time = strtotime($item_date);
	    }
	}

	if (false !== $latest_item_time) {
	    update_option('last_import', $latest_item_time);
	}
    }
}

add_action('wp', 'digitas_import_feed_items');

/**
 * Insert an attachment from an URL address.
 *
 * @param  String $url
 * @param  Int    $post_id
 * @param  Array  $meta_data
 * @return Int    Attachment ID
 */
function digitas_insert_attachment_from_url($url, $post_id = null) {

    if (!class_exists('WP_Http'))
	include_once( ABSPATH . WPINC . '/class-http.php' );

    $http = new WP_Http();
    $response = $http->request($url);
    if ($response['response']['code'] != 200) {
	return false;
    }

    $upload = wp_upload_bits(basename($url), null, $response['body']);
    if (!empty($upload['error'])) {
	return false;
    }

    $file_path = $upload['file'];
    $file_name = basename($file_path);
    $file_type = wp_check_filetype($file_name, null);
    $attachment_title = sanitize_file_name(pathinfo($file_name, PATHINFO_FILENAME));
    $wp_upload_dir = wp_upload_dir();

    $post_info = array(
	'guid' => $wp_upload_dir['url'] . '/' . $file_name,
	'post_mime_type' => $file_type['type'],
	'post_title' => $attachment_title,
	'post_content' => '',
	'post_status' => 'inherit',
    );

// Create the attachment
    $attach_id = wp_insert_attachment($post_info, $file_path, $post_id);
    update_post_meta($post_id, '_thumbnail_id', $attach_id);

// Include image.php
    require_once( ABSPATH . 'wp-admin/includes/image.php' );

// Define attachment metadata
    $attach_data = wp_generate_attachment_metadata($attach_id, $file_path);

// Assign metadata to attachment
    wp_update_attachment_metadata($attach_id, $attach_data);

    return $attach_id;
}

add_action('after_setup_theme', 'digitas_remove_admin_bar');

// Hide admin bar
function digitas_remove_admin_bar() {
    if (defined(WP_ENV)) {
	if (WP_ENV == 'production') {
	    show_admin_bar(false);
	}
    }
}

//Activate ACF if it's deactivate
if (!is_plugin_active('advanced-custom-fields/acf.php')) {
    activate_plugin('advanced-custom-fields/acf.php');
}

// Add place an order button in simulator
function digitas_add_simu_order_button() {
    if (is_singular(DIG_PRO_PTYPE)):
	$order = get_field('simulator_place_an_order_link', get_the_ID());
	if ($order):
	    ?>
	    <script>
	        jQuery(window).scroll(function () {
	    	jQuery('.callToAction-link-order').text('<?php echo $order['title'] ?>');
	    	jQuery('.callToAction-link-order').attr('href', '<?php echo $order['url'] ?>');
	        });
	    </script>
	<?php else: ?>
	    <script>
	        jQuery(window).scroll(function () {
	    	jQuery('.callToAction-link-order').css('display', 'none');
	        });
	    </script>
	<?php
	endif;
    endif;
}

add_action('wp_footer', 'digitas_add_simu_order_button');

function digitas_curl($url, $data, $post, $auth) {
    $ch = curl_init($url);
    if ($auth) {
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $auth));
    } else {
	curl_setopt($ch, CURLOPT_HEADER, false);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, $post);
    if ($post) {
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    } else {
	curl_setopt($ch, CURLOPT_URL, $url);
    }
    $contents = curl_exec($ch);
    curl_close($ch);

    return json_decode($contents);
}

// Add to cart
function digitas_add_to_cart() {
    if (isset($_GET['add_item']) && $_GET['add_item'] == '1') {

	// Create token
	$data = array(
	    "client_id" => 'demo_client',
	    "client_secret" => 'secret_demo_client',
	    "grant_type" => 'password',
	    "username" => 'api@example.com',
	    "password" => 'sylius-api'
	);
	$auth = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/oauth/v2/token', $data, true, false)->access_token;

	// Create cart
	$data = array(
	    "customer" => 'khouildi.salah@gmail.com',
	    "channel" => 'US_WEB',
	    "localeCode" => 'en_US',
	);

	$car_id_transient = get_transient('cart_id');

	if (!$car_id_transient) {
	    $cart = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/v1/carts/', $data, true, $auth);
	    $car_id_transient = set_transient('cart_id', $cart->id, 12 * 3600);
	    $cart_id = $cart->id;
	} else {
	    $cart_id = $car_id_transient;
	    $cart = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/v1/carts/' . $cart_id, array(), false, $auth);
	    if ($cart->code == 404) {
		$cart = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/v1/carts/', $data, true, $auth);
		$car_id_transient = set_transient('cart_id', $cart->id, 12 * 3600);
		$cart_id = $cart->id;
	    }
	}

// Add an item to the cart
	$data = array(
	    "variant" => '7e00f7cf-0c51-3c09-9704-3dc63c6ef2d0-variant-0',
	    "quantity" => $_GET['nombreticketsresto'],
	);
	$add_item = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/v1/carts/' . $cart_id . '/items/', $data, true, $auth);

	wp_redirect('https://advantage.pocsf.dev.digitasdlbi.xyz/en_US/');
    }
}

add_action('template_redirect', 'digitas_add_to_cart');

function digitas_display_cart() {

// Create token
    $data = array(
	"client_id" => 'demo_client',
	"client_secret" => 'secret_demo_client',
	"grant_type" => 'password',
	"username" => 'api@example.com',
	"password" => 'sylius-api'
    );
    $auth = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/oauth/v2/token', $data, true, false)->access_token;

    $cart_id = get_transient('cart_id');


    $cart = digitas_curl('https://advantage.pocsf.dev.digitasdlbi.xyz/api/v1/carts/' . $cart_id, array(), false, $auth);
    if ($cart->id) :
	?>
	<div class="container">
	    <div class="row">
		<div class="col-md-12">
		    <h3>CART</h3>
		    <?php
		    foreach ($cart->items as $item) {
			echo $item->quantity . ' X ' . substr_replace($item->unitPrice, '.', -2, 0) . ' = ' . substr_replace($item->total, '.', -2, 0) . '<br />';
		    }

		    echo 'TOTAL : ' . substr_replace($cart->total, '.', -2, 0);
		    ?>

		</div>
	    </div>
	</div>
	<?php
    endif;
}

// Alter search query
function search_filter($query) {
    if (!is_admin() && $query->is_main_query()) {
	if ($query->is_search) {
	    if (isset($_GET['search'])) {
		$query->set('post_type', array($_GET['search']));
	    }
	}
    }
}

add_action('pre_get_posts', 'search_filter');
