<?php
/**
 * The template for displaying search results pages.
 *
 * @package understrap
 */
get_header();


// Retreive the origin of the search query for the Tagging Plan
$searchOrigin = "";
if ( isset($_GET['origin']) ) {
  $origin = $_GET['origin'];
  switch ($origin) {
    case "LMT" :
      $searchOrigin = "LMT"; // Search made from the FAQ
      break;
    case "Burger" :
      $searchOrigin = "Burger"; // Search made from the search page
      break;
    case "FAQ" :
      $searchOrigin = "FAQ"; // Search made from the search page
      break;
    default :
      $searchOrigin = "générique"; // Search made from the search page
      break;
  }
} else {
  $searchOrigin = "générique";
}

$bgimage = get_field('banner_faq_and_404', 'option');
if( !empty($bgimage) ) {
  $bgimgurl = $bgimage['url'];
} else {
  $bgimgurl = get_stylesheet_directory_uri() . '/dist/images/bg-404-search.jpg';
}
?>

    <section class="search-block-aside" style="background-image:url('<?php echo $bgimgurl; ?>');" >
        <div class="container">
            <h1 class="tt"><?php echo __("We're here to help", 'lbi-digitas-theme'); ?></h1>
            <?php
            // Output a search for that only searches portfolio post types
            $search = get_search_form(false);
            echo $search;
            ?>
        </div><!-- container search end -->
    </section>

    <section class="wrapper" id="wrapper-search">
        <div class="container" id="content">
            <div class="row">
              <div class="col-12">
                <a href="<?php echo get_post_type_archive_link( SOD_FAQ_PTYPE ); ?>" class="back-link-page">
                  <i class="icon-arrow-left" aria-hidden="true"></i>
                  <span><?php echo __("Back to the FAQ page", 'lbi-digitas-theme'); ?></span>
                </a>
              </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-12">
          		    <?php if(!empty($_GET['s']) && isset($_GET['s'])):
                    // Total posts
                    global $wp_query; ?>
                    <h2 class="digitas-title"><strong><?php echo $wp_query->found_posts; ?> </strong> <?php echo __('Results for', 'lbi-digitas-theme') ?> <strong>"<?php echo get_search_query(); ?>"</strong></h2>
          		    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-9">
                        <div class="search-items">

                            <?php if (have_posts() && !empty($_GET['s'])) :
                                $z=0;?>
                                <div class="panel-group" id="accordion-faq" role="tablist" aria-multiselectable="true">
                                <?php while (have_posts()) : the_post();

                                    /**
                                     * Run the loop for the search to output the results.
                                     * If you want to overload this in a child theme then include a file
                                     * called content-search.php and that will be used instead.
                                     */
                                    digitas_get_template_part('loop-templates/content-search.php', array(
                                        'z' => $z
                                    ));
                                    $z++;
                                endwhile;

                                // Tag whith results ?>
                                <script>
                                  dataLayer.push({
                                    'event':'search',
                                    'value':'<?php echo addslashes($_GET['s']); ?>',
                                    'moteur-type':'<?php echo $searchOrigin; ?>'
                                  });
                                </script>
                              </div>
                            <?php else :

                                get_template_part('loop-templates/content', 'none');

                                // Tag no results ?>
                                <script>
                                  dataLayer.push({
                                    'event':'search-noresults',
                                    'value':'<?php echo addslashes($_GET['s']); ?>',
                                    'moteur-type':'<?php echo $searchOrigin; ?>'
                                  });
                                </script>

                            <?php endif; ?>

                        </div>
                    </div>
		    <?php if(get_query_var('s')): ?>
                    <div class="col-12 col-md-3">
                      <?php get_template_part('templates/content', 'search-page-filters'); ?>
                    </div>
		    <?php endif ?>
                </div>

                    <?php if (have_posts() && !empty($_GET['s'])) : ?>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="text-center search-paginate"><?php
                                    echo paginate_links(); ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

    </section>

    <div class="breadcrumb">
        <?php
        if(function_exists('bcn_display')):
            bcn_display();
        endif; ?>
    </div>


<?php get_footer(); ?>
