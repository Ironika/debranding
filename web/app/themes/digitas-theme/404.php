<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package understrap
 */

get_header();
$links_list = get_field('digitas_404_useful_links_list', 'option');
?>
<section class="wrapper" id="wrapper-faq">
    <div class="container" id="content">
        <div class="container-padded">
            <div class="content-area" id="primary">
                <main class="site-main" id="main" role="main">
                    <section class="error-404 not-found">
                        <header class="page-header">
                            <h1 class="page-title"><?php esc_html_e( 'Hummm,', 'lbi-digitas-theme' ); ?></h1>
                            <h2 class="sub-title"><?php esc_html_e( 'Looks like something went wrong', 'lbi-digitas-theme' ); ?></h2>
                            <p class="text-ct"><?php esc_html_e( 'Don\'t worry, we\'re hard work getting that fixed. In the meantime we have a piece of cake...', 'lbi-digitas-theme' ); ?></p>
                        </header><!-- .page-header -->

                        <div class="page-content">
                            <p class="text-ct"><?php esc_html_e( 'Here are some useful links for you to find your way:', 'lbi-digitas-theme' ); ?></p>
                            <?php if (count($links_list) > 0): ?>
                                <ul class="useful-links">
                                    <?php foreach ($links_list as $link): ?>
                                        <?php
                                            $name = '';
                                            $href = '#';
                                            if ($link['digitas_404_useful_link_name']) {
                                                $name = $link['digitas_404_useful_link_name'];
                                            }
                                            if ($link['digitas_404_useful_link_internal_content']) {
                                                $href = $link['digitas_404_useful_link_internal_content'];
                                            } elseif ($link['digitas_404_useful_link_external_content']) {
                                                $href = $link['digitas_404_useful_link_external_content'];
                                            }
                                        ?>
                                        <li><a href="<?php echo $href ?>" class="icon-arrow-right"><span><?php echo $name; ?></span></a></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div><!-- .page-content -->
                    </section><!-- .error-404 -->
                </main><!-- #main -->
            </div><!-- #primary -->
        </div> <!-- .row -->
    </div><!-- Container end -->

    <?php
    $bgimage = get_field('banner_faq_and_404', 'option');
    if( !empty($bgimage) ) {
      $bgimgurl = $bgimage['url'];
    } else {
      $bgimgurl = get_stylesheet_directory_uri() . '/dist/images/bg-404-search.jpg';
    }
    ?>
    <section class="search-block-aside" style="background-image:url('<?php echo $bgimgurl; ?>');" >
        <div class="container">
            <h2 class="tt"><?php esc_html_e("We're here to help", 'lbi-digitas-theme' ); ?></h2>
            <?php require get_template_directory_child() . '/inc/banner-search.php'; ?>
        </div><!-- container search end -->
    </section>
</section><!-- Wrapper end -->


<?php get_footer('light'); ?>
