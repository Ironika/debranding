<?php
/* Homepage banner */
?>
<?php if( isset($_COOKIE['product_homepage']) && get_field('homepage_show_last_viewed_product', 'option') ) :

$product_id = $_COOKIE['product_homepage'];
?>

<section class="prod-head">
<!-- Product head block -->
<?php $prodheadimgbg = get_field('product_header_image_background', $product_id); ?>
<div class="prod-head--background" style="background-image: url('<?php echo $prodheadimgbg['url']; ?>')">
<div class="prod-head--content">
  <div class="container">
  <div class="row">
    <div class="col-md-6">
      <div class="prod-head--content_textblock hidden-sm-down">
        <h2 class="sodexo-page-head-title"><?php echo get_field('product_header_title',$product_id) ?></h2>
        <div class="sodexo-page-head-description"><?php echo get_field('product_header_description', $product_id) ?></div>
        <a class="btn-sodexo btn-sodexo-red" href="<?php echo get_permalink($product_id) ?>"><?php  echo __('Discover more', 'lbi-digitas-theme');?></a>
      </div>
    </div><!--.product-head-block-->
    <div class="col-md-6 col-12 align-self-center text-center">
      <?php $prodheadimg = get_field('product_header_image', $product_id);
      if( !empty($prodheadimg) ): ?>
        <img class="prod-head--content_image" src="<?php echo $prodheadimg['url']; ?>" alt="<?php echo $prodheadimg['alt']; ?>" />
      <?php endif; ?>
    </div>
  </div>
  </div>
</div>
</div>
<div class="prod-head--content_mobile hidden-md-up">
<div class="container-fluid">
<div class="row">
  <div class="col-md-12">
    <div class="prod-head--content_textblock">
      <?php $link_simulate = get_field('product_header_link_simulate', $product_id);?>
      <h1 class="sodexo-page-head-title_mobile"><?php echo get_field('product_header_title',$product_id) ?></h1>
      <div class="sodexo-page-head-description"><?php echo get_field('product_header_description', $product_id) ?></div>
      <?php  if ($link_simulate["url"]) { ?>
        <a class="btn-sodexo btn-sodexo-red" href="<?php echo $link_simulate["url"] ?>">
            <?php if($link_simulate["title"]):?>
                <?php echo $link_simulate["title"]; ?>
            <?php else : ?>
                <?php  echo __('Discover more', 'lbi-digitas-theme');?>
            <?php endif ?>
        </a>
      <?php } ?>
    </div>
  </div>
</div>
</div>
</div>
<!--.product-head-block-->

</section>

<?php else : ?>

  <div class="banner-home <?php if ( get_field('homepage_banner_color', 'option') == 'blacktxt' ) :  echo 'lighten'; endif; ?>">

    <div class="banner-home-type <?php if( get_field('homepage_banner_enable_video', 'option') ) : echo 'video-on'; else : echo 'video-off'; endif; ?>">

        <div class="banner-home-type-image">
          <?php
          // we alaways show the image on mobile because the autoplay function is disabled on mobile
          $hp_image = get_field('homepage_banner_image', 'option');
          $size = 'full'; // (thumbnail, medium, large, full or custom size)
          if ( $hp_image ) :
            echo wp_get_attachment_image( $hp_image, $size );
          endif;
          ?>
        </div>

        <?php if( get_field('homepage_banner_enable_video', 'option') ) : ?>

        <div class="banner-home-type-video">
          <div class="banner-home-type-video-container">

          <?php
          if (get_field('homepage_banner_video', 'option')) { // if a Youtube URL is set
          
          // The iframe is placed on the image if enabled but hidden on mobile (no autoplay)
          $url = get_field('homepage_banner_video', 'option', false, false);
          preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
          $video_id = $matches[1];
          ?>
          
          <div id="sodexoVideoPlayerBanner" style="opacity:0;"></div>
          
          <script>
          var tag = document.createElement('script');
          
          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
          
          function onYouTubeIframeAPIReady() {
            var player;
            player = new YT.Player('sodexoVideoPlayerBanner', {
              videoId: '<?php echo $video_id; ?>',
              playerVars: {
                playlist: '<?php echo $video_id; ?>',
                autoplay: 1,
                controls: 0,
                showinfo: 0,
                modestbranding: 1,
                loop: 1,
                fs: 0,
                cc_load_policy: 0,
                iv_load_policy: 3,
                autohide: 0
              },
              events: {
                'onReady' : onPlayerReady,
                'onStateChange': onPlayerStateChange
              }
            });
          }
          function onPlayerReady(event) {
            event.target.mute();
            // event.target.playVideo();
          }
          function onPlayerStateChange(event) {
              var videoBanner = document.getElementById("sodexoVideoPlayerBanner")
              videoBanner.style.transition = "opacity 0.5s";
              if (event.data === 1) {
                videoBanner.style.opacity = "1";
              }
          }
          </script>
            
        <?php } /* else if (get_field('homepage_upload_video', 'option')) { ?>
            <video autoplay loop muted>
              <source src="<?php echo get_field('homepage_upload_video', 'option')['url']; ?>" type="video/mp4">
              Your browser does not support the video tag.
            </video>
          <?php } */ ?>

          </div>
        </div>

        <?php endif; ?>

    </div>

    <div class="banner-home-text">
      <?php $hp_text = get_field('homepage_banner_text', 'option');
        if( $hp_text ) { ?><h2><?php echo $hp_text; ?></h2><?php } ?>
        <br>
        <a id="banner_arrow_down" href="Javascript:void(0)"><i class="fa fa-angle-down fa-lg" aria-hidden="true"></i></a>
    </div>

  </div>

<?php endif; ?>
