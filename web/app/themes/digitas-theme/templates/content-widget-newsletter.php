<?php
// Display widget newsletter
$args = array(
	'before_widget' => '<div class="widget %s">',
	'after_widget' => '</div>',
	'before_title' => '<h2 class="widget-title">',
	'after_title' => '</h2>'
);

// Get Field
$title = get_field('title', 'widget-newsletter-2');
$placeholder = get_field('placeholder', 'widget-newsletter-7');
$label_checkbox = get_field('label_checkbox', 'widget-newsletter-7');
$description = get_field('description', 'widget-newsletter-7');

// Field widget cf. BO
$instance = array(
	'title' => $title,
	'placeholder' => __('Your email','lbi-digitas-theme'),
	'label_checkbox' => __('I accept terms and conditions','lbi-digitas-theme'),
	'description' => ''
);

// Display widget
the_widget( 'SodexoWidget_Newsletter', $instance, $args );?>
