<?php
/* banner affiliate */
?>
<div class="banner-home <?php if ( get_field('homepage_affiliate_banner_color') == 'blacktxt' ) :  echo 'lighten'; endif; ?>">

  <div class="banner-home-type <?php if( get_field('homepage_affiliate_banner_enable_video') ) : echo 'video-on'; else : echo 'video-off'; endif; ?>">

      <div class="banner-home-type-image">
        <?php
        // we alaways show the image on mobile because the autoplay function is disabled on mobile
        $hp_image = get_field('homepage_affiliate_banner_image');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
        if ( $hp_image ) :
          echo wp_get_attachment_image( $hp_image, $size );
        endif;
        ?>
      </div>

      <?php if( get_field('homepage_affiliate_banner_enable_video') ) : ?>

      <div class="banner-home-type-video">
        <div class="banner-home-type-video-container">

        <?php
        // The iframe is placed on the image if enabled but hidden on mobile (no autoplay)
        // get iframe HTML
        $url = get_field('homepage_affiliate_banner_video', false, false);
        // var_dump($url);
        //parse_str(parse_url( $url, PHP_URL_QUERY ), $vars );
        //$video_id = $vars['v'];
        preg_match("/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:m\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/", $url, $matches);
        $video_id = $matches[1];
        ?>

        <div id="sodexoVideoPlayerBanner"></div>

        <script async src="https://www.youtube.com/iframe_api"></script>

        <script>
         function onYouTubeIframeAPIReady() {
          var player;
          player = new YT.Player('sodexoVideoPlayerBanner', {
            videoId: '<?php echo $video_id; ?>', // YouTube Video ID
            // width: 2000,       // Player width (in px)
            // height: 850,       // Player height (in px)
            playerVars: {
              playlist: '<?php echo $video_id; ?>',
              autoplay: 1,        // Auto-play the video on load
              controls: 0,        // Show pause/play buttons in player
              showinfo: 0,        // Hide the video title
              modestbranding: 1,  // Hide the Youtube Logo
              loop: 1,            // Run the video in a loop
              fs: 0,              // Hide the full screen button
              cc_load_policy: 0,  // Hide closed captions
              iv_load_policy: 3,  // Hide the Video Annotations
              autohide: 0         // Hide video controls when playing
            },
            events: {
              onReady: function(e) {
                e.target.mute();
              }
            }
          });
         }
        </script>

        </div>
      </div>

      <?php endif; ?>

  </div>

  <div class="banner-home-text">
    <?php $hp_text = get_field('homepage_affiliate_banner_text');
      if( $hp_text ) { ?>
        <h1><?php echo $hp_text; ?></h1>
      <?php } ?>
      <a href="#"><i class="fa fa-angle-down fa-lg" aria-hidden="true"></i></a>
  </div>

</div>
