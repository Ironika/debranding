#!/bin/sh
set -eu
#set -x

print_usage() {
	cat << EOT
Usage
    ${0##*/} [-p data_path]
    Deploy plugins and themes onto the WP update server.
    E.g.: ${0##*/} -h dev.sodexo.digitas.fr -p /srv/sodexo/dev/php-wordpress-dev_master

#    -h docker host
#        e.g. "dev.sodexo.digitas.fr"

    -p data path
        e.g. "/srv/sodexo/dev/php-wordpress-dev_master"
EOT
}


do_gulp() {
	for d in "$@"; do
		if [ -s "$d/gulpfile.js" ]; then
			cd "$d"
			if [ ! -d node_modules ]; then
				npm link gulp-cli
				npm install
			fi
			gulp build
			# CACHEDIR.TAG is for tar exclusion (via --exclude-caches-all): http://www.brynosaurus.com/cachedir/
			echo "Signature: 8a477f597d28d172789f06886806bc55" > node_modules/CACHEDIR.TAG
			cd -
		fi
	done
}


deploy_direct() {
	echo 'Backing up data {mu-plugins,plugins,themes} into deploy-backup.tgz, and deleting old content'
	sudo tar czf deploy-backup.tgz -C "${DATA_DEV_PATH}" bedrock/web/app/mu-plugins bedrock/web/app/plugins bedrock/web/app/themes && sudo rm -r "${DATA_DEV_PATH}/bedrock/web/app/mu-plugins" "${DATA_DEV_PATH}/bedrock/web/app/plugins" "${DATA_DEV_PATH}/bedrock/web/app/themes"
	echo 'Copying new data {mu-plugins,plugins,themes} into WP instance'
	tar c --exclude-caches-all mu-plugins plugins themes | sudo tar x -C "${DATA_DEV_PATH}/bedrock/web/app/"
	# uid:gid 33 = www-data
	sudo chown -R 33:33 "${DATA_DEV_PATH}/bedrock/web/app/"
	echo 'Flushing WP caches'
	if ls -f "${DATA_DEV_PATH}/flush_cache_flags"/* > /dev/null 2>&1; then
		sudo rm -f "${DATA_DEV_PATH}/flush_cache_flags"/*
	fi
}


main() {
	#DOCKER_DEV_HOST=${DOCKER_DEV_HOST:-}
	DATA_DEV_PATH=${DATA_DEV_PATH:-}
	
	# Options
	while getopts "p:" option; do
		case "$option" in
			#h) DOCKER_DEV_HOST=$OPTARG ;;
			p) DATA_DEV_PATH=$OPTARG ;;
			*) print_usage; exit 1 ;;
		esac
	done
	shift $((OPTIND - 1))  # Shift off the options and optional --
	
	echo "Compiling static resources..."
	do_gulp themes/dlbi-*
	echo "Deploying plugins and themes..."
	deploy_direct
}
main "$@"

